"""
Sensor that ensure the required Entity Ids have been detected by HomeAssistant
"""

import logging
import voluptuous as vol
from homeassistant.helpers.entity import Entity
from homeassistant.components.sensor import PLATFORM_SCHEMA
import homeassistant.helpers.config_validation as cv

_LOGGER = logging.getLogger(__name__)


# TODO: Find a way to credit the author
# https://www.flaticon.com/free-icon/checked_291201#term=check&page=1&position=6
LOADING_ICON = 'https://image.flaticon.com/icons/svg/291/291242.svg'
OK_ICON = 'https://image.flaticon.com/icons/svg/291/291201.svg'
ERROR_ICON = 'https://image.flaticon.com/icons/svg/291/291202.svg'

LOADING_STATE = 'Loading'
OK_STATE = 'All Entities present'
ERROR_STATE = 'Missing Entities! See logs'


CONF_ENTITY_IDS = 'entity_ids'
PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Required(CONF_ENTITY_IDS): cv.entity_ids
})
def setup_platform(_hass, config, add_devices, _discovery_info=None):
    """
    Register the sensor with HomeAssistant
    """
    ids_to_check = config.get(CONF_ENTITY_IDS)
    add_devices([EntityIdChecker(ids_to_check)])

class EntityIdChecker(Entity):
    """
    Sensor that ensure the required Entity Ids have been detected by HomeAssistant
    """

    def __init__(self, ids_to_check):
        self._ids_to_check = ids_to_check
        self._state = LOADING_STATE
        self._missing_entities = []

    @property
    def name(self):
        return 'Entity Id Checker'

    @property
    def state(self):
        return self._state

    @property
    def entity_picture(self):
        if self._state == OK_STATE:
            return OK_ICON
        elif self._state == ERROR_STATE:
            return ERROR_ICON
        else:
            return LOADING_ICON

    def update(self):
        """
        Ensure that all required entity_ids are detected by Home Assistant.
        """
        all_entity_ids = self.hass.states.entity_ids()

        missing_entities = []
        for id in self._ids_to_check:
            if id not in all_entity_ids:
                missing_entities.append(id)

        if not missing_entities:
            self._state = OK_STATE
        else:
            self._state = ERROR_STATE
            for id in missing_entities:
                _LOGGER.error('Missing entity: %s', id)

        self._missing_entities = missing_entities

    @property
    def device_state_attributes(self):
        if self._state == ERROR_STATE:
            return {'Missing_Entities': self._missing_entities}
        return None
