---
layout: page
title: "Entity Checker Sensor"
description: "Get notified immediately if an Entity is missing from Home Assistant"
date: 2017-01-10 22:00
sidebar: true
comments: true
sharing: true
footer: true
logo: 'https://image.flaticon.com/icons/svg/291/291201.svg'
ha_category: Sensor
ha_iot_class: "Local Polling"
ha_release: ??
---

# Entity Checker

The `Entity Checker` sensor platform allows to be notified whenever a required **Entity** is missing, or has a wrong id.

No more time spent checking component after component to ensure the Entities have the correct id. 
`Entity Checker` does that for you.

Once the check is green, you are guaranteed all the **required entities** are detected with the **correct id**.
You can now focus on the business logic of you automation.

```yaml
# Example configuration.yaml entry
sensor:
  - platform: entity_checker
    entity_ids:
      - light.desk
      - light.bed
      - switch.kitchen
      - sensor.motion_kitchen
      - group.living_room
```

## Configuration variables:

- **entity_ids** (*Required*): List of id to check for

If an **Entity** from the list is not detected, or has a different `id`.  
The sensor will immediately become **red**, will throw an **Event** (TODO), and will **log the missing ids**.

> **Warning:**  
> This sensor does not inducate that the **Entity** works properly, only that the expected ids have 
> been correctly detected by Home Assistant.
> The corresponding **Entity** might still be malfunctioning. 
> The scope of this sensor is only to ensure there is no false assumptions on which `entity_ids` are
> safe to use for Automation.