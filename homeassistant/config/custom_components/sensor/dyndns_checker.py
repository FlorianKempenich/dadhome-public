"""
Sensor that ensure the DNS resolution works as expected
"""
import asyncio
import logging
from datetime import timedelta

import voluptuous as vol

from homeassistant.const import STATE_PROBLEM, STATE_UNKNOWN, STATE_OK
from homeassistant.components.sensor import PLATFORM_SCHEMA
import homeassistant.helpers.config_validation as cv
from homeassistant.helpers.entity import Entity

REQUIREMENTS = ['aiodns==1.1.1', 'ipgetter==0.7']
_LOGGER = logging.getLogger(__name__)

# TODO: Find a way to credit the author
# https://www.flaticon.com/free-icon/checked_291201#term=check&page=1&position=6
LOADING_ICON = 'https://image.flaticon.com/icons/svg/291/291242.svg'
OK_ICON = 'https://image.flaticon.com/icons/svg/291/291201.svg'
ERROR_ICON = 'https://image.flaticon.com/icons/svg/291/291202.svg'

SCAN_INTERVAL = timedelta(seconds=20) #TODO: Change back to 120s

CONF_DOMAIN = 'domain'
CONF_RESOLVER = 'resolver'
DEFAULT_RESOLVER = '8.8.8.8' # Google DNS
PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Required(CONF_DOMAIN): cv.string,
    vol.Optional(CONF_RESOLVER, default=DEFAULT_RESOLVER): cv.string
})
@asyncio.coroutine
def async_setup_platform(hass, config, async_add_devices, _discovery_info=None):
    """Set up the DNS IP sensor."""
    domain = config.get(CONF_DOMAIN)
    resolver = config.get(CONF_RESOLVER)

    async_add_devices([DynDnsChecker(hass, domain, resolver)], True)

class DynDnsChecker(Entity):
    """Implementation of a Dynamic DNS Checker sensor."""

    def __init__(self, hass, domain, resolver):
        """Initialize the sensor."""
        self.hass = hass # TODO: Try to remove, and directly use 'self.hass'
        self._state = STATE_UNKNOWN

        import aiodns
        self._resolver = aiodns.DNSResolver(loop=self.hass.loop)
        self._resolver.nameservers = [resolver]
        self._querytype = 'A'

        self._domain = domain
        self._external_ip = None
        self._resolved_ip = None


    @property
    def name(self):
        """Return the name of the sensor."""
        return 'DynDNS Checker'

    @property
    def state(self):
        """Return the status."""
        return self._state

    @property
    def entity_picture(self):
        if self._state == STATE_OK:
            return OK_ICON
        elif self._state == STATE_PROBLEM:
            return ERROR_ICON
        else:
            return LOADING_ICON

    @property
    def device_state_attributes(self):
        return {
            'Domain': self._domain,
            'External_IP': self._external_ip,
            'Resolved_IP': self._resolved_ip
        }

    @asyncio.coroutine
    def async_update(self):
        """Get the current External IP address"""
        import ipgetter
        ip = ipgetter.myip()
        if ip != '':
            self._external_ip = ip
        else:
            self._external_ip = STATE_PROBLEM


        """Get the current DNS IP address for hostname."""
        response = yield from self._resolver.query(self._domain, self._querytype)
        if response:
            self._resolved_ip = response[0].host
        else:
            _LOGGER.error('Could not resolve domain: %s', self._domain)
            self._resolved_ip = STATE_PROBLEM


        """Compute the state of the sensor"""
        if (self._external_ip == self._resolved_ip) and (self._external_ip != STATE_PROBLEM):
            self._state = STATE_OK
        else:
            self._state = STATE_PROBLEM
