---
layout: page
title: "DynDNS Sensor"
description: "Ensure your domain is pointing to this instance of Home Assistant"
date: 2017-01-13 22:00
sidebar: true
comments: true
sharing: true
footer: true
logo: 'https://image.flaticon.com/icons/svg/291/291201.svg'
ha_category: Sensor
ha_iot_class: "Cloud Polling"
ha_release: ??
---

# DynDNS Checker

The `DynDNS Checker` sensor platform ensures your domain is pointing to this instance of Home Assistant.

It compares the **external IP** this instance with the **IP resolved** via the given **domain**.

Listen to changes in state to get notified whenever there is a problem with your dynamic IP DNS provider.

```yaml
# Example configuration.yaml entry
sensor:
  - platform: dyndns_checker
    domain: my.homeassistant.com
```

## Configuration variables:

- **domain** (*Required*): Domain supposed to point to this instance of HomeAssistant.

> **Warning:**  
> This sensor does not indicate that the port forwarding and other network configuration work as expected.
> It only guarantees the given **domain** resolves to the correct IP, the external IP of this instance.