#!/bin/bash


SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

curl -X GET \
  -k \
  -H "Content-Type: application/json" \
  -H "x-ha-access: $(cat $SCRIPT_DIR/config/PASSWORD)" \
  https://dad-home.access.ly/api/stream
