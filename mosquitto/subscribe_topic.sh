#!/bin/bash
set -u

TOPIC=$1

read -p 'Type your password:' -s PASSWORD

mosquitto_sub \
    -h thehome.floriankempenich.com \
    -t "$TOPIC" \
    -p 8883 \
    --capath /etc/ssl/certs/ \
    -u "flo" \
    -P "$PASSWORD"

