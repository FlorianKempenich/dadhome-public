#!/bin/bash

# Claim token only valid & needed once.
# If config is reset, re-generate a token: https://www.plex.tv/claim/
CLAIM_TOKEN="claim-a8EtpypzFcd-E5MrUEDa"

# TODO:
# Transform to python script after working
# and load static ip from `config.yaml` & config (for domain)
HOST_IP="192.168.1.18"
# DOMAIN="dad-home.access.ly"

PLEX_ROOT="/mnt/TorrentHDD/plex"
PLEX_CONFIG="$PLEX_ROOT/config"
PLEX_TRANSCODE_CACHE="$PLEX_ROOT/transcode_cache"
PLEX_LIBRARY="$PLEX_ROOT/library"

PLEX_PORT=32400
PLEX_SERVER_NAME="DadHome"
PLEX_VERSION="1.19.3.2852-219a9974e"

docker kill plex
docker rm plex
docker run \
  -d \
  --name plex \
  --restart always \
  -p $PLEX_PORT:32400/tcp \
  -p 3005:3005/tcp \
  -p 8324:8324/tcp \
  -p 32469:32469/tcp \
  -p 1900:1900/udp \
  -p 32410:32410/udp \
  -p 32412:32412/udp \
  -p 32413:32413/udp \
  -p 32414:32414/udp \
  -e TZ="CEST" \
  -e ADVERTISE_IP="$HOST_IP:$PLEX_PORT" \
  -e PLEX_CLAIM="$CLAIM_TOKEN" \
  -h $PLEX_SERVER_NAME \
  -v $PLEX_CONFIG:/config \
  -v $PLEX_TRANSCODE_CACHE:/transcode \
  -v $PLEX_LIBRARY:/library \
  plexinc/pms-docker:$PLEX_VERSION

