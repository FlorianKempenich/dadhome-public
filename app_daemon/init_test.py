import click
import re

FLOOR='cave'

@click.command()
@click.argument('test_file_path', type=click.Path(exists=True))
def init_test(test_file_path):
    def to_camel_case(in_snake_case):
        words = room_name.split('_')
        for word in room_name.split('_'):
            word_cap = word.capitalize()
            words.remove(word)
            words.append(word_cap)
        return ''.join(words)

    def extract_room_name(test_file_name):
        print(test_file_name)
        regex = re.compile(r".*test_(.*)\.py")
        return regex.match(test_file_name).group(1)

    room_name = extract_room_name(test_file_path)
    print(room_name)
    room_camel_case = to_camel_case(room_name)
    print(room_camel_case)

    with open(test_file_path, 'w') as test_file:
        test_file.write("""\
import pytest
from apps.{floor} import {room_camel_case}

@pytest.fixture
def {room_name}(given_that):
    {room_name} = {room_camel_case}(
        None, None, None, None, None, None, None, None)
    {room_name}.initialize()
    given_that.mock_functions_are_cleared()
    return {room_name}

def test_bootstrap({room_name}):
    print('yo')

""".format(floor=FLOOR, room_name=room_name, room_camel_case=room_camel_case))


if __name__ == "__main__":
    init_test()
