#!/bin/bash

if [ "$1" == "only" ]; then
    echo Running Only Tests Marked By \`@pytest.mark.only\`
    pytest-watch \
        --ext=.py,.yaml \
        -- \
        -m only \
        --tb=short
else
    echo Running All Tests
    pytest-watch \
        --ext=.py,.yaml \
        -- \
        --tb=short
fi
