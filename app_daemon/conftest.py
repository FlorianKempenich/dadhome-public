import pytest

from apps.common.constants import LIGHT
from apps.common.constants import TIMES
from apps.common.types import LightMode
from hypothesis import settings, HealthCheck


@pytest.fixture
def dawn(given_that):
    given_that.time_is(TIMES['dawn'])


@pytest.fixture
def day(given_that):
    given_that.time_is(TIMES['day'])


@pytest.fixture
def evening(given_that):
    # TODO: Maybe replace with sunset(?)
    given_that.time_is(TIMES['evening'])


@pytest.fixture
def night(given_that):
    given_that.time_is(TIMES['night'])


@pytest.fixture(params=[
    'dawn',
    'day',
    'evening',
    'night'])
def all_times(request):
    fixture_name = request.param
    request.getfixturevalue(fixture_name)


@pytest.fixture
def helper(assert_that):
    class Helper:
        def assert_turned_on_with_given_light_mode(self, light,
                                                   light_mode: LightMode):
            def should_be_using_color():
                return light.has_color and 'color_name' in LIGHT[light_mode]

            if light.has_brightness and should_be_using_color():
                assert_that(light.entity_id).was.turned_on(
                        brightness_pct=LIGHT[light_mode]['brightness_pct'],
                        color_name=LIGHT[light_mode]['color_name'])
            elif light.has_brightness and light.has_color_temperature:
                assert_that(light.entity_id).was.turned_on(
                        brightness_pct=LIGHT[light_mode]['brightness_pct'],
                        color_temp=LIGHT[light_mode]['color_temp'])
            elif light.has_brightness:
                assert_that(light.entity_id).was.turned_on(
                        brightness_pct=LIGHT[light_mode]['brightness_pct'])
            else:
                assert_that(light.entity_id).was.turned_on()

    return Helper()


def register_hypothesis_profiles():
    def with_default(**other_options):
        default_options = {
            'suppress_health_check': HealthCheck.all()
        }
        return {**default_options, **other_options}

    settings.register_profile("tdd",
                              **with_default(max_examples=10))
    settings.register_profile("only",
                              **with_default(max_examples=300))
    settings.register_profile("before_release",
                              **with_default(max_examples=1000))


register_hypothesis_profiles()
