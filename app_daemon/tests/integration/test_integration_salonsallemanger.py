import pytest
from appdaemontestframework import automation_fixture

from apps.common.constants import EVENTS, IDS, LIGHT
from apps.floors.rezchaussee import SalonSalleManger
from apps.room_templates import HassRoom


@automation_fixture(SalonSalleManger)
def salon_salle_manger():
    pass


@pytest.fixture
def init_motion_sensors_to_off(given_that):
    given_that.state_of(IDS['rezchaussee.salonsallemanger.motion']).is_set_to('off')


@pytest.fixture
def init_room_state_to_off(given_that):
    given_that.state_of(IDS['rezchaussee.salonsallemanger.room_state']).is_set_to('off')


@pytest.fixture
def when_new(given_that, salon_salle_manger: HassRoom):
    class WhenNewWrapper:
        def motion(self):
            salon_salle_manger._new_motion(None, None, None)

        def no_more_motion(self):
            salon_salle_manger._no_more_motion(None, None, None, None, None)

        def room_state(self, new_room_state):
            given_that.state_of(IDS['rezchaussee.salonsallemanger.room_state']).is_set_to(new_room_state)
            salon_salle_manger._new_room_state(entity=None,
                                               _attribute=None,
                                               _old=None,
                                               new=new_room_state,
                                               _kwargs=None)

        def cube_action(self, cube_action, rotate_value=None):
            if rotate_value and cube_action != 'rotate':
                raise ValueError('Should not happen')
            salon_salle_manger._new_cube_action(_event=None,
                                                data={'action_type': cube_action, 'action_value': rotate_value},
                                                _kwargs=None)

    return WhenNewWrapper()


@pytest.mark.usefixtures('init_motion_sensors_to_off', 'init_room_state_to_off', 'day')
class TestIntegrationBedroom:

    def test_callbacks_are_registered(self, salon_salle_manger, assert_that):
        for cube_entity_id in [IDS['rezchaussee.salonsallemanger.magic_cube.1'],
                               IDS['rezchaussee.salonsallemanger.magic_cube.2'],
                               IDS['rezchaussee.salonsallemanger.magic_cube.3'],
                               IDS['rezchaussee.salonsallemanger.magic_cube.4']]:
            assert_that(salon_salle_manger) \
                .listens_to.event(EVENTS['cube_action'],
                                  entity_id=cube_entity_id) \
                .with_callback(salon_salle_manger._new_cube_action)

        assert_that(salon_salle_manger) \
            .listens_to.state(IDS['rezchaussee.salonsallemanger.room_state']) \
            .with_callback(salon_salle_manger._new_room_state)

    def test_automatic_lights_with_room_state_off(self):
        # Do nothing - already tested, see: 'TestIntegrationAutomaticLights'
        pass

    def test_real_scenario(self, given_that, when_new, assert_that, helper):
        # Time is day, start state is 'off'
        brightness = LIGHT['day']['brightness_pct']
        color_temp = LIGHT['day']['color_temp']
        room_state_id = IDS['rezchaussee.salonsallemanger.room_state']

        # Acts as automatic lights in 'off' state
        when_new.motion()
        assert_that(IDS['rezchaussee.salonsallemanger.light.salon_plafond.1']).was.turned_on(brightness_pct=brightness,
                                                                                             color_temp=color_temp)
        assert_that(IDS['rezchaussee.salonsallemanger.light.ambiance_1.1']).was.turned_on(brightness_pct=brightness,
                                                                                          color_temp=color_temp)
        given_that.mock_functions_are_cleared()
        when_new.no_more_motion()
        assert_that(IDS['rezchaussee.salonsallemanger.light.salon_plafond.1']).was.turned_off()
        assert_that(IDS['rezchaussee.salonsallemanger.light.ambiance_1.1']).was.turned_off()

        # Cube switches to 'day' state - Flip 90deg
        when_new.cube_action('flip90')
        assert_that('input_select/select_option').was.called_with(entity_id=room_state_id, option='day')
        given_that.mock_functions_are_cleared()
        when_new.room_state('day')
        assert_that('scene/turn_on').was.called_with(entity_id=IDS['rezchaussee.salonsallemanger.scene.day'])

        # In 'day' state -> No automatic lights
        given_that.mock_functions_are_cleared()
        when_new.motion()
        assert_that(IDS['rezchaussee.salonsallemanger.light.salon_plafond.1']).was_not.turned_on(
            brightness_pct=brightness,
            color_temp=color_temp)
        assert_that(IDS['rezchaussee.salonsallemanger.light.ambiance_1.1']).was_not.turned_on(brightness_pct=brightness,
                                                                                              color_temp=color_temp)

        # Cube switches to 'evening' state - Rotates
        when_new.cube_action('rotate', 35)
        assert_that('input_select/select_option').was.called_with(entity_id=room_state_id, option='evening')
        given_that.mock_functions_are_cleared()
        when_new.room_state('evening')
        assert_that('scene/turn_on').was.called_with(entity_id=IDS['rezchaussee.salonsallemanger.scene.evening'])

        # Cube switches to 'day' state - Flip 90
        when_new.cube_action('flip90')
        assert_that('input_select/select_option').was.called_with(entity_id=room_state_id, option='day')
        given_that.mock_functions_are_cleared()
        when_new.room_state('day')
        assert_that('scene/turn_on').was.called_with(entity_id=IDS['rezchaussee.salonsallemanger.scene.day'])

        # Cube toggles Lecture - Flip 180
        when_new.cube_action('flip180')
        # TODO: ADD IN APPDAEMONTESTFRAMWORK --> `assert_that(THING).was.toggled()
        # TODO: ADD IN APPDAEMONTESTFRAMWORK --> `assert_that(THING).was.toggled()
        # TODO: ADD IN APPDAEMONTESTFRAMWORK --> `assert_that(THING).was.toggled()
        assert_that('switch/toggle').was.called_with(entity_id=IDS['rezchaussee.salonsallemanger.light.lecture'])

        # Cube switches to 'off' state - Shake Air
        when_new.cube_action('shake_air')
        assert_that('input_select/select_option').was.called_with(entity_id=room_state_id, option='off')
        given_that.mock_functions_are_cleared()
        when_new.room_state('off')
        assert_that('scene/turn_on').was.called_with(entity_id=IDS['rezchaussee.salonsallemanger.scene.off'])
