from datetime import time

import pytest
from appdaemontestframework import automation_fixture
from pytest import param, mark

from apps.common.constants import EVENTS, IDS, TIMES
from apps.common.types import Light, MotionSensor
from apps.floors.etage import Papa, Flo, Geo

SALLE_BAIN_AND_COULOIR_LIGHTS = [
    Light(IDS['etage.couloir.gateway'], has_color=True),
    Light(IDS['etage.sallebain.light.plafond'], has_color=True)]
SALLE_BAIN_AND_COULOIR_MOTION = [MotionSensor(IDS['etage.sallebain.motion']),
                                 MotionSensor(IDS['etage.couloir.motion'])]


@automation_fixture(
        (Papa, {'motion_sensors':       [IDS['etage.papa.motion']],
                'switches':             [IDS['etage.papa.switch.1'],
                                         IDS['etage.papa.switch.2'],
                                         IDS['etage.papa.switch.3'],
                                         IDS['etage.papa.switch.4']],
                'auto_lights':          [
                    Light(IDS['etage.papa.light.plafond'], has_color=True)],
                'night_lights':         [
                    Light(IDS['etage.couloir.gateway'], has_color=True),
                    Light(IDS['etage.sallebain.light.plafond'],
                          has_color=True)],
                'room_state_entity_id': IDS['etage.papa.room_state'],
                'is_upstairs':          True}),
        (Flo, {'motion_sensors':       [IDS['etage.flo.motion']],
               'switches':             [IDS['etage.flo.switch']],
               'auto_lights':          [Light(IDS['etage.flo.light.ambiance'],
                                              has_color=False,
                                              has_color_temperature=False,
                                              has_brightness=True)],
               'night_lights':         [
                   Light(IDS['etage.couloir.gateway'], has_color=True),
                   Light(IDS['etage.sallebain.light.plafond'], has_color=True)],
               'room_state_entity_id': IDS['etage.flo.room_state'],
               'is_upstairs':          True}),
        (Geo, {'motion_sensors':       [IDS['etage.geo.motion']],
               'switches':             [IDS['etage.geo.switch']],
               'auto_lights':          [Light(IDS['etage.geo.light.plafond'],
                                              has_color=False,
                                              has_color_temperature=False,
                                              has_brightness=False)],
               'night_lights':         [
                   Light(IDS['etage.couloir.gateway'], has_color=True),
                   Light(IDS['etage.sallebain.light.plafond'], has_color=True)],
               'room_state_entity_id': IDS['etage.geo.room_state'],
               'is_upstairs':          True}))
def bedroom_with_params():
    pass


@automation_fixture(Papa)
def dad_bedroom(given_that):
    given_that.state_of(IDS['etage.papa.motion']).is_set_to('off')
    given_that.state_of(IDS['etage.papa.room_state']).is_set_to('Normal')


@pytest.fixture
def bedroom(bedroom_with_params):
    return bedroom_with_params[0]


@pytest.fixture
def test_params(bedroom_with_params):
    return bedroom_with_params[1]


@pytest.fixture
def start_in_normal_state(given_that, test_params):
    given_that.state_of(test_params['room_state_entity_id']).is_set_to('Normal')


@pytest.fixture
def init_motion_sensors_to_off(given_that, test_params):
    for motion_sensor in test_params['motion_sensors']:
        given_that.state_of(motion_sensor).is_set_to('off')

    for motion_sensor in SALLE_BAIN_AND_COULOIR_MOTION:
        given_that.state_of(motion_sensor.entity_id).is_set_to('off')


@pytest.fixture
def when_new(given_that, bedroom, test_params):
    class WhenNewWrapper:
        def motion(self):
            bedroom._new_motion(None, None, None)

        def no_more_motion(self):
            bedroom._no_more_motion(None, None, None, None, None)

        def click(self):
            bedroom._new_click(None, None, None)

        def time(self, new_time):
            given_that.time_is(new_time)
            bedroom._new_time({'trigger_time': time(hour=10)})

    return WhenNewWrapper()


@pytest.mark.usefixtures('init_motion_sensors_to_off', 'start_in_normal_state')
class TestIntegrationBedroom:

    def test_callbacks_are_registered(self, bedroom, assert_that,
                                      hass_functions, test_params):
        for button in test_params.get('buttons', []):
            assert_that(bedroom) \
                .listens_to.event(EVENTS['xiaomi_click'], entity_id=button,
                                  click_type='single') \
                .with_callback(bedroom._new_click)
        for switch in test_params.get('switches', []):
            assert_that(bedroom) \
                .listens_to.event(EVENTS['xiaomi_click'], entity_id=switch) \
                .with_callback(bedroom._new_click)

        for motion_sensor in test_params.get('motion_sensors', []):
            assert_that(bedroom) \
                .listens_to.event(EVENTS['motion'], entity_id=motion_sensor) \
                .with_callback(bedroom._new_motion)
            assert_that(bedroom) \
                .listens_to.state(motion_sensor, new='off', old='on') \
                .with_callback(bedroom._no_more_motion)

        reset_time = TIMES['reset_to_normal_state']
        assert_that(bedroom) \
            .registered.run_daily(reset_time, trigger_time=reset_time) \
            .with_callback(bedroom._new_time)

    def test_normal(self, given_that, when_new, assert_that, helper,
                    test_params, bedroom):
        if bedroom.__class__ == Papa:
            pytest.skip('Covered by TestDadBedroom')
        auto_lights = test_params['auto_lights']

        given_that.time_is(TIMES['day'])
        given_that.mock_functions_are_cleared()
        when_new.motion()
        for light in auto_lights:
            helper.assert_turned_on_with_given_light_mode(light, 'day')

        given_that.mock_functions_are_cleared()
        when_new.no_more_motion()
        for light in auto_lights:
            assert_that(light.entity_id).was.turned_off()

        given_that.time_is(TIMES['evening'])
        given_that.mock_functions_are_cleared()
        when_new.motion()
        for light in auto_lights:
            helper.assert_turned_on_with_given_light_mode(light, 'evening')

        given_that.mock_functions_are_cleared()
        when_new.no_more_motion()
        for light in auto_lights:
            assert_that(light.entity_id).was.turned_off()

    @pytest.mark.parametrize('sleep_start_time, expected_night_lights_mode', [
        param(time(hour=11), 'sleep_after_day', id='Day'),
        param(time(hour=21), 'sleep_before_day', id='Evening'),
        param(time(hour=3), 'sleep_before_day', id='Night'),
    ])
    def test_sleep_concluded_by_click(self, given_that, when_new, assert_that,
                                      helper, test_params, bedroom,
                                      sleep_start_time,
                                      expected_night_lights_mode):
        auto_lights = test_params['auto_lights']
        night_lights = test_params['night_lights']
        room_state_entity_id = test_params['room_state_entity_id']

        # given_that.time_is(TIMES['evening'])
        given_that.time_is(sleep_start_time)

        when_new.click()
        assert_that('input_select/select_option').was.called_with(
                entity_id=room_state_entity_id, option='Sommeil')
        given_that.state_of(room_state_entity_id).is_set_to('Sommeil')

        given_that.mock_functions_are_cleared()
        when_new.motion()
        for light in night_lights:
            helper.assert_turned_on_with_given_light_mode(light,
                                                          expected_night_lights_mode)
        when_new.no_more_motion()
        for light in night_lights:
            assert_that(light.entity_id).was.turned_off()

        given_that.time_is(TIMES['dawn'])
        given_that.mock_functions_are_cleared()
        when_new.click()
        for light in auto_lights:
            helper.assert_turned_on_with_given_light_mode(light, 'dawn')
        assert_that('input_select/select_option').was.called_with(
                entity_id=room_state_entity_id, option='Normal')
        given_that.state_of(room_state_entity_id).is_set_to('Normal')

    @pytest.mark.parametrize('sleep_start_time, expected_night_lights_mode', [
        param(time(hour=11), 'sleep_after_day', id='Day'),
        param(time(hour=21), 'sleep_before_day', id='Evening'),
        param(time(hour=3), 'sleep_before_day', id='Night'),
    ])
    def test_sleep_concluded_by_time(self, given_that, when_new, assert_that,
                                     helper, test_params, bedroom,
                                     sleep_start_time,
                                     expected_night_lights_mode):
        night_lights = test_params['night_lights']
        room_state_entity_id = test_params['room_state_entity_id']

        given_that.time_is(sleep_start_time)

        when_new.click()
        assert_that('input_select/select_option').was.called_with(
                entity_id=room_state_entity_id, option='Sommeil')
        given_that.state_of(room_state_entity_id).is_set_to('Sommeil')

        given_that.mock_functions_are_cleared()
        when_new.motion()
        for light in night_lights:
            helper.assert_turned_on_with_given_light_mode(light,
                                                          expected_night_lights_mode)
        when_new.no_more_motion()
        for light in night_lights:
            assert_that(light.entity_id).was.turned_off()

        given_that.time_is(TIMES['reset_to_normal_state'])
        given_that.mock_functions_are_cleared()
        when_new.time(TIMES['reset_to_normal_state'])
        assert_that('input_select/select_option').was.called_with(
                entity_id=room_state_entity_id, option='Normal')
        given_that.state_of(room_state_entity_id).is_set_to('Normal')

    @pytest.mark.usefixtures('upstairs_room_only')
    class TestRoomsUpstairs:

        @pytest.fixture
        def upstairs_room_only(self, bedroom, test_params):
            if not test_params['is_upstairs']:
                pytest.skip(
                        f"These tests are only for upstairs room. Skipping for '{str(bedroom.__class__)}'")

        def test_all_room_upstairs_have_bathroom_and_couloir_as_night_lights(
                self, test_params):
            assert test_params['night_lights'] == SALLE_BAIN_AND_COULOIR_LIGHTS

        def test_during_sleep__bathroom_and_couloir_turn_off_only_if_no_motion_in_their_room(
                self,
                given_that,
                when_new,
                assert_that,
                helper,
                test_params,
                bedroom):
            # This solution isn't entirely perfect.
            # Conditions might happen where either the bathroom or Couloir light remain on even after no more
            # motion anywhere. But this will be reset whenever the sleep mode is exited.
            # The refactor required to implement more granularity isn't worth given the rarity of this event ever
            # being noticed.
            # Conditions for 'bug' to happen:
            # - Motion in Room
            # - Motion in either Couloir or Bathroom (not both) - Let's pick Couloir as example
            # - No more motion in Room
            # - (Both Lights remain on because still motion in Couloir)
            # - No more motion in Couloir
            # - Couloir automatic lights takes over and turns off the light
            # - Bathroom . . . remains on "forever" (until sleep is manually/automatically exited)

            given_that.time_is(TIMES['evening'])
            given_that.state_of(test_params['room_state_entity_id']).is_set_to(
                    'Sommeil')

            when_new.motion()
            for light in SALLE_BAIN_AND_COULOIR_LIGHTS:
                helper.assert_turned_on_with_given_light_mode(light,
                                                              'sleep_before_day')

            given_that.mock_functions_are_cleared()
            given_that.state_of(
                    SALLE_BAIN_AND_COULOIR_MOTION[0].entity_id).is_set_to('off')
            given_that.state_of(
                    SALLE_BAIN_AND_COULOIR_MOTION[1].entity_id).is_set_to('on')
            when_new.no_more_motion()
            for light in SALLE_BAIN_AND_COULOIR_LIGHTS:
                assert_that(light.entity_id).was_not.turned_off()

            given_that.mock_functions_are_cleared()
            given_that.state_of(
                    SALLE_BAIN_AND_COULOIR_MOTION[0].entity_id).is_set_to('on')
            given_that.state_of(
                    SALLE_BAIN_AND_COULOIR_MOTION[1].entity_id).is_set_to('off')
            when_new.no_more_motion()
            for light in SALLE_BAIN_AND_COULOIR_LIGHTS:
                assert_that(light.entity_id).was_not.turned_off()

            given_that.mock_functions_are_cleared()
            given_that.state_of(
                    SALLE_BAIN_AND_COULOIR_MOTION[0].entity_id).is_set_to('off')
            given_that.state_of(
                    SALLE_BAIN_AND_COULOIR_MOTION[1].entity_id).is_set_to('off')
            when_new.no_more_motion()
            for light in SALLE_BAIN_AND_COULOIR_LIGHTS:
                assert_that(light.entity_id).was.turned_off()


class TestDadBedroom:
    @mark.only
    def test_during_evening_automatic_light_stays_on_for_2_hours(self,
                                                                 given_that,
                                                                 helper,
                                                                 assert_that,
                                                                 time_travel,
                                                                 dad_bedroom):
        # It is during the day
        given_that.time_is(TIMES['day'])
        # Someone enters the room
        given_that.state_of(IDS['etage.papa.motion']).is_set_to('on')
        dad_bedroom._new_motion(None, None, None)
        # The light turns on
        helper.assert_turned_on_with_given_light_mode(
                Light(IDS['etage.papa.light.plafond'], has_color=True),
                'day'
        )
        # When no more motion, lights turns off
        given_that.state_of(IDS['etage.papa.motion']).is_set_to('off')
        dad_bedroom._no_more_motion(None, None, None, None, None)
        assert_that(IDS['etage.papa.light.plafond']).was.turned_off()

        # Now it is evening time
        given_that.time_is(TIMES['evening'])
        given_that.mock_functions_are_cleared()
        # Someone enters the room
        dad_bedroom._new_motion(None, None, None)
        # The light turns on
        helper.assert_turned_on_with_given_light_mode(
                Light(IDS['etage.papa.light.plafond'], has_color=True),
                'evening'
        )
        given_that.mock_functions_are_cleared()
        # When no more motion, lights DOES NOT TURN OFF YET
        given_that.state_of(IDS['etage.papa.motion']).is_set_to('off')
        dad_bedroom._no_more_motion(None, None, None, None, None)
        assert_that(IDS['etage.papa.light.plafond']).was_not.turned_off()
        # After 2 hours without motion, light finally turns off
        time_travel.fast_forward(120 - 1).minutes()
        assert_that(IDS['etage.papa.light.plafond']).was_not.turned_off()
        time_travel.fast_forward(1).minutes()
        assert_that(IDS['etage.papa.light.plafond']).was.turned_off()
