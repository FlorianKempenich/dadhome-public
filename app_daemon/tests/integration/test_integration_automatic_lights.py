import pytest
from pytest import mark
from appdaemontestframework import automation_fixture

from apps.common.constants import TIMES, LIGHT, IDS, EVENTS
from apps.common.types import Light, MotionSensor, LightMode
from apps.floors import soussol, etage, cave, rezchaussee

SALLE_BAIN_AND_COULOIR_MOTION = [MotionSensor(IDS['etage.sallebain.motion']),
                                 MotionSensor(IDS['etage.couloir.motion'])]


@automation_fixture(
    (etage.Couloir, {'motion_sensors': [IDS['etage.couloir.motion']],
                     'lights': [Light(IDS['etage.couloir.light.plafond'])]}),
    (etage.Flo, {'motion_sensors': [IDS['etage.flo.motion']],
                 'lights': [Light(IDS['etage.flo.light.ambiance'],
                                  has_color_temperature=False)]}),
    (etage.Geo, {'motion_sensors': [IDS['etage.geo.motion']],
                 'lights': [Light(IDS['etage.geo.light.plafond'],
                                  has_color_temperature=False,
                                  has_brightness=False)]}),
    (etage.SalleBain, {'motion_sensors': [IDS['etage.sallebain.motion']],
                       'lights': [Light(IDS['etage.sallebain.light.plafond'],
                                        has_color=True)]}),
    (rezchaussee.Cuisine, {'motion_sensors': [IDS['rezchaussee.cuisine.motion.1'],
                                              IDS['rezchaussee.cuisine.motion.2']],
                           'lights': [Light(IDS['rezchaussee.cuisine.light.spots'],
                                            has_color_temperature=False,
                                            has_brightness=False)]}),
    (rezchaussee.Toilettes, {'motion_sensors': [IDS['rezchaussee.toilettes.motion']],
                             'lights': [Light(IDS['rezchaussee.toilettes.light.plafond'])]}),
    (rezchaussee.SalonSalleManger, {'motion_sensors': [IDS['rezchaussee.salonsallemanger.motion']],
                                    'lights': [Light(IDS['rezchaussee.salonsallemanger.light.salon_plafond.1']),
                                               Light(IDS['rezchaussee.salonsallemanger.light.ambiance_1.1'])]}),
    (soussol.Buanderie, {'motion_sensors': [IDS['soussol.buanderie.motion']],
                         'lights': [Light(IDS['soussol.buanderie.light.plafond'])],
                         'always_day_mode': True}),
    (soussol.Bureau, {'motion_sensors': [IDS['soussol.bureau.motion.1'],
                                         IDS['soussol.bureau.motion.2']],
                      'lights': [Light(IDS['soussol.bureau.light.plafond'])]}),
    (soussol.Couloir, {'motion_sensors': [IDS['soussol.couloir.motion']],
                       'lights': [Light(IDS['soussol.couloir.light.plafond'])]}),
    (cave.Atelier, {'motion_sensors': [IDS['cave.atelier.motion']],
                    'lights': [Light(IDS['cave.atelier.light.neons'],
                                     has_color_temperature=False,
                                     has_brightness=False)],
                    'extra_delay_in_min': 5}),
    (cave.CaveMilieu, {'motion_sensors': [IDS['cave.cavemilieu.motion']],
                       'lights': [Light(IDS['cave.cavemilieu.light.plafond'],
                                        has_color_temperature=False,
                                        has_brightness=False)]}),
    (cave.CaveVin, {'motion_sensors': [IDS['cave.cavevin.motion']],
                    'lights': [Light(IDS['cave.cavevin.light.plafond'],
                                     has_color_temperature=False,
                                     has_brightness=False)]}),
)
def automation_with_args(given_that):
    given_that.state_of(IDS['etage.papa.room_state']).is_set_to('Normal')
    given_that.state_of(IDS['etage.flo.room_state']).is_set_to('Normal')
    given_that.state_of(IDS['etage.geo.room_state']).is_set_to('Normal')
    given_that.state_of(IDS['rezchaussee.salonsallemanger.room_state']).is_set_to('off')
    given_that.state_of(IDS['rezchaussee.salonsallemanger.home_theater']).is_set_to('on')


@pytest.fixture
def automation(automation_with_args):
    return automation_with_args[0]


@pytest.fixture
def lights(automation_with_args):
    return automation_with_args[1].get('lights', ())


@pytest.fixture
def motion_sensors(given_that, automation_with_args):
    return automation_with_args[1]['motion_sensors']


@pytest.fixture
def init_motion_sensors_to_off(given_that, motion_sensors):
    for sensor in motion_sensors:
        given_that.state_of(sensor).is_set_to('off')
    for sensor in SALLE_BAIN_AND_COULOIR_MOTION:
        given_that.state_of(sensor.entity_id).is_set_to('off')


@pytest.fixture
def always_day_mode(automation_with_args):
    return automation_with_args[1].get('always_day_mode', False)


@pytest.fixture
def extra_delay_in_min(automation_with_args):
    delay = automation_with_args[1].get('extra_delay_in_min', 0)
    return delay


@pytest.mark.usefixtures('init_motion_sensors_to_off')
class TestIntegrationAutomaticLights:

    def test_callbacks_are_registered(self, automation, motion_sensors, assert_that):
        for motion_sensor in motion_sensors:
            assert_that(automation) \
                .listens_to.event(EVENTS['motion'], entity_id=motion_sensor) \
                .with_callback(automation._new_motion)
            assert_that(automation) \
                .listens_to.state(motion_sensor, new='off', old='on') \
                .with_callback(automation._no_more_motion)

    def test_real_scenario(self, helper, automation, given_that, assert_that, motion_sensors, lights, always_day_mode,
                           extra_delay_in_min, time_travel):
        # Starting during the day
        given_that.time_is(TIMES['day'])
        automation._new_motion(None, None, None)
        for light in lights:
            helper.assert_turned_on_with_given_light_mode(light,  'day')

        # Time is now evening
        given_that.time_is(TIMES['evening'])
        given_that.mock_functions_are_cleared()
        automation._new_motion(None, None, None)
        for light in lights:
            if always_day_mode:
                helper.assert_turned_on_with_given_light_mode(light, 'day')
            else:
                helper.assert_turned_on_with_given_light_mode(light, 'evening')

        # No more motion - one sensor still on
        for sensor in motion_sensors:
            given_that.state_of(sensor).is_set_to('off')
        given_that.state_of(motion_sensors[0]).is_set_to('on')
        given_that.mock_functions_are_cleared()
        automation._no_more_motion(None, None, None, None, None)
        time_travel.fast_forward(extra_delay_in_min).minutes()
        for light in lights:
            assert_that(light.entity_id).was_not.turned_off()

        # No more motion - all sensors off
        given_that.state_of(motion_sensors[0]).is_set_to('off')
        automation._no_more_motion(None, None, None, None, None)
        time_travel.fast_forward(extra_delay_in_min).minutes()
        for light in lights:
            assert_that(light.entity_id).was.turned_off()

    @mark.usefixtures('day', 'init_motion_sensors_to_off')
    def test_lights_only_turned_off_after_extra_delay(self, lights, automation, given_that, assert_that, time_travel,
                                                      extra_delay_in_min):
        if extra_delay_in_min == 0:
            pytest.skip("Nothing to test")
        automation._no_more_motion(None, None, None, None, None)

        time_travel.fast_forward(extra_delay_in_min - 1).minutes()
        for light in lights:
            assert_that(light.entity_id).was_not.turned_off()

        time_travel.fast_forward(1).minutes()
        for light in lights:
            assert_that(light.entity_id).was.turned_off()
