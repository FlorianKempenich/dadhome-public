import pytest
from appdaemontestframework import automation_fixture
from mock import patch

from apps.common.constants import IDS
from apps.floors.etage import SalleBain
from apps.floors.rezchaussee import Cuisine
from apps.floors.soussol import Bureau


@pytest.fixture
def sleep_mode(given_that):
    given_that.state_of(IDS['etage.papa.room_state']).is_set_to('Sommeil')


@pytest.fixture
def normal_mode(given_that):
    given_that.state_of(IDS['etage.papa.room_state']).is_set_to('Normal')


@pytest.fixture(params=[
    'normal_mode',
    'sleep_mode'])
def all_modes(given_that, request):
    fixture_name = request.param
    request.getfixturevalue(fixture_name)


@automation_fixture(
    (SalleBain, {'secondary_manual_light': IDS['etage.sallebain.light.neon'],
                 'motion_sensors': [IDS['etage.sallebain.motion']]}),
    (Cuisine, {'secondary_manual_light': IDS['rezchaussee.cuisine.light.plafond'],
               'motion_sensors': [IDS['rezchaussee.cuisine.motion.1'],
                                  IDS['rezchaussee.cuisine.motion.2']]}),
    (Bureau, {'secondary_manual_light': IDS['soussol.bureau.light.neons'],
              'motion_sensors': [IDS['soussol.bureau.motion.1'],
                                 IDS['soussol.bureau.motion.2']]}))
def automation_with_args():
    pass


@pytest.fixture
def automation(automation_with_args):
    return automation_with_args[0]


@pytest.fixture
def secondary_manual_light(automation_with_args):
    return automation_with_args[1]['secondary_manual_light']


@pytest.fixture
def init_motion_sensors_to_off(given_that, automation_with_args):
    for motion_sensor in automation_with_args[1]['motion_sensors']:
        given_that.state_of(motion_sensor).is_set_to('off')


@pytest.fixture
def patch_default_timeout_for_all_modules():
    timeout_required_for_test = 120

    all_default_timeout_versions = [
        'apps.floors.etage.DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S',
        'apps.floors.rezchaussee.DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S',
        'apps.floors.soussol.DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S']

    default_timeout_patches = list(map(lambda to_patch: patch(to_patch, timeout_required_for_test),
                                       all_default_timeout_versions))

    for default_timeout_patch in default_timeout_patches:
        default_timeout_patch.start()
    yield
    for default_timeout_patch in default_timeout_patches:
        default_timeout_patch.stop()


@pytest.mark.usefixtures('patch_default_timeout_for_all_modules',
                         'all_modes',
                         'all_times',
                         'init_motion_sensors_to_off')
class TestIntegrationManualLightsOnTimeout:

    def test_full_scenario(self, automation, assert_that, time_travel, secondary_manual_light):
        def new_motion():
            automation._new_motion(None, None, None)

        def no_more_motion():
            automation._no_more_motion(None, None, None, None, None)

        def new_click():
            automation._new_click(None, None, None)

        # For clarity sake, this test assumes 'DELAY_FOR_SECONDARY_LIGHTS_IN_S' is set to 120
        # The constant is patched to ensure that '@patch('apps.common.DELAY_FOR_SECONDARY_LIGHTS_IN_S', 120)'
        time_travel.assert_current_time(0).minutes()
        no_more_motion()

        time_travel.fast_forward(30).seconds()
        time_travel.assert_current_time(30).seconds()
        new_motion()

        time_travel.fast_forward(15).seconds()
        time_travel.assert_current_time(45).seconds()
        no_more_motion()

        time_travel.fast_forward(60).seconds()
        time_travel.assert_current_time(105).seconds()  # 1min 45sec
        new_motion()

        time_travel.fast_forward(15).seconds()
        time_travel.assert_current_time(120).seconds()  # 2min
        assert_that(secondary_manual_light).was_not.turned_off()

        time_travel.fast_forward(45).seconds()
        time_travel.assert_current_time(165).seconds()  # 2min 45sec
        assert_that(secondary_manual_light).was_not.turned_off()

        new_click()
        assert_that('switch/toggle').was.called_with(entity_id=secondary_manual_light)

        time_travel.fast_forward(15).seconds()
        time_travel.assert_current_time(180).seconds()  # 3min
        no_more_motion()

        time_travel.fast_forward(119).seconds()
        time_travel.assert_current_time(299).seconds()  # 4min 59s
        assert_that(secondary_manual_light).was_not.turned_off()

        time_travel.fast_forward(1).seconds()
        time_travel.assert_current_time(300).seconds()  # 5min
        assert_that(secondary_manual_light).was.turned_off()
