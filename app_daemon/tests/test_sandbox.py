import pytest
from hypothesis import given
from hypothesis.strategies import integers
from mock import patch

from apps.sandbox import PropertyBasedSandbox


class TestPropertyBased:
    @pytest.fixture
    def pb(self):
        return PropertyBasedSandbox()

    class TestMinus:
        @given(integers(), integers(min_value=0))
        def test_second_is_positive__result_always_lower_than_first(self, pb, first, second):
            assert pb.minus(first, second) <= first

    @patch('apps.sandbox.random.randint')
    @given(integers(min_value=0), integers(min_value=0))
    def test_sum_always_bigger_than_first(self, randint, pb, a, b):
        randint.side_effect = [a, b]
        sum = pb.sum_rand_numbers()
        assert sum >= a

    @patch('apps.sandbox.random.randint')
    @given(integers(min_value=0), integers(min_value=0))
    def test_sum_always_bigger_than_second(self, randint, pb, a, b):
        randint.side_effect = [a, b]
        sum = pb.sum_rand_numbers()
        assert sum >= b
