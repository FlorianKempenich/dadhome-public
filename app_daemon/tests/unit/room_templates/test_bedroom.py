from datetime import time

import pytest
from pytest import param
from appdaemontestframework import automation_fixture

from apps.common.constants import LIGHT, PERIODS, EVENTS
from apps.common.types import Light, XiaomiSwitch, XiaomiButton, MotionSensor, RoomState
from apps.room_templates import BedroomRoom

MOCK_ROOM_STATE = RoomState('bedroom')
MOCK_AUTOMATIC_LIGHTS = [Light('auto_light'),
                         Light('auto_light_wo_color_tmp',
                               has_color_temperature=False),
                         Light('auto_light_wo_brightness',
                               has_brightness=False),
                         Light('auto_light_wo_color_tmp_or_brightness',
                               has_color_temperature=False,
                               has_brightness=False),
                         Light('auto_and_night_light')]
MOCK_NIGHT_LIGHTS = [Light('night_light'),
                     Light('night_light_wo_color_tmp',
                           has_color_temperature=False),
                     Light('night_light_wo_brightness',
                           has_brightness=False),
                     Light('night_light_wo_color_tmp_or_brightness',
                           has_color_temperature=False,
                           has_brightness=False),
                     Light('auto_and_night_light')]
MOCK_SWITCHES = [XiaomiSwitch('switch1'), XiaomiSwitch('switch2')]
MOCK_BUTTONS = [XiaomiButton('button1'), XiaomiButton('button2')]
MOCK_MOTION_SENSORS = [MotionSensor('motion_sensor1'), MotionSensor('motion_sensor2')]
MOCK_RESET_TO_NORMAL_STATE_TIME = time(hour=11)


class BedroomWithMockValues(BedroomRoom):
    def initialize(self):
        self.initialize_room(
            room_state=MOCK_ROOM_STATE,
            automatic_lights=MOCK_AUTOMATIC_LIGHTS,
            night_lights=MOCK_NIGHT_LIGHTS,
            click_entities=MOCK_SWITCHES + MOCK_BUTTONS,
            motion_sensors=MOCK_MOTION_SENSORS,
            reset_to_normal_state=MOCK_RESET_TO_NORMAL_STATE_TIME)


@automation_fixture(BedroomWithMockValues)
def automation():
    pass


@pytest.fixture
def bedroom(bedroom_with_params):
    return bedroom_with_params[0]


@pytest.fixture
def normal_mode(given_that):
    given_that.state_of(MOCK_ROOM_STATE.entity_id).is_set_to('Normal')


@pytest.fixture
def sleep_mode(given_that):
    given_that.state_of(MOCK_ROOM_STATE.entity_id).is_set_to('Sommeil')


@pytest.fixture
def init_motion_sensors_to_off(given_that):
    for motion_sensor in MOCK_MOTION_SENSORS:
        given_that.state_of(motion_sensor.entity_id).is_set_to('off')


@pytest.fixture
def when_new(given_that, automation):
    class WhenNewWrapper:
        def motion(self):
            automation._new_motion(None, None, None)

        def no_more_motion(self):
            automation._no_more_motion(None, None, None, None, None)

        def click(self):
            automation._new_click(None, None, None)

        def time(self, new_time):
            given_that.time_is(new_time)
            automation._new_time({'trigger_time': new_time})

    return WhenNewWrapper()


def test_callbacks_are_registered(automation, assert_that, hass_functions):
    for button in MOCK_BUTTONS:
        assert_that(automation) \
            .listens_to.event(EVENTS['xiaomi_click'], entity_id=button.entity_id, click_type='single') \
            .with_callback(automation._new_click)
    for switch in MOCK_SWITCHES:
        assert_that(automation) \
            .listens_to.event(EVENTS['xiaomi_click'], entity_id=switch.entity_id) \
            .with_callback(automation._new_click)

    for motion_sensor in MOCK_MOTION_SENSORS:
        assert_that(automation) \
            .listens_to.event(EVENTS['motion'], entity_id=motion_sensor.entity_id) \
            .with_callback(automation._new_motion)
        assert_that(automation) \
            .listens_to.state(motion_sensor.entity_id, new='off', old='on') \
            .with_callback(automation._no_more_motion)

    assert_that(automation) \
        .registered.run_daily(time(hour=10), trigger_time=time(hour=10)) \
        .with_callback(automation._new_time)


@pytest.mark.usefixtures('init_motion_sensors_to_off')
class TestBedroom:
    @pytest.mark.usefixtures('normal_mode')
    class TestNormalMode:
        @pytest.mark.parametrize('motion_time,expected_light_mode', [
            (6, 'dawn'),
            (8, 'day'),
            (19, 'day'),
            (23, 'evening'),
            (2, 'night')
        ])
        def test_behaves_as_automatic_light(self,
                                            given_that,
                                            when_new,
                                            assert_that,
                                            motion_time,
                                            expected_light_mode
                                            ):
            given_that.time_is(time(hour=motion_time))

            when_new.motion()
            assert_that('auto_light').was.turned_on(
                color_temp=LIGHT[expected_light_mode]['color_temp'],
                brightness_pct=LIGHT[expected_light_mode]['brightness_pct'])
            assert_that('auto_light_wo_color_tmp').was.turned_on(
                brightness_pct=LIGHT[expected_light_mode]['brightness_pct'])
            assert_that('auto_light_wo_brightness').was.turned_on(
                color_temp=LIGHT[expected_light_mode]['color_temp'])
            assert_that('auto_light_wo_color_tmp_or_brightness').was.turned_on()
            assert_that('auto_and_night_light').was.turned_on(
                color_temp=LIGHT[expected_light_mode]['color_temp'],
                brightness_pct=LIGHT[expected_light_mode]['brightness_pct'])

            given_that.mock_functions_are_cleared()
            when_new.no_more_motion()
            assert_that('auto_light').was.turned_off()
            assert_that('auto_light_wo_color_tmp').was.turned_off()
            assert_that('auto_light_wo_brightness').was.turned_off()
            assert_that('auto_light_wo_color_tmp_or_brightness').was.turned_off()
            assert_that('auto_and_night_light').was.turned_off()

        @pytest.mark.usefixtures('day')
        def test_click_turns_off_light(self, given_that, when_new, assert_that):
            when_new.click()
            assert_that('auto_light').was.turned_off()
            assert_that('auto_light_wo_color_tmp').was.turned_off()
            assert_that('auto_light_wo_brightness').was.turned_off()
            assert_that('auto_light_wo_color_tmp_or_brightness').was.turned_off()
            assert_that('auto_and_night_light').was.turned_off()

        @pytest.mark.parametrize('sleep_start_time', [
            param(time(hour=11), id='Day'),
            param(time(hour=21), id='Evening'),
            param(time(hour=3), id='Night'),
        ])
        def test_click_enters_sleep_mode(self, given_that, when_new, assert_that, sleep_start_time):
            given_that.time_is(sleep_start_time)
            when_new.click()
            assert_that('input_select/select_option').was.called_with(entity_id=MOCK_ROOM_STATE.entity_id,
                                                                      option='Sommeil')


    @pytest.mark.usefixtures('sleep_mode')
    class TestSleepMode:

        @pytest.mark.parametrize('motion_time,expected_light_mode', [
            (6, 'sleep_before_day'),
            (7, 'sleep_after_day'),
            (18, 'sleep_after_day'),
            (23, 'sleep_before_day')
        ])
        def test_night_lights_behave_as_automatic_lights(self,
                                                         given_that,
                                                         when_new,
                                                         assert_that,
                                                         motion_time,
                                                         expected_light_mode):
            given_that.time_is(time(hour=motion_time))

            when_new.motion()
            assert_that('night_light').was.turned_on(
                color_temp=LIGHT[expected_light_mode]['color_temp'],
                brightness_pct=LIGHT[expected_light_mode]['brightness_pct'])
            assert_that('night_light_wo_color_tmp').was.turned_on(
                brightness_pct=LIGHT[expected_light_mode]['brightness_pct'])
            assert_that('night_light_wo_brightness').was.turned_on(
                color_temp=LIGHT[expected_light_mode]['color_temp'])
            assert_that('night_light_wo_color_tmp_or_brightness').was.turned_on()

            given_that.mock_functions_are_cleared()
            when_new.no_more_motion()
            assert_that('night_light').was.turned_off()
            assert_that('night_light_wo_color_tmp').was.turned_off()
            assert_that('night_light_wo_brightness').was.turned_off()
            assert_that('night_light_wo_color_tmp_or_brightness').was.turned_off()

        @pytest.mark.parametrize('click_time,expected_light_mode', [
            (6, 'dawn'),
            (7, 'day'),
            (23, 'evening'),
            (2, 'night')
        ])
        def test_click_turns_on_auto_lights_just_like_normal_mode(self,
                                                                  given_that,
                                                                  when_new,
                                                                  assert_that,
                                                                  click_time,
                                                                  expected_light_mode):
            given_that.time_is(time(hour=click_time))
            when_new.click()
            assert_that('auto_light').was.turned_on(
                color_temp=LIGHT[expected_light_mode]['color_temp'],
                brightness_pct=LIGHT[expected_light_mode]['brightness_pct'])
            assert_that('auto_light_wo_color_tmp').was.turned_on(
                brightness_pct=LIGHT[expected_light_mode]['brightness_pct'])
            assert_that('auto_light_wo_brightness').was.turned_on(
                color_temp=LIGHT[expected_light_mode]['color_temp'])
            assert_that('auto_light_wo_color_tmp_or_brightness').was.turned_on()

        @pytest.mark.parametrize('click_time', [6, 8])
        def test_click_switch_to_normal_mode(self, given_that, click_time, when_new, assert_that):
            given_that.time_is(time(hour=click_time))
            when_new.click()
            assert_that('input_select/select_option').was.called_with(entity_id=MOCK_ROOM_STATE.entity_id,
                                                                      option='Normal')

        @pytest.mark.parametrize('click_time', [6, 8])
        def test_click_turns_off_night_lights_that_are_not_also_auto_lights(self,
                                                                            given_that,
                                                                            click_time,
                                                                            when_new,
                                                                            assert_that):
            given_that.time_is(time(hour=click_time))
            when_new.click()
            assert_that('night_light').was.turned_off()
            assert_that('night_light_wo_color_tmp').was.turned_off()
            assert_that('night_light_wo_brightness').was.turned_off()
            assert_that('night_light_wo_color_tmp_or_brightness').was.turned_off()
            assert_that('auto_and_night_light').was_not.turned_off()

        def test_reset_to_normal_state_at_trigger_time(self, given_that, when_new, assert_that):
            when_new.time(MOCK_RESET_TO_NORMAL_STATE_TIME)
            assert_that('input_select/select_option').was.called_with(entity_id=MOCK_ROOM_STATE.entity_id,
                                                                      option='Normal')
