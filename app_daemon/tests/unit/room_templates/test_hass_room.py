import datetime

import pytest
from appdaemontestframework import automation_fixture

from apps.common.constants import LIGHT
from apps.common.types import EventHandler, MotionSensor, XiaomiSwitch, XiaomiButton, Light, RoomStateValue
from apps.room_templates import HassRoom

MOCK_MOTION_SENSORS = [MotionSensor('sensor1'), MotionSensor('sensor2')]
MOCK_SWITCHES = [XiaomiSwitch('switch1'), XiaomiSwitch('switch2')]
MOCK_BUTTONS = [XiaomiButton('button1'), XiaomiButton('button2')]


class FakeEventHandler(EventHandler):
    def handle_new_motion(self) -> None:
        raise ValueError('Fake only! Shouldn\'t be called!!')

    def handle_no_more_motion(self) -> None:
        raise ValueError('Fake only! Shouldn\'t be called!!')

    def handle_new_click(self) -> None:
        raise ValueError('Fake only! Shouldn\'t be called!!')

    def handle_new_time(self, trigger_time: datetime.time) -> None:
        raise ValueError('Fake only! Shouldn\'t be called!!')

    def handle_new_room_state(self, new_room_state_value: RoomStateValue) -> None:
        raise ValueError('Fake only! Shouldn\'t be called!!')

    def handle_new_cube_action(self, cube_action, action_value=None) -> None:
        raise ValueError('Fake only! Shouldn\'t be called!!')


class HassRoomWithFakeEventHandler(HassRoom):
    _fake_eh: EventHandler

    def initialize(self):
        self._fake_eh = FakeEventHandler()

    def event_handler(self) -> EventHandler:
        return self._fake_eh



@automation_fixture(HassRoomWithFakeEventHandler)
def hass_room():
    pass


class TestHassRoom:
    class TestTurnLightsOnWithLightMode:
        @pytest.mark.parametrize("light_mode", ['day', 'dawn', 'evening', 'night'])
        def test_normal(self, hass_room: HassRoom, light_mode, assert_that):
            lights = [Light('light')]
            hass_room.turn_on_lights_with_light_mode(lights, light_mode)
            assert_that('light').was.turned_on(brightness_pct=LIGHT[light_mode]['brightness_pct'],
                                               color_temp=LIGHT[light_mode]['color_temp'])

        @pytest.mark.parametrize("light_mode", ['day', 'dawn', 'evening', 'night'])
        def test_no_brightness(self, hass_room: HassRoom, light_mode, assert_that):
            lights = [Light('light_wo_brightness', has_brightness=False)]
            hass_room.turn_on_lights_with_light_mode(lights, light_mode)
            assert_that('light_wo_brightness').was.turned_on(color_temp=LIGHT[light_mode]['color_temp'])

        @pytest.mark.parametrize("light_mode", ['day', 'dawn', 'evening', 'night'])
        def test_no_brightness(self, hass_room: HassRoom, light_mode, assert_that):
            lights = [Light('light_wo_color_temp', has_color_temperature=False)]
            hass_room.turn_on_lights_with_light_mode(lights, light_mode)
            assert_that('light_wo_color_temp').was.turned_on(brightness_pct=LIGHT[light_mode]['brightness_pct'])

        class TestWithColor:
            def test_no_color_in_light_mode__use_color_temp(self, hass_room: HassRoom, assert_that):
                light_mode_without_color = 'day'
                assert 'color_name' not in LIGHT[light_mode_without_color]
                lights = [Light('with_color', has_color=True)]
                hass_room.turn_on_lights_with_light_mode(lights, light_mode_without_color)
                assert_that('with_color').was.turned_on(
                    brightness_pct=LIGHT[light_mode_without_color]['brightness_pct'],
                    color_temp=LIGHT[light_mode_without_color]['color_temp'])

            def test_color_available_in_light_mode(self, hass_room: HassRoom, assert_that):
                light_mode_with_color = 'sleep_before_day'
                assert 'color_name' in LIGHT[light_mode_with_color]
                lights = [Light('with_color', has_color=True)]
                hass_room.turn_on_lights_with_light_mode(lights, light_mode_with_color)
                assert_that('with_color').was.turned_on(
                    brightness_pct=LIGHT[light_mode_with_color]['brightness_pct'],
                    color_name=LIGHT[light_mode_with_color]['color_name'])

        def test_multiple_lights(self, hass_room: HassRoom, assert_that):
            light_mode = 'day'
            lights = [Light('light1'),
                      Light('light2'),
                      Light('light3'),
                      Light('light4'),
                      Light('light5')]
            hass_room.turn_on_lights_with_light_mode(lights, light_mode)
            for light in lights:
                assert_that(light.entity_id).was.turned_on(
                    brightness_pct=LIGHT[light_mode]['brightness_pct'],
                    color_temp=LIGHT[light_mode]['color_temp'])
