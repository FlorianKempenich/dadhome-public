from datetime import time

from hypothesis import given
from hypothesis.strategies import random_module, integers, times, composite

from apps.common.time_period_24h import add_minutes, TimePeriod24H
from apps.common.utilities import RandomTimePeriodGenerator

EIGHT_PM = time(hour=20)
START = EIGHT_PM
MIN_INTERVAL = 10
MAX_INTERVAL = 20
MIN_DURATION = 5
MAX_DURATION = 45


@composite
def random_tp_gen_strategy(draw):
    start = draw(times(max_value=time(hour=20)), label="Start Time")
    min_interval = draw(integers(min_value=1, max_value=10))
    max_interval = draw(integers(min_value=min_interval, max_value=30))
    min_duration = draw(integers(min_value=1, max_value=20))
    max_duration = draw(integers(min_value=min_duration, max_value=55))

    return RandomTimePeriodGenerator(start, min_interval, max_interval, min_duration, max_duration)


class TestFirstPeriod:
    @given(random_tp_gen_strategy(), random_module())
    def test_is_after_start_time(self, random_tp_gen, _rand_mod):
        first_period = random_tp_gen.next_period()
        assert first_period.start > random_tp_gen.start

    @given(random_tp_gen_strategy(), random_module())
    def test_is_before_max_interval_since_start_time(self, random_tp_gen, _rand_mod):
        first_period = random_tp_gen.next_period()
        assert first_period.start <= add_minutes(random_tp_gen.start, random_tp_gen.max_interval)

    @given(random_tp_gen_strategy(), random_module())
    def test_is_after_min_interval_since_start_time(self, random_tp_gen, _rand_mod):
        first_period = random_tp_gen.next_period()
        assert first_period.start >= add_minutes(random_tp_gen.start, random_tp_gen.min_interval)


def generate_multiple_period_and_get_last_one(random_tp_gen, number_of_periods_to_generate):
    last_period = None
    for i in range(number_of_periods_to_generate):
        last_period = random_tp_gen.next_period()
    return last_period


class TestNextPeriod:

    @given(random_tp_gen_strategy(), integers(min_value=1, max_value=100), random_module())
    def test_interval_is_between_min_and_max(self,
                                             random_tp_gen: RandomTimePeriodGenerator,
                                             number_of_periods_to_generate,
                                             _rand_mod):
        last_period = generate_multiple_period_and_get_last_one(random_tp_gen, number_of_periods_to_generate)
        next_period = random_tp_gen.next_period()
        interval_duration = TimePeriod24H(last_period.end, next_period.start).duration_in_min()
        assert random_tp_gen.min_interval <= interval_duration <= random_tp_gen.max_interval

    @given(random_tp_gen_strategy(), random_module())
    def test_duration_is_at_least_min_duration(self,
                                               random_tp_gen: RandomTimePeriodGenerator,
                                               _rand_mod):
        next_period = random_tp_gen.next_period()
        assert next_period.end >= add_minutes(next_period.start, random_tp_gen.min_duration)

    @given(random_tp_gen_strategy(), random_module())
    def test_duration_is_at_most_max_duration(self,
                                              random_tp_gen: RandomTimePeriodGenerator,
                                              _rand_mod):
        next_period = random_tp_gen.next_period()
        assert next_period.end <= add_minutes(next_period.start, random_tp_gen.max_duration)
