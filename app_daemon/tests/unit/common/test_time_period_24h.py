from datetime import time

import pytest
from hypothesis import assume, given
from hypothesis.strategies import composite, times

from apps.common.time_period_24h import add_minutes
from apps.common.utilities import TimePeriod24H

M = 1
H = 60


class TestAddMinutes:
    class TestPositiveMinutes:
        def test_within_same_hour(self):
            assert add_minutes(time(hour=20, minute=10), 4 * M) == time(hour=20, minute=14)

        def test_overlapping_one_hour(self):
            assert add_minutes(time(hour=20, minute=10), 55 * M) == time(hour=21, minute=5)

        def test_overlapping_multiple_hours(self):
            assert add_minutes(time(hour=20, minute=10), 2 * H + 55 * M) == time(hour=23, minute=5)

        def test_overlapping_day(self):
            assert add_minutes(time(hour=20, minute=10), 4 * H + 5 * M) == time(hour=0, minute=15)

        def test_overlapping_multiple_day(self):
            assert add_minutes(time(hour=20, minute=10), 60 * H + 5 * M) == time(hour=8, minute=15)

    class TestNegativeMinutes:
        def test_within_same_hour(self):
            assert add_minutes(time(hour=20, minute=10), - 4 * M) == time(hour=20, minute=6)

        def test_overlapping_one_hour(self):
            assert add_minutes(time(hour=20, minute=10), - 15 * M) == time(hour=19, minute=55)

        def test_overlapping_multiple_hours(self):
            assert add_minutes(time(hour=20, minute=10), - (2 * H + 55 * M)) == time(hour=17, minute=15)

        def test_overlapping_day(self):
            assert add_minutes(time(hour=20, minute=10), - (23 * H + 5 * M)) == time(hour=21, minute=5)

        def test_overlapping_multiple_day(self):
            assert add_minutes(time(hour=20, minute=10), - (60 * H + 5 * M)) == time(hour=8, minute=5)


@composite
def periods(draw):
    start = draw(times())
    end = draw(times())
    assume(not equal_to_the_minute(start, end))
    return TimePeriod24H(start, end)


@composite
def not_overnight_periods(draw):
    period = draw(periods())
    assume(period.start < period.end)
    return period


@composite
def overnight_periods(draw):
    period = draw(periods())
    assume(period.start > period.end)
    return period


def in_period_before_midnight(overnight_period, point_in_time):
    return point_in_time >= overnight_period.start


def in_period_after_midnight(overnight_period, point_in_time):
    return point_in_time < overnight_period.end


def equal_to_the_minute(first: time, second: time):
    return first.hour == second.hour and first.minute == second.minute


class TestTimePeriod24h:
    class TestAnyPeriod:
        @given(times())
        def test_same_start_end_to_the_minute__raise_error(self, point_in_time: time):
            start = point_in_time.replace(second=1, microsecond=1)
            end = point_in_time.replace(second=0, microsecond=0)
            with pytest.raises(Exception):
                TimePeriod24H(start=start, end=end)

        @given(times(), times())
        def test_equality(self, start, end):
            assume(not equal_to_the_minute(start, end))
            assert TimePeriod24H(start=start, end=end) == TimePeriod24H(start=start, end=end)

        @given(periods())
        def test_duration_is_always_positive(self, period: TimePeriod24H):
            assert period.duration_in_min() > 0

        @given(periods())
        def test_start_plus_duration_is_end(self, period: TimePeriod24H):
            start_plus_duration = add_minutes(period.start, period.duration_in_min())
            assert equal_to_the_minute(start_plus_duration, period.end)

    class TestNormalPeriodEndLaterThanStart:
        @given(not_overnight_periods(), times())
        def test_in_period(self, period, point_in_time):
            assume(period.start <= point_in_time < period.end)
            assert period.contains(point_in_time)

        @given(not_overnight_periods(), times())
        def test_out_of_period(self, period, point_in_time):
            assume(not period.start <= point_in_time <= period.end)
            assert not period.contains(point_in_time)

        @given(not_overnight_periods())
        def test_start_is_inclusive(self, period):
            assert period.contains(period.start)

        @given(not_overnight_periods())
        def test_end_is_exclusive(self, period):
            assert not period.contains(period.end)

    class TestOvernightPeriodEndEarlierThanStart:
        @given(overnight_periods(), times())
        def test_in_period(self, period, point_in_time):
            assume(in_period_before_midnight(period, point_in_time) or
                   in_period_after_midnight(period, point_in_time))
            assert period.contains(point_in_time)

        @given(overnight_periods(), times())
        def test_out_of_period(self, period, point_in_time):
            assume(not in_period_before_midnight(period, point_in_time) and
                   not in_period_after_midnight(period, point_in_time))
            assert not period.contains(point_in_time)

        @given(overnight_periods())
        def test_start_is_inclusive(self, period):
            assert period.contains(period.start)

        @given(overnight_periods())
        def test_end_is_exclusive(self, period):
            assert not period.contains(period.end)
