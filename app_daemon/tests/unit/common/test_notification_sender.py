from string import printable

from appdaemon.plugins.hass.hassapi import Hass
from hypothesis import given
from hypothesis.strategies import text
from pytest import fixture

from apps.common.utilities import Notification, NotificationSender, WindowsOpened, NewMotion


@fixture
def mock_hass(given_that):
    return Hass(None, None, None, None, None, None, None)


class TestNotification:
    @given(text(printable), text(printable), text(printable))
    def test_equality(self, target, title, message):
        assert Notification(target, title, message) == Notification(target, title, message)

    def test_windows_opened(self):
        notif = WindowsOpened(['window1', 'window2'])
        assert 'window1' in notif.message
        assert 'window2' in notif.message

    def test_new_motion(self):
        notif = NewMotion(['motion_sensor1', 'motion_sensor2'])
        assert 'motion_sensor1' in notif.message
        assert 'motion_sensor2' in notif.message


class TestNotificationSender:
    @fixture
    def notif_sender(self, mock_hass):
        return NotificationSender(mock_hass)

    @given(target=text(printable), title=text(printable), message=text(printable))
    def test_send_notification(self, target, title, message, notif_sender, assert_that):
        notif = Notification(target, title, message)
        notif_sender.send(notif)
        assert_that('notify/pushbullet').was.called_with(target=target, title=title, message=message)
