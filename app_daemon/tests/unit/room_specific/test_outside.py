from appdaemontestframework import automation_fixture

from apps.common.constants import IDS
from apps.floors.outside import Terrasse


@automation_fixture(Terrasse)
def terrasse(given_that):
    given_that.state_of('sun.sun').is_set_to('above_horizon')
    given_that.state_of(IDS['rezchaussee.salonsallemanger.home_theater']).is_set_to('off')


class TestTerrasse:
    def test_callbacks_are_registered(self, terrasse, assert_that):
        assert_that(terrasse) \
            .listens_to.state(IDS['other.sun'], new='below_horizon', old='above_horizon') \
            .with_callback(terrasse._sun_goes_down)
        assert_that(terrasse) \
            .listens_to.state(IDS['other.sun'], new='above_horizon', old='below_horizon') \
            .with_callback(terrasse._sun_goes_up)
        assert_that(terrasse) \
            .listens_to.state(IDS['outside.terrasse.light.coin_repas'], new='on', old='off') \
            .with_callback(terrasse._coin_repas_turned_on)
        assert_that(terrasse) \
            .listens_to.state(IDS['outside.terrasse.light.coin_repas'], new='off', old='on') \
            .with_callback(terrasse._coin_repas_turned_off)
        assert_that(terrasse) \
            .listens_to.state(IDS['rezchaussee.salonsallemanger.home_theater'], new='on', old='off') \
            .with_callback(terrasse._home_theater_started)
        assert_that(terrasse) \
            .listens_to.state(IDS['rezchaussee.salonsallemanger.home_theater'], new='off', old='on') \
            .with_callback(terrasse._home_theater_stopped)

    class TestTreeLights:
        class TestAtInit:
            def test_during_night_turn_on_tree_lights_after_delay(self, terrasse, given_that, assert_that, time_travel):
                given_that.state_of(IDS['other.sun']).is_set_to('below_horizon')
                terrasse.initialize()
                assert_that(IDS['outside.terrasse.light.arbre']).was_not.turned_on()
                time_travel.fast_forward(30).seconds()
                assert_that(IDS['outside.terrasse.light.arbre']).was.turned_on()

            def test_during_day_do_not_turn_on_tree_lights(self, terrasse, given_that, assert_that):
                given_that.state_of(IDS['other.sun']).is_set_to('above_horizon')
                terrasse.initialize()
                assert_that(IDS['outside.terrasse.light.arbre']).was_not.turned_on()

        class TestSun:
            class TestSunGoesDown:
                def test_turn_on_tree_lights(self, terrasse, given_that, assert_that):
                    terrasse._sun_goes_down(None, None, None, None, None)
                    assert_that(IDS['outside.terrasse.light.arbre']).was.turned_on()

                def test_home_theater_is_on_DO_NOT_turn_on_tree_lights(self, terrasse, given_that, assert_that):
                    given_that.state_of(IDS['rezchaussee.salonsallemanger.home_theater']).is_set_to('on')
                    terrasse._sun_goes_down(None, None, None, None, None)
                    assert_that(IDS['outside.terrasse.light.arbre']).was_not.turned_on()

            class TestSunGoesUp:
                def test_sun_goes_up_turn_off_tree_lights(self, terrasse, given_that, assert_that):
                    terrasse._sun_goes_up(None, None, None, None, None)
                    assert_that(IDS['outside.terrasse.light.arbre']).was.turned_off()

        class TestHomeTheater:
            class TestHomeTheaterStarts:
                class TestSunIsDown:
                    def test_turn_off_tree_light(self, terrasse, given_that, assert_that):
                        given_that.state_of(IDS['other.sun']).is_set_to('below_horizon')
                        terrasse._home_theater_started(None, None, None, None, None)
                        assert_that(IDS['outside.terrasse.light.arbre']).was.turned_off()

            class TestHomeTheaterStops:
                class TestSunIsDown:
                    def test_turn_on_tree_light(self, terrasse, given_that, assert_that):
                        given_that.state_of(IDS['other.sun']).is_set_to('below_horizon')
                        terrasse._home_theater_stopped(None, None, None, None, None)
                        assert_that(IDS['outside.terrasse.light.arbre']).was.turned_on()

                class TestSunIsUp:
                    def test_do_not_turn_on_tree_light(self, terrasse, given_that, assert_that):
                        given_that.state_of(IDS['other.sun']).is_set_to('above_horizon')
                        terrasse._home_theater_stopped(None, None, None, None, None)
                        assert_that(IDS['outside.terrasse.light.arbre']).was_not.turned_on()

    class TestCoinRepas:
        def test_turn_on_spots_with_coin_repas(self, terrasse, assert_that):
            terrasse._coin_repas_turned_on(None, None, None, None, None)
            assert_that(IDS['outside.terrasse.light.spots']).was.turned_on()

        def test_turn_off_spots_with_coin_repas(self, terrasse, assert_that):
            terrasse._coin_repas_turned_off(None, None, None, None, None)
            assert_that(IDS['outside.terrasse.light.spots']).was.turned_off()
