from appdaemontestframework import automation_fixture

from apps.common.constants import IDS
from apps.floors.rezchaussee import Entree


@automation_fixture(Entree)
def entree():
    pass


class TestEntree:
    def test_callbacks_are_registered(self, entree, assert_that):
        assert_that(entree) \
            .listens_to.state(IDS['rezchaussee.entree.placard.haut'], new='on', old='off') \
            .with_callback(entree._closet_opened)
        assert_that(entree) \
            .listens_to.state(IDS['rezchaussee.entree.placard.bas'], new='on', old='off') \
            .with_callback(entree._closet_opened)
        assert_that(entree) \
            .listens_to.state(IDS['rezchaussee.entree.placard.haut'], new='off', old='on') \
            .with_callback(entree._closet_closed)
        assert_that(entree) \
            .listens_to.state(IDS['rezchaussee.entree.placard.bas'], new='off', old='on') \
            .with_callback(entree._closet_closed)

    def test_turn_on_light(self, entree, assert_that):
        entree._closet_opened(None, None, None, None, None)
        assert_that(IDS['rezchaussee.entree.light.plafond']).was.turned_on()

    def test_turn_off_light(self, entree, assert_that):
        entree._closet_closed(None, None, None, None, None)
        assert_that(IDS['rezchaussee.entree.light.plafond']).was.turned_off()
