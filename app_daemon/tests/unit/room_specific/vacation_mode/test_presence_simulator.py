from datetime import time

import pytest
from appdaemon.plugins.hass.hassapi import Hass
from hypothesis import given
from hypothesis.strategies import times, sampled_from
from mock import patch, call
from pytest import mark, fixture

from apps.common.constants import IDS
from apps.common.types import Light
from apps.common.utilities import RandomTimePeriodGenerator, TimePeriod24H
from apps.floors.vacation_mode import RandomPresenceSimulator, AlwaysOnPresenceSimulator

MOCK_LIGHTS = [Light('light1'), Light('light2', has_color_temperature=False)]

# For readability sake, active active period starts at midnight
ACTIVE_PERIOD = TimePeriod24H(time(hour=0), time(hour=4))
TIME_IN_ACTIVE_PERIOD = time(hour=2)
TIME_BEFORE_ACTIVE_PERIOD = time(hour=23)
TIME_AFTER_ACTIVE_PERIOD = time(hour=12)

MOCK_RANDOM_ON_PERIODS = [
    TimePeriod24H(time(hour=20, minute=1), time(hour=20, minute=6)),
    TimePeriod24H(time(hour=21, minute=40), time(hour=21, minute=50)),
    TimePeriod24H(time(hour=23, minute=45), time(hour=0, minute=30)),
    TimePeriod24H(time(hour=3, minute=5), time(hour=3, minute=15))
]

# Skipping for now => Need to redo the mocking of time-related things
pytestmark = pytest.mark.skip


@pytest.fixture
def mock_hass(hass_functions, time_travel):
    hass_mocked_by_framework = Hass(None, None, None, None, None, None, None)
    return hass_mocked_by_framework


class TestAlwaysOnPresenceSimulator:
    @pytest.fixture
    def always_on_ps(self, mock_hass):
        return AlwaysOnPresenceSimulator(mock_hass,
                                         lights=MOCK_LIGHTS,
                                         start=ACTIVE_PERIOD.start,
                                         end=ACTIVE_PERIOD.end)

    class TestAtInitialization:
        def test_listens_sunset(self, always_on_ps, mock_hass, hass_functions):
            hass_functions['listen_state'].assert_called_with(always_on_ps._sunset,
                                                              IDS['other.sun'],
                                                              old='above_horizon',
                                                              new='below_horizon')

        def test_test_listen_start_and_end_times(self, always_on_ps, mock_hass, hass_functions):
            hass_functions['run_daily'].assert_has_calls([call(always_on_ps._enter_active_period, ACTIVE_PERIOD.start),
                                                          call(always_on_ps._exit_active_period, ACTIVE_PERIOD.end)],
                                                         any_order=True)

    class TestAtActivation:
        @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
        def test_sun_down_in_active_period__turn_on_lights(self,
                                                           turn_on_lights_with_light_mode,
                                                           given_that,
                                                           always_on_ps,
                                                           mock_hass):
            given_that.state_of(IDS['other.sun']).is_set_to('below_horizon')
            given_that.time_is(TIME_IN_ACTIVE_PERIOD)
            always_on_ps.activate()
            turn_on_lights_with_light_mode.assert_called_with(mock_hass, MOCK_LIGHTS, 'evening')

        @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
        def test_sun_up__do_not_turn_on_lights(self,
                                               turn_on_lights_with_light_mode,
                                               given_that,
                                               always_on_ps):
            given_that.state_of(IDS['other.sun']).is_set_to('above_horizon')
            given_that.time_is(TIME_IN_ACTIVE_PERIOD)
            always_on_ps.activate()
            turn_on_lights_with_light_mode.assert_not_called()

        @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
        def test_not_in_active_period__do_not_turn_on_lights(self,
                                                             turn_on_lights_with_light_mode,
                                                             given_that,
                                                             always_on_ps):
            given_that.state_of(IDS['other.sun']).is_set_to('below_horizon')
            given_that.time_is(TIME_BEFORE_ACTIVE_PERIOD)
            always_on_ps.activate()
            turn_on_lights_with_light_mode.assert_not_called()

    @mark.usefixtures('activated')
    class TestActivated:
        @fixture
        @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
        def activated(self,
                      _turn_on_lights_with_light_mode,
                      always_on_ps):
            always_on_ps.activate()

        class TestEnterActivePeriod:
            @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
            def test_sun_down__turn_on_lights(self,
                                              turn_on_lights_with_light_mode,
                                              given_that,
                                              always_on_ps,
                                              mock_hass):
                given_that.state_of(IDS['other.sun']).is_set_to('below_horizon')
                given_that.time_is(ACTIVE_PERIOD.start)

                always_on_ps._enter_active_period(None)

                turn_on_lights_with_light_mode.assert_called_with(mock_hass, MOCK_LIGHTS, 'evening')

            @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
            def test_sun_up__do_not_turn_on_lights(self,
                                                   turn_on_lights_with_light_mode,
                                                   given_that,
                                                   always_on_ps,
                                                   mock_hass):
                given_that.state_of(IDS['other.sun']).is_set_to('above_horizon')
                given_that.time_is(ACTIVE_PERIOD.start)

                always_on_ps._enter_active_period(None)

                turn_on_lights_with_light_mode.assert_not_called()

        class TestAtSunset:
            @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
            def test_in_active_period__turn_on_lights(self,
                                                      turn_on_lights_with_light_mode,
                                                      given_that,
                                                      always_on_ps,
                                                      mock_hass):
                given_that.state_of(IDS['other.sun']).is_set_to('below_horizon')
                given_that.time_is(TIME_IN_ACTIVE_PERIOD)

                always_on_ps._sunset(None, None, None, None, None)

                turn_on_lights_with_light_mode.assert_called_with(mock_hass, MOCK_LIGHTS, 'evening')

            @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
            def test_not_in_active_period__do_not_turn_on_lights(self,
                                                                 turn_on_lights_with_light_mode,
                                                                 given_that,
                                                                 always_on_ps,
                                                                 mock_hass):
                given_that.state_of(IDS['other.sun']).is_set_to('below_horizon')
                given_that.time_is(TIME_BEFORE_ACTIVE_PERIOD)

                always_on_ps._sunset(None, None, None, None, None)

                turn_on_lights_with_light_mode.assert_not_called()

        class TestExitActivePeriod:
            @patch('apps.floors.vacation_mode.turn_off_lights')
            @given(sun_state=sampled_from(['above_horizon', 'below_horizon']))
            def test_always_turn_off_lights(self,
                                            turn_off_lights,
                                            given_that,
                                            always_on_ps,
                                            mock_hass,
                                            sun_state):
                turn_off_lights.reset_mock()
                given_that.state_of(IDS['other.sun']).is_set_to(sun_state)
                given_that.time_is(ACTIVE_PERIOD.end)

                always_on_ps._exit_active_period(None)

                turn_off_lights.assert_called_with(mock_hass, MOCK_LIGHTS)

    class TestAtDeactivation:
        @patch('apps.floors.vacation_mode.turn_off_lights')
        @given(current_time=times(), sun_state=sampled_from(['above_horizon', 'below_horizon']))
        def test_always_turn_off_lights(self,
                                        turn_off_lights,
                                        given_that,
                                        always_on_ps,
                                        mock_hass,
                                        current_time,
                                        sun_state):
            turn_off_lights.reset_mock()
            given_that.state_of(IDS['other.sun']).is_set_to(sun_state)
            given_that.time_is(current_time)

            always_on_ps.deactivate()

            turn_off_lights.assert_called_with(mock_hass, MOCK_LIGHTS)

    @mark.usefixtures('deactivated')
    class TestDeactivated:
        @fixture
        @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
        def deactivated(self,
                        _turn_on_lights_with_light_mode,
                        always_on_ps):
            always_on_ps.deactivate()

        @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
        @given(current_time=times(), sun_state=sampled_from(['above_horizon', 'below_horizon']))
        def test_enter_active_period__do_not_turn_on_lights(self,
                                                            turn_on_lights_with_light_mode,
                                                            given_that,
                                                            always_on_ps,
                                                            mock_hass,
                                                            current_time,
                                                            sun_state):
            turn_on_lights_with_light_mode.reset_mock()
            given_that.state_of(IDS['other.sun']).is_set_to(sun_state)
            given_that.time_is(current_time)

            always_on_ps._enter_active_period(None)

            turn_on_lights_with_light_mode.assert_not_called()

        @patch('apps.floors.vacation_mode.turn_off_lights')
        @given(current_time=times(), sun_state=sampled_from(['above_horizon', 'below_horizon']))
        def test_exit_active_period__do_not_turn_off_lights(self,
                                                            turn_off_lights,
                                                            given_that,
                                                            always_on_ps,
                                                            mock_hass,
                                                            current_time,
                                                            sun_state):
            turn_off_lights.reset_mock()
            given_that.state_of(IDS['other.sun']).is_set_to(sun_state)
            given_that.time_is(current_time)

            always_on_ps._exit_active_period(None)

            turn_off_lights.assert_not_called()

        @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
        @given(current_time=times())
        def test_sunset__do_not_turn_on_lights(self,
                                               turn_on_lights_with_light_mode,
                                               given_that,
                                               always_on_ps,
                                               mock_hass,
                                               current_time):
            turn_on_lights_with_light_mode.reset_mock()
            given_that.state_of(IDS['other.sun']).is_set_to('below_horizon')
            given_that.time_is(current_time)

            always_on_ps._sunset(None, None, None, None, None)

            turn_on_lights_with_light_mode.assert_not_called()


class TestRandomPresenceSimulator:
    @pytest.fixture
    def random_ps(self, mock_hass):
        return RandomPresenceSimulator(mock_hass,
                                       lights=MOCK_LIGHTS,
                                       start=ACTIVE_PERIOD.start,
                                       end=ACTIVE_PERIOD.end)

    @pytest.fixture
    def jump_to_time(self, time_travel):
        class JumpToTime:
            now = 0

            def __call__(self, minute):
                time_difference = minute - self.now
                time_travel.fast_forward(time_difference).minutes()
                self.now = minute
                time_travel.assert_current_time(self.now).minutes()

        return JumpToTime()

    def test_listen_to_start_of_active_period(self, hass_functions, random_ps):
        hass_functions['run_daily'].assert_called_with(random_ps._entering_active_period, start=ACTIVE_PERIOD.start)

    class TestScheduleRandomOnOff:
        def test_not_in_active_period__raise_error(self,
                                                   mock_hass,
                                                   random_ps,
                                                   hass_functions):
            mock_hass.time.return_value = TIME_BEFORE_ACTIVE_PERIOD
            with pytest.raises(ValueError) as error:
                random_ps.schedule_random_on_off()
            error.match("outside of active period")

        @pytest.mark.usefixtures('in_active_period', 'ensure_active_period_is_from_midnight_to_4am')
        class TestInActivePeriod:
            @pytest.fixture
            def in_active_period(self, mock_hass):
                mock_hass.time.return_value = ACTIVE_PERIOD.start

            @pytest.fixture
            def ensure_active_period_is_from_midnight_to_4am(self):
                if not ACTIVE_PERIOD.start == time(hour=0):
                    pytest.fail("For readability purpose Active period must start at Midnight (hour=0)")
                if not ACTIVE_PERIOD.end == time(hour=4):
                    pytest.fail("For readability purpose Active period must end at 4AM (hour=4)")

            @patch.object(RandomTimePeriodGenerator, 'next_period')
            def test_first_random_period_not_in_active_period__raise_error(self,
                                                                    next_period,
                                                                    mock_hass,
                                                                    random_ps):
                not_in_active_period = TimePeriod24H(start=time(hour=22), end=time(hour=23))
                next_period.side_effect = [not_in_active_period]

                with pytest.raises(ValueError):
                    random_ps.schedule_random_on_off()

            @patch.object(RandomTimePeriodGenerator, 'next_period')
            def test_next_random_period_starts_before_previous_ends__raise_error(self,
                                                                          next_period,
                                                                          mock_hass,
                                                                          random_ps):
                first_period = TimePeriod24H(start=time(hour=2), end=time(hour=3))
                second_period_that_starts_before_first_ends = TimePeriod24H(start=time(hour=2, minute=30),
                                                                            end=time(hour=3, minute=10))
                next_period.side_effect = [
                    first_period,
                    second_period_that_starts_before_first_ends,
                    # Outside active period
                    TimePeriod24H(start=time(hour=5), end=time(hour=7))
                ]
                with pytest.raises(ValueError):
                    random_ps.schedule_random_on_off()

            @patch.object(RandomTimePeriodGenerator, 'next_period')
            @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
            @patch('apps.floors.vacation_mode.turn_off_lights')
            def test_schedule_until_end_of_active_period(self,
                                                         turn_off_lights,
                                                         turn_on_lights_with_light_mode,
                                                         next_period,
                                                         mock_hass,
                                                         random_ps,
                                                         jump_to_time,
                                                         hass_functions):
                def reset_mocks():
                    turn_on_lights_with_light_mode.reset_mock()
                    turn_off_lights.reset_mock()

                # GIVEN:
                #  - It's midnight(hour=0) at T=0
                mock_hass.time.return_value = time(hour=0)
                jump_to_time(minute=0)
                #  - In active period
                assert ACTIVE_PERIOD.contains(mock_hass.time())
                #  - 3 "random" period are in ACTIVE_PERIOD
                #  - 1 "random" period is outside ACTIVE_PERIOD
                next_period.side_effect = [
                    TimePeriod24H(time(hour=0, minute=5), time(hour=0, minute=35)),
                    TimePeriod24H(time(hour=1, minute=0), time(hour=2, minute=0)),
                    TimePeriod24H(time(hour=3, minute=0), time(hour=3, minute=50)),
                    # Outside of active period (ends at 4am)
                    TimePeriod24H(time(hour=5, minute=0), time(hour=7, minute=0))
                ]

                # WHEN:
                #  - Scheduling random on/off
                random_ps.schedule_random_on_off()

                # THEN:
                #  - Lights turn on/off during random period in active period
                #  - Random period outside of active period is ignored
                reset_mocks()
                # PERIOD 1: Start - 0.05AM -   5m
                jump_to_time(minute=4)
                turn_on_lights_with_light_mode.assert_not_called()
                jump_to_time(minute=5)
                turn_on_lights_with_light_mode.assert_called_with(mock_hass, MOCK_LIGHTS, 'evening')
                # PERIOD 1: End   - 0.35AM -  35m
                jump_to_time(minute=34)
                turn_off_lights.assert_not_called()
                jump_to_time(minute=35)
                turn_off_lights.assert_called_with(mock_hass, MOCK_LIGHTS)

                reset_mocks()
                # PERIOD 2: Start - 1.00AM -  60m
                jump_to_time(minute=59)
                turn_on_lights_with_light_mode.assert_not_called()
                jump_to_time(minute=60)
                turn_on_lights_with_light_mode.assert_called_with(mock_hass, MOCK_LIGHTS, 'evening')
                # PERIOD 2: End   - 2.00AM - 120m
                jump_to_time(minute=119)
                turn_off_lights.assert_not_called()
                jump_to_time(minute=120)
                turn_off_lights.assert_called_with(mock_hass, MOCK_LIGHTS)

                reset_mocks()
                # PERIOD 3: Start - 3.00AM - 180m
                jump_to_time(minute=179)
                turn_on_lights_with_light_mode.assert_not_called()
                jump_to_time(minute=180)
                turn_on_lights_with_light_mode.assert_called_with(mock_hass, MOCK_LIGHTS, 'evening')
                # PERIOD 3: End   - 3.50AM - 230m
                jump_to_time(minute=229)
                turn_off_lights.assert_not_called()
                jump_to_time(minute=230)
                turn_off_lights.assert_called_with(mock_hass, MOCK_LIGHTS)

                reset_mocks()
                # IGNORED PERIOD 4: Start - 5.00AM - 300m
                jump_to_time(minute=300)
                turn_on_lights_with_light_mode.assert_not_called()
                # IGNORED PERIOD 4: End   - 7.00AM - 420m
                jump_to_time(minute=420)
                turn_off_lights.assert_not_called()

            @patch.object(RandomTimePeriodGenerator, 'next_period')
            @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
            @patch('apps.floors.vacation_mode.turn_off_lights')
            def test_last_period_astride_end_of_active_period__skip_last_period(self,
                                                                                turn_off_lights,
                                                                                turn_on_lights_with_light_mode,
                                                                                next_period,
                                                                                mock_hass,
                                                                                random_ps,
                                                                                jump_to_time):
                def reset_mocks():
                    turn_on_lights_with_light_mode.reset_mock()
                    turn_off_lights.reset_mock()

                # For readability sake, active active period starts at midnight
                assert ACTIVE_PERIOD.start == time(hour=0)
                assert ACTIVE_PERIOD.end == time(hour=4)

                # GIVEN:
                #  - It's midnight(hour=0) at T=0
                mock_hass.time.return_value = time(hour=0)
                jump_to_time(minute=0)
                #  - In active period
                assert ACTIVE_PERIOD.contains(mock_hass.time())
                #  - 1 "random" period is in ACTIVE_PERIOD
                #  - 1 "random" period is astride ACTIVE_PERIOD (starts in active, ends after active)
                #  - 1 "random" period is outside ACTIVE_PERIOD
                next_period.side_effect = [
                    TimePeriod24H(time(hour=0, minute=5), time(hour=0, minute=35)),
                    # Period astride active period (starts in active period, ends after active period)
                    TimePeriod24H(time(hour=3, minute=0), time(hour=5, minute=0)),
                    # Period outside active period
                    TimePeriod24H(time(hour=6, minute=0), time(hour=7, minute=0)),
                ]

                # WHEN:
                #  - Scheduling random on/off
                random_ps.schedule_random_on_off()

                # THEN:
                #  - Lights turn on/off during random period in active period
                #  - Random period outside of active period is ignored
                reset_mocks()
                # PERIOD 1: Start - 0.05AM -   5m
                jump_to_time(minute=4)
                turn_on_lights_with_light_mode.assert_not_called()
                jump_to_time(minute=5)
                turn_on_lights_with_light_mode.assert_called_with(mock_hass, MOCK_LIGHTS, 'evening')
                # PERIOD 1: End   - 0.35AM -  35m
                jump_to_time(minute=34)
                turn_off_lights.assert_not_called()
                jump_to_time(minute=35)
                turn_off_lights.assert_called_with(mock_hass, MOCK_LIGHTS)

                reset_mocks()
                # IGNORED ASTRIDE PERIOD 2: Start - 3.00AM -  180m
                jump_to_time(minute=179)
                turn_on_lights_with_light_mode.assert_not_called()
                jump_to_time(minute=180)
                turn_on_lights_with_light_mode.assert_not_called()
                # IGNORED ASTRIDE PERIOD 2: End   - 5.00AM - 300m
                jump_to_time(minute=299)
                turn_off_lights.assert_not_called()
                jump_to_time(minute=300)
                turn_on_lights_with_light_mode.assert_not_called()

            @patch.object(RandomTimePeriodGenerator, 'next_period')
            @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
            @patch('apps.floors.vacation_mode.turn_off_lights')
            def test_period_over_midnight(self,
                                          turn_off_lights,
                                          turn_on_lights_with_light_mode,
                                          next_period,
                                          mock_hass,
                                          time_travel,
                                          jump_to_time,
                                          hass_functions):
                def reset_mocks():
                    turn_on_lights_with_light_mode.reset_mock()
                    turn_off_lights.reset_mock()

                # GIVEN:
                #  - Active Period starts at 9PM (until 4AM)
                random_ps = RandomPresenceSimulator(mock_hass,
                                                    MOCK_LIGHTS,
                                                    start=time(hour=21),
                                                    end=time(hour=4))
                #  - It's 10PM at T=0
                mock_hass.time.return_value = time(hour=22)
                time_travel.assert_current_time(0).minutes()
                #  - 1 "random" period overlapping midnight
                #  - 1 "random" period is outside ACTIVE_PERIOD
                next_period.side_effect = [
                    # Period overlapping midnight
                    TimePeriod24H(time(hour=23), time(hour=3)),
                    # Period outside active period
                    TimePeriod24H(time(hour=6), time(hour=7)),
                ]

                # WHEN:
                #  - Scheduling random on/off
                random_ps.schedule_random_on_off()

                # THEN:
                #  - Active period is working properly over midnight
                reset_mocks()
                # PERIOD 1: Start - 11PM -   T=1H(60m) since T=0
                jump_to_time(minute=59)
                turn_on_lights_with_light_mode.assert_not_called()
                jump_to_time(minute=60)
                turn_on_lights_with_light_mode.assert_called_with(mock_hass, MOCK_LIGHTS, 'evening')
                # PERIOD 1: End   - 3AM -  T=5H(300m) since T=0
                jump_to_time(minute=299)
                turn_off_lights.assert_not_called()
                # jump_to_time(minute=300)
                # turn_off_lights.assert_called_with(mock_hass, MOCK_LIGHTS)

    class TestCancelScheduledOnOff:
        @patch.object(RandomTimePeriodGenerator, 'next_period')
        @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
        @patch('apps.floors.vacation_mode.turn_off_lights')
        def test_previously_scheduled_on_off_periods_are_cancelled(self,
                                                                   turn_off_lights,
                                                                   turn_on_lights_with_light_mode,
                                                                   next_period,
                                                                   mock_hass,
                                                                   time_travel,
                                                                   random_ps,
                                                                   jump_to_time,
                                                                   hass_functions):
            def reset_mocks():
                turn_on_lights_with_light_mode.reset_mock()
                turn_off_lights.reset_mock()

            # Given:
            #  - It's midnight(hour=0) at T=0
            mock_hass.time.return_value = time(hour=0)
            time_travel.assert_current_time(0).minutes()
            #  - In active period
            assert ACTIVE_PERIOD.contains(mock_hass.time())
            #  - 3 "random" period are scheduled
            next_period.side_effect = [
                TimePeriod24H(time(hour=0, minute=5), time(hour=0, minute=35)),
                TimePeriod24H(time(hour=1, minute=0), time(hour=2, minute=0)),
                TimePeriod24H(time(hour=3, minute=0), time(hour=3, minute=50)),
                # Outside of active period (ends at 4am)
                TimePeriod24H(time(hour=5, minute=0), time(hour=7, minute=0))
            ]
            random_ps.schedule_random_on_off()

            # When:
            #  - It's after first two periods: 2.30AM
            mock_hass.time.return_value = time(hour=2, minute=30)
            random_ps.cancel_scheduled_on_off()

            # Then:
            #  - 3rd period isn't executed
            jump_to_time(minute=179)
            reset_mocks()
            # SHOULD BE CANCELLED PERIOD 3: Start - 3.00AM - 180m
            jump_to_time(minute=180)
            turn_on_lights_with_light_mode.assert_not_called()
            # SHOULD BE CANCELLED PERIOD 3: End   - 3.50AM - 230m
            jump_to_time(minute=230)
            turn_off_lights.assert_not_called()

        @patch.object(RandomTimePeriodGenerator, 'next_period')
        @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
        @patch('apps.floors.vacation_mode.turn_off_lights')
        def test_canceled_then_rescheduled_then_cancelled_again(self,
                                                                turn_off_lights,
                                                                turn_on_lights_with_light_mode,
                                                                next_period,
                                                                mock_hass,
                                                                time_travel,
                                                                random_ps,
                                                                jump_to_time,
                                                                hass_functions):
            def reset_mocks():
                turn_on_lights_with_light_mode.reset_mock()
                turn_off_lights.reset_mock()

            #
            # GIVEN:
            #  - It's midnight(hour=0) at T=0
            #
            mock_hass.time.return_value = time(hour=0)
            time_travel.assert_current_time(0).minutes()
            #  - In active period
            assert ACTIVE_PERIOD.contains(mock_hass.time())

            #
            # STEP1: 3 "random" period are scheduled
            #
            next_period.side_effect = [
                TimePeriod24H(time(hour=0, minute=5), time(hour=0, minute=35)),
                TimePeriod24H(time(hour=1, minute=0), time(hour=2, minute=0)),
                TimePeriod24H(time(hour=3, minute=0), time(hour=3, minute=50)),
                # Outside of active period (ends at 4am)
                TimePeriod24H(time(hour=5, minute=0), time(hour=7, minute=0))
            ]
            random_ps.schedule_random_on_off()

            #
            # STEP2: 1st "random" period is executed
            #
            reset_mocks()
            #    Period 1: Start - 0.05AM -   5m
            jump_to_time(minute=4)
            turn_on_lights_with_light_mode.assert_not_called()
            jump_to_time(minute=5)
            turn_on_lights_with_light_mode.assert_called_with(mock_hass, MOCK_LIGHTS, 'evening')
            #    Period 1: End   - 0.35AM -  35m
            jump_to_time(minute=34)
            turn_off_lights.assert_not_called()
            jump_to_time(minute=35)
            turn_off_lights.assert_called_with(mock_hass, MOCK_LIGHTS)

            #
            # STEP3: After 1st period Scheduled On/Off are cancelled (0.40AM == 5 min after 1st period)
            #
            jump_to_time(40)
            mock_hass.time.return_value = time(hour=0, minute=40)
            random_ps.cancel_scheduled_on_off()

            #
            # STEP4: Period 2 is indeed cancelled
            #
            reset_mocks()
            #    Period 2: Start - 1.00AM -  60m
            jump_to_time(minute=60)
            turn_on_lights_with_light_mode.assert_not_called()
            #    Period 2: End   - 2.00AM - 120m
            jump_to_time(minute=120)
            turn_off_lights.assert_not_called()

            #
            # STEP5: Time is 3AM and 2 new "random" periods are scheduled
            #
            jump_to_time(minute=180)
            mock_hass.time.return_value = time(hour=3)
            next_period.side_effect = [
                TimePeriod24H(time(hour=3, minute=10), time(hour=3, minute=30)),
                TimePeriod24H(time(hour=3, minute=35), time(hour=3, minute=40)),
                # Outside of active period (ends at 4am)
                TimePeriod24H(time(hour=5, minute=0), time(hour=7, minute=0))
            ]
            random_ps.schedule_random_on_off()

            #
            # STEP6: 1st new "random" period is executed
            #
            reset_mocks()
            #    New period 1: Start - 3.10AM - 190m
            jump_to_time(minute=189)
            turn_on_lights_with_light_mode.assert_not_called()
            jump_to_time(minute=190)
            turn_on_lights_with_light_mode.assert_called_with(mock_hass, MOCK_LIGHTS, 'evening')
            #    New period 1: End   - 3.30AM - 210m
            jump_to_time(minute=209)
            turn_off_lights.assert_not_called()
            jump_to_time(minute=210)
            turn_off_lights.assert_called_with(mock_hass, MOCK_LIGHTS)

            #
            # STEP7: After 1st new period Scheduled On/Off are cancelled (3.40AM == 10 min after 1st new period)
            #
            jump_to_time(220)
            mock_hass.time.return_value = time(hour=3, minute=40)
            random_ps.cancel_scheduled_on_off()

            #
            # STEP8: No new periods were executed until the end of active period
            #
            assert ACTIVE_PERIOD.end == time(hour=4)
            reset_mocks()
            jump_to_time(250)  # 10 min after end of active period
            turn_on_lights_with_light_mode.assert_not_called()
            turn_off_lights.assert_not_called()

        @patch.object(RandomTimePeriodGenerator, 'next_period')
        @patch('apps.floors.vacation_mode.turn_on_lights_with_light_mode')
        @patch('apps.floors.vacation_mode.turn_off_lights')
        def test_cancelled_during_on_off_period__keep_light_on(self,
                                                               turn_off_lights,
                                                               turn_on_lights_with_light_mode,
                                                               next_period,
                                                               mock_hass,
                                                               time_travel,
                                                               random_ps,
                                                               jump_to_time,
                                                               hass_functions):
            def reset_mocks():
                turn_on_lights_with_light_mode.reset_mock()
                turn_off_lights.reset_mock()

            #
            # GIVEN:
            #
            #  - It's midnight(hour=0) at T=0
            mock_hass.time.return_value = time(hour=0)
            time_travel.assert_current_time(0).minutes()
            #
            #  - In active period
            assert ACTIVE_PERIOD.contains(mock_hass.time())
            #
            # - 1 "random" period is scheduled
            next_period.side_effect = [
                TimePeriod24H(time(hour=0, minute=5), time(hour=0, minute=35)),
                # Outside of active period (ends at 4am)
                TimePeriod24H(time(hour=5, minute=0), time(hour=7, minute=0))
            ]
            random_ps.schedule_random_on_off()

            #
            # WHEN:
            #
            #  - In the middle of period 1: 0.20AM - 20m
            jump_to_time(minute=20)
            mock_hass.time.return_value = time(hour=0, minute=20)
            #
            #  - Cancel scheduled on/off
            random_ps.cancel_scheduled_on_off()

            #
            # THEN: Light is never turned off
            reset_mocks()
            jump_to_time(minute=240)  # 4AM
            turn_off_lights.assert_not_called()

    class TestAtActivation:
        @patch.object(RandomPresenceSimulator, 'schedule_random_on_off')
        def test_in_active_period__schedule_random_on_off(self, schedule_random_on_off, random_ps, mock_hass):
            mock_hass.time.return_value = TIME_IN_ACTIVE_PERIOD
            random_ps.activate()
            schedule_random_on_off.assert_called()

        @patch.object(RandomPresenceSimulator, 'schedule_random_on_off')
        def test_not_in_active_period__do_not_schedule_random_on_off(self, schedule_random_on_off, random_ps,
                                                                     mock_hass):
            mock_hass.time.return_value = TIME_BEFORE_ACTIVE_PERIOD
            random_ps.activate()
            schedule_random_on_off.assert_not_called()

    class TestWhenActive:
        @patch.object(RandomPresenceSimulator, 'schedule_random_on_off')
        def test_enter_active_period__schedule_random_on_off(self, schedule_random_on_off, mock_hass, random_ps):
            # Given: RandomPS is active
            mock_hass.time.return_value = TIME_BEFORE_ACTIVE_PERIOD
            random_ps.activate()
            schedule_random_on_off.reset_mock()

            # When: Entering Active Period
            mock_hass.time.return_value = TIME_IN_ACTIVE_PERIOD
            random_ps._entering_active_period(None)

            # Then: Random On/Off are scheduled
            schedule_random_on_off.assert_called()

    class TestAtDeactivation:
        @patch.object(RandomPresenceSimulator, 'cancel_scheduled_on_off')
        def test_cancel_scheduled_on_off_periods(self, cancel_scheduled_on_off, random_ps):
            random_ps.deactivate()
            cancel_scheduled_on_off.assert_called()

    class TestWhenNotActive:
        @patch.object(RandomPresenceSimulator, 'schedule_random_on_off')
        def test_enter_active_period__do_not_schedule_random_on_off(self, schedule_random_on_off, mock_hass, random_ps):
            # Given: RandomPS is not active
            mock_hass.time.return_value = TIME_BEFORE_ACTIVE_PERIOD
            # Do not activate
            schedule_random_on_off.assert_not_called()

            # When: Entering Active Period
            mock_hass.time.return_value = TIME_IN_ACTIVE_PERIOD
            random_ps._entering_active_period(None)

            # Then: Random On/Off are not scheduled
            schedule_random_on_off.assert_not_called()

        @patch.object(RandomPresenceSimulator, 'schedule_random_on_off')
        @patch.object(RandomPresenceSimulator, 'cancel_scheduled_on_off')
        def test_alt_enter_active_period__do_not_schedule_random_on_off(self,
                                                                        _cancel_scheduled_on_off,
                                                                        schedule_random_on_off,
                                                                        mock_hass,
                                                                        random_ps):
            # Given: RandomPS is activated and then deactivated
            mock_hass.time.return_value = TIME_BEFORE_ACTIVE_PERIOD
            random_ps.activate()
            random_ps.deactivate()
            schedule_random_on_off.reset_mock()
            schedule_random_on_off.assert_not_called()

            # When: Entering Active Period
            mock_hass.time.return_value = TIME_IN_ACTIVE_PERIOD
            random_ps._entering_active_period(None)

            # Then: Random On/Off are not scheduled
            schedule_random_on_off.assert_not_called()
