from datetime import time

import pytest
from appdaemontestframework import automation_fixture
from mock import patch, call

from apps.common.constants import IDS, all_windows_and_doors_ids, all_motion_sensors_ids, EVENTS
from apps.common.types import Light
from apps.common.utilities import WindowsOpened, NewMotion, NotificationSender
from apps.floors.vacation_mode import VacationMode, AlwaysOnPresenceSimulator, \
    RandomPresenceSimulator


@automation_fixture(VacationMode)
def vacation_mode():
    pass


@pytest.fixture
def init_vacation_mode_to_off(given_that):
    given_that.state_of(IDS['other.vacation_mode']).is_set_to('off')


@pytest.fixture
def init_all_windows_and_doors_to_closed(given_that):
    for window_door_id in all_windows_and_doors_ids():
        given_that.state_of(window_door_id).is_set_to('off')


@pytest.mark.usefixtures('init_vacation_mode_to_off',
                         'init_all_windows_and_doors_to_closed')
class TestVacationMode:

    @pytest.fixture
    def mock_presence_simulators(self):
        patch('apps.floors.vacation_mode.RandomPresenceSimulator').start()
        patch('apps.floors.vacation_mode.AlwaysOnPresenceSimulator').start()
        yield 'ok'
        patch.stopall()

    class TestInitialization:
        def test_callbacks_are_registered(self, vacation_mode, assert_that):
            assert_that(vacation_mode) \
                .listens_to.state(IDS['other.vacation_mode'], old='off', new='on') \
                .with_callback(vacation_mode._vacation_mode_activated)
            assert_that(vacation_mode) \
                .listens_to.state(IDS['other.vacation_mode'], old='on', new='off') \
                .with_callback(vacation_mode._vacation_mode_disabled)
            for motion_sensor_id in all_motion_sensors_ids():
                assert_that(vacation_mode) \
                    .listens_to.event(EVENTS['motion'], entity_id=motion_sensor_id) \
                    .with_callback(vacation_mode._new_motion)
            for door_window_id in all_windows_and_doors_ids():
                assert_that(vacation_mode) \
                    .listens_to.state(door_window_id, old='off', new='on') \
                    .with_callback(vacation_mode._window_door_opened)

        @patch.object(RandomPresenceSimulator, '__init__', return_value=None)
        @patch.object(AlwaysOnPresenceSimulator, '__init__', return_value=None)
        def test_presence_simulators_are_correctly_initialized(self,
                                                               always_on_ps___init__,
                                                               random_ps__init__,
                                                               vacation_mode):
            # TODO: Handle sunset/start-time/random-start-delay either here, in the PS, or split in both.
            # GIVEN:
            # The lights to initialize
            lights_salon_sallemanger = [Light(IDS['rezchaussee.salonsallemanger.light.salon_plafond.1']),
                                        Light(IDS['rezchaussee.salonsallemanger.light.salon_plafond.2']),
                                        Light(IDS['rezchaussee.salonsallemanger.light.salon_plafond.3']),
                                        Light(IDS['rezchaussee.salonsallemanger.light.salon_plafond.4']),
                                        Light(IDS['rezchaussee.salonsallemanger.light.salle_manger_plafond'])]
            lights_cuisine = [Light(IDS['rezchaussee.cuisine.light.plafond']),
                              Light(IDS['rezchaussee.cuisine.light.spots'])]
            lights_sallebain = [Light(IDS['etage.sallebain.light.plafond'])]
            lights_papa = [Light(IDS['etage.papa.light.plafond'])]

            # WHEN:
            # Initializing 'VacationMode'
            vacation_mode.initialize()

            # THEN:
            # Presence simulators have been initialized with the correct parameters

            always_on_ps___init__.assert_has_calls([call(vacation_mode,
                                                         lights=lights_salon_sallemanger,
                                                         start=time(hour=18),
                                                         end=time(hour=22, minute=30)),
                                                    call(vacation_mode,
                                                         lights=lights_cuisine,
                                                         start=time(hour=18),
                                                         end=time(hour=22, minute=30)),
                                                    call(vacation_mode,
                                                         lights=lights_papa,
                                                         start=time(hour=22),
                                                         end=time(hour=0))],
                                                   any_order=True)
            random_ps__init__.assert_has_calls([call(vacation_mode,
                                                     lights=lights_sallebain,
                                                     start=time(hour=18),
                                                     end=time(hour=23))],
                                               any_order=True)

        @patch.object(VacationMode, '_vacation_mode_activated')
        def test_vacation_mode__on_at_start__trigger_activate_vacation_mode(self,
                                                                            mocked___vacation_mode_activated,
                                                                            given_that,
                                                                            vacation_mode):
            given_that.state_of(IDS['other.vacation_mode']).is_set_to('on')
            vacation_mode.initialize()
            mocked___vacation_mode_activated.assert_called()

        @patch.object(VacationMode, '_vacation_mode_activated')
        def test_vacation_mode__off_at_start__do_not_trigger_activate_vacation_mode(self,
                                                                                    mocked___vacation_mode_activated,
                                                                                    given_that,
                                                                                    vacation_mode):
            given_that.state_of(IDS['other.vacation_mode']).is_set_to('off')
            vacation_mode.initialize()
            mocked___vacation_mode_activated.assert_not_called()

    class TestActivateVacationMode:

        @patch.object(AlwaysOnPresenceSimulator, 'activate')
        @patch.object(RandomPresenceSimulator, 'activate')
        def test_activate_all_presence_simulators(self,
                                                  random_ps__activate,
                                                  always_on_ps__activate,
                                                  vacation_mode):
            vacation_mode.initialize()
            vacation_mode._vacation_mode_activated(None, None, None, None, None)

            assert always_on_ps__activate.call_count == 3
            assert random_ps__activate.call_count == 1

        @pytest.mark.usefixtures('mock_presence_simulators')
        @patch.object(NotificationSender, 'send')
        def test_some_windows_opened__send_notification(self, mock_send, given_that, vacation_mode):
            # Given: Window is open
            vacation_mode.initialize()
            given_that.state_of(IDS['etage.papa.window.1']).is_set_to('on')  # Window
            given_that.state_of(IDS['rezchaussee.entree.door']).is_set_to('on')  # Door
            given_that.state_of(IDS['rezchaussee.salonsallemanger.window.2']).is_set_to('on')  # Velux

            # When: Activating vacation mode
            vacation_mode._vacation_mode_activated(None, None, None, None, None)

            # Then: Notif is sent
            mock_send.assert_called_with(WindowsOpened([IDS['etage.papa.window.1'],
                                                        IDS['rezchaussee.entree.door'],
                                                        IDS['rezchaussee.salonsallemanger.window.2']]))

    class TestDisableVacationMode:
        @patch.object(RandomPresenceSimulator, 'deactivate')
        @patch.object(AlwaysOnPresenceSimulator, 'deactivate')
        def test_disable_all_presence_simulators(self,
                                                 always_on_ps__deactivate,
                                                 random_ps__deactivate,
                                                 vacation_mode):
            vacation_mode.initialize()
            vacation_mode._vacation_mode_disabled(None, None, None, None, None)

            assert always_on_ps__deactivate.call_count == 3
            assert random_ps__deactivate.call_count == 1

    @pytest.mark.usefixtures('mock_presence_simulators')
    class TestWhenVacationModeActivated:
        @patch.object(NotificationSender, 'send')
        def test_motion_anywhere__trigger_notification(self, mock_send, given_that, vacation_mode):
            vacation_mode.initialize()

            given_that.state_of(IDS['other.vacation_mode']).is_set_to('on')
            vacation_mode._new_motion(None, {'entity_id': IDS['rezchaussee.toilettes.motion']}, None)

            mock_send.assert_called_with(NewMotion([IDS['rezchaussee.toilettes.motion']]))

        @patch.object(NotificationSender, 'send')
        def test_door_window_opened__trigger_notification(self, mock_send, given_that, vacation_mode):
            vacation_mode.initialize()

            given_that.state_of(IDS['other.vacation_mode']).is_set_to('on')
            vacation_mode._window_door_opened(IDS['soussol.buanderie.window'], None, None, None, None)

            mock_send.assert_called_with(WindowsOpened([IDS['soussol.buanderie.window']]))

    @pytest.mark.usefixtures('mock_presence_simulators')
    class TestWhenVacationModeDisabled:
        @patch.object(NotificationSender, 'send')
        def test_motion_anywhere__do_not_trigger_notification(self, mock_send, given_that, vacation_mode):
            vacation_mode.initialize()

            given_that.state_of(IDS['other.vacation_mode']).is_set_to('off')
            vacation_mode._new_motion(None, {'entity_id': IDS['rezchaussee.toilettes.motion']}, None)

            mock_send.assert_not_called()

        @patch.object(NotificationSender, 'send')
        def test_door_window_opened__do_not_trigger_notification(self, mock_send, given_that, vacation_mode):
            vacation_mode.initialize()

            given_that.state_of(IDS['other.vacation_mode']).is_set_to('off')
            vacation_mode._window_door_opened(IDS['soussol.buanderie.window'], None, None, None, None)

            mock_send.assert_not_called()
