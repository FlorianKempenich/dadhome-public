from datetime import time

import pytest
from appdaemontestframework import automation_fixture

from apps.common.constants import LIGHT, IDS
from apps.floors.etage import Couloir, SalleBain


@automation_fixture(Couloir)
def couloir_etage(given_that):
    given_that.state_of(IDS['etage.couloir.motion']).is_set_to('off')


@automation_fixture(SalleBain)
def salle_bain(given_that):
    given_that.state_of(IDS['etage.sallebain.motion']).is_set_to('off')


@pytest.fixture
def sleep_mode(given_that):
    given_that.state_of(IDS['etage.papa.room_state']).is_set_to('Sommeil')



@pytest.fixture
def normal_mode(given_that):
    given_that.state_of(IDS['etage.papa.room_state']).is_set_to('Normal')


LIGHT_COULOIR_PLAFOND = IDS['etage.couloir.light.plafond']
LIGHT_COULOIR_GATEWAY = IDS['etage.couloir.gateway']


class TestCouloirEtage:
    @pytest.mark.usefixtures('sleep_mode')
    class TestSleepMode:

        @pytest.mark.parametrize('motion_time,light_to_turn_on,expected_light_mode', [
            (22, LIGHT_COULOIR_GATEWAY, 'sleep_before_day'),
            (1, LIGHT_COULOIR_GATEWAY, 'sleep_before_day'),
            (6, LIGHT_COULOIR_GATEWAY, 'sleep_before_day'),
            (8, LIGHT_COULOIR_PLAFOND, 'sleep_after_day')
        ])
        def test_light_turn_on_after_motion(self, couloir_etage, given_that, motion_time, light_to_turn_on,
                                            expected_light_mode, assert_that):
            given_that.time_is(time(hour=motion_time))
            couloir_etage._new_motion(None, None, None)

            opts = {}
            opts['brightness_pct'] = LIGHT[expected_light_mode]['brightness_pct']

            if 'color_name' in LIGHT[expected_light_mode]:
                opts['color_name'] = LIGHT[expected_light_mode]['color_name']
            else:
                opts['color_temp'] = LIGHT[expected_light_mode]['color_temp']

            assert_that(light_to_turn_on).was.turned_on(**opts)

        def test_BOTH_light_turn_off_when_no_motion(self, couloir_etage, assert_that):
            couloir_etage._no_more_motion(None, None, None, None, None)
            assert_that(LIGHT_COULOIR_PLAFOND).was.turned_off()
            assert_that(LIGHT_COULOIR_GATEWAY).was.turned_off()

    @pytest.mark.usefixtures('normal_mode')
    class TestNormalMode:
        # NOTE: For the rests of the tests of the normal mode, see: `tests/test_automatic_lights.py`
        def test_BOTH_light_turn_off_when_no_motion(self, couloir_etage, assert_that):
            couloir_etage._no_more_motion(None, None, None, None, None)
            assert_that(LIGHT_COULOIR_PLAFOND).was.turned_off()
            assert_that(LIGHT_COULOIR_GATEWAY).was.turned_off()

class TestSalleBain:
    @pytest.mark.usefixtures('sleep_mode')
    class TestSleepMode:

        @pytest.mark.parametrize('motion_time,expected_light_mode', [
            (22, 'sleep_before_day'),
            (1, 'sleep_before_day'),
            (6, 'sleep_before_day'),
            (8, 'sleep_after_day')
        ])
        def test_light_turn_on_after_motion(self, salle_bain, given_that, motion_time, expected_light_mode,
                                            assert_that):
            given_that.time_is(time(hour=motion_time))
            salle_bain._new_motion(None, None, None)

            opts = {}
            opts['brightness_pct'] = LIGHT[expected_light_mode]['brightness_pct']

            if 'color_name' in LIGHT[expected_light_mode]:
                opts['color_name'] = LIGHT[expected_light_mode]['color_name']
            else:
                opts['color_temp'] = LIGHT[expected_light_mode]['color_temp']

            assert_that(IDS['etage.sallebain.light.plafond']).was.turned_on(**opts)

        def test_light_turn_off_when_no_motion(self, salle_bain, assert_that):
            salle_bain._no_more_motion(None, None, None, None, None)
            assert_that(IDS['etage.sallebain.light.plafond']).was.turned_off()
