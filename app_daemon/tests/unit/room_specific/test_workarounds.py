from appdaemontestframework import automation_fixture
from apps.common.constants import IDS, EVENTS, CLICK_TYPES
from apps.floors.workarounds import NoMoreBatteryInCubeWorkaround


@automation_fixture(NoMoreBatteryInCubeWorkaround)
def cube_workaround():
    pass


class TestNoMoreBatteryInCubeWorkaround:
    def test_callbacks_are_registered(self, cube_workaround, assert_that):
        assert_that(cube_workaround) \
            .listens_to.event(EVENTS['xiaomi_click'],
                              entity_id=IDS['workaround.button'],
                              click_type=CLICK_TYPES['single']) \
            .with_callback(cube_workaround._new_single_click)
        assert_that(cube_workaround) \
            .listens_to.event(EVENTS['xiaomi_click'],
                              entity_id=IDS['workaround.button'],
                              click_type=CLICK_TYPES['double']) \
            .with_callback(cube_workaround._new_double_click)
        assert_that(cube_workaround) \
            .listens_to.event(EVENTS['xiaomi_click'],
                              entity_id=IDS['workaround.button'],
                              click_type=CLICK_TYPES['hold']) \
            .with_callback(cube_workaround._new_hold_click)

    def test_switch_to_off(self, cube_workaround: NoMoreBatteryInCubeWorkaround, assert_that):
        cube_workaround._new_hold_click(None, None, None)
        assert_that('input_select/select_option').was.called_with(
                entity_id=IDS['rezchaussee.salonsallemanger.room_state'],
                option='off')

    def test_switch_to_day(self, cube_workaround: NoMoreBatteryInCubeWorkaround, assert_that):
        cube_workaround._new_double_click(None, None, None)
        assert_that('input_select/select_option').was.called_with(
                entity_id=IDS['rezchaussee.salonsallemanger.room_state'],
                option='day')

    def test_switch_to_evening(self, cube_workaround: NoMoreBatteryInCubeWorkaround, assert_that):
        cube_workaround._new_single_click(None, None, None)
        assert_that('input_select/select_option').was.called_with(
                entity_id=IDS['rezchaussee.salonsallemanger.room_state'],
                option='evening')