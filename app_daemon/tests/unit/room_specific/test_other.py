from appdaemontestframework import automation_fixture

from apps.floors.other import Rallonge
from apps.common.constants import EVENTS, IDS


@automation_fixture(Rallonge)
def rallonge():
    pass


def test_callbacks_are_registered(rallonge, assert_that):
    assert_that(rallonge)\
        .listens_to.event(EVENTS['xiaomi_click'], entity_id=IDS['other.button.rallonge'], click_type='single')\
        .with_callback(rallonge._new_click_button)

def test_button_toggles_rallonge(rallonge, assert_that):
    rallonge._new_click_button(None, None, None)
    assert_that('switch/toggle').was.called_with(entity_id=IDS['other.plug.rallonge'])

