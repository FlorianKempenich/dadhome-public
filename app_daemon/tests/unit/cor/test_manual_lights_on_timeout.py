from unittest import mock

import pytest
from appdaemon.plugins.hass import hassapi as hass

from apps.common.types import Light, Room
from apps.cor import ManualLightsOnTimeoutCoREH, EndOfCoRInjector

MOCK_MANUAL_LIGHTS_ON_TIMEOUT = [Light('mock_light_1'), Light('mock_light_1')]
MOCK_TIMEOUT = 120


@pytest.fixture
@mock.patch('apps.common.types.Room')
def mock_room_with_time_travel(mocked_room, time_travel):
    actual_hass_automatically_mocked_by_time_travel = \
        hass.Hass(None, None, None, None, None, None, None)
    mocked_room.run_in.side_effect = \
        actual_hass_automatically_mocked_by_time_travel.run_in
    return mocked_room


@pytest.fixture
def mock_room(mock_room_with_time_travel):
    return mock_room_with_time_travel


@pytest.fixture
def manual_lights_on_timeout_coreh(mock_room_with_time_travel: Room):
    return ManualLightsOnTimeoutCoREH(
        manual_lights_on_timeout=MOCK_MANUAL_LIGHTS_ON_TIMEOUT,
        timeout_in_s=MOCK_TIMEOUT,
        successor=EndOfCoRInjector(mock_room_with_time_travel))


@pytest.fixture
def ensure_timeout_is_120_s():
    if MOCK_TIMEOUT != 120:
        pytest.fail("For readability reason, all tests in this file depend on the timeout being exactly 120s. "
                    "The timeout can be customized on a per-room basis, but will need to remain 120s for the tests")


@pytest.mark.usefixtures('ensure_timeout_is_120_s')
class TestManualLightsOnTimeout:
    def test_click_toggles_manual_light(self,
                                        manual_lights_on_timeout_coreh,
                                        mock_room: Room):
        manual_lights_on_timeout_coreh.handle_new_click()
        mock_room.toggle_lights.assert_called_with(MOCK_MANUAL_LIGHTS_ON_TIMEOUT)

    def test_does_not_turn_off_after_no_more_motion(self,
                                                    manual_lights_on_timeout_coreh,
                                                    mock_room: Room):
        manual_lights_on_timeout_coreh.handle_no_more_motion()
        mock_room.toggle_lights.assert_not_called()

    def test_turns_off_after_no_more_motion_for_timeout_duration(self,
                                                                 manual_lights_on_timeout_coreh,
                                                                 time_travel,
                                                                 mock_room: Room):
        manual_lights_on_timeout_coreh.handle_no_more_motion()

        time_travel.fast_forward(1).minutes()
        mock_room.turn_off_lights.assert_not_called()

        time_travel.fast_forward(1).minutes()
        time_travel.assert_current_time(2).minutes()
        mock_room.turn_off_lights.assert_called_with(MOCK_MANUAL_LIGHTS_ON_TIMEOUT)

    def test_does_not_turns_off_if_motion_reactivated_before_delay(self,
                                                                   manual_lights_on_timeout_coreh,
                                                                   time_travel,
                                                                   mock_room: Room):
        manual_lights_on_timeout_coreh.handle_no_more_motion()

        time_travel.fast_forward(1).minutes()
        mock_room.turn_off_lights.assert_not_called()

        manual_lights_on_timeout_coreh.handle_new_motion()

        time_travel.fast_forward(1).minutes()
        time_travel.assert_current_time(2).minutes()
        mock_room.turn_off_lights.assert_not_called()

    def test_multiple_successive_motion_toggle_final_state_on(self,
                                                              manual_lights_on_timeout_coreh,
                                                              time_travel,
                                                              mock_room: Room):
        # Given: Motion is on/off multiple times, eventually stays ON
        manual_lights_on_timeout_coreh.handle_no_more_motion()
        time_travel.fast_forward(1).seconds()
        manual_lights_on_timeout_coreh.handle_new_motion()
        time_travel.fast_forward(1).seconds()
        manual_lights_on_timeout_coreh.handle_no_more_motion()
        time_travel.fast_forward(1).seconds()
        manual_lights_on_timeout_coreh.handle_new_motion()
        time_travel.fast_forward(1).seconds()
        manual_lights_on_timeout_coreh.handle_no_more_motion()
        time_travel.fast_forward(1).seconds()
        manual_lights_on_timeout_coreh.handle_new_motion()

        # When: Delay elapses
        time_travel.fast_forward(2).minutes()

        # Then: Manual light was not turned off
        mock_room.turn_off_lights.assert_not_called()

    def test_multiple_successive_motion_toggle_final_state_off(self,
                                                               manual_lights_on_timeout_coreh,
                                                               time_travel,
                                                               mock_room: Room):
        # Given: Motion is on/off multiple times, eventually stays ON
        manual_lights_on_timeout_coreh.handle_no_more_motion()
        time_travel.fast_forward(1).seconds()
        manual_lights_on_timeout_coreh.handle_new_motion()
        time_travel.fast_forward(1).seconds()
        manual_lights_on_timeout_coreh.handle_no_more_motion()
        time_travel.fast_forward(1).seconds()
        manual_lights_on_timeout_coreh.handle_new_motion()
        time_travel.fast_forward(1).seconds()
        manual_lights_on_timeout_coreh.handle_no_more_motion()

        # When: Just before delay (120s - 1s)
        time_travel.fast_forward(119).seconds()
        # Then: Lights not yet turned off
        mock_room.turn_off_lights.assert_not_called()

        # When: Just after delay (120s - 1s + 1s)
        time_travel.fast_forward(1).seconds()
        # Then: Manual light was turned off
        mock_room.turn_off_lights.assert_called_with(MOCK_MANUAL_LIGHTS_ON_TIMEOUT)
