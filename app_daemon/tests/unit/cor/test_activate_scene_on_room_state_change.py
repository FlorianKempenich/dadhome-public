from unittest import mock

import pytest

from apps.common.types import Room, Scene
from apps.cor import ActivateSceneOnRoomStateChange, EndOfCoRInjector

MOCK_ROOM_STATE_SCENE_MAPPING = {'state_value_1': Scene('scene1'),
                                 'state_value_2': Scene('scene2')}


@pytest.fixture
@mock.patch('apps.common.types.Room')
def mock_room(mocked_room):
    return mocked_room


@pytest.fixture
def active_scene_coreh(mock_room: Room):
    return ActivateSceneOnRoomStateChange(
        room_state_scene_mapping=MOCK_ROOM_STATE_SCENE_MAPPING,
        successor=EndOfCoRInjector(mock_room))


def test_valid_new_room_state___activate_corresponding_scene(active_scene_coreh, mock_room: Room):
    active_scene_coreh.handle_new_room_state('state_value_1')
    mock_room.activate_scene.assert_called_with(Scene('scene1'))


def test_invalid_new_room_state___ignore(active_scene_coreh, mock_room: Room):
    active_scene_coreh.handle_new_room_state('invalid_state_value')
    mock_room.activate_scene.assert_not_called()
