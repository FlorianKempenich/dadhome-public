from unittest import mock

import pytest

from apps.common.types import Room
from apps.cor import EndOfCoRInjector, UpdateRoomStateOnCubeAction

MOCK_CUBE_ACTION_ROOM_STATE_MAPPING = {'flip': 'room_state_value_1',
                                       'tap_twice': 'room_state_value_2',
                                       'rotate__below_cutoff': 'room_state_value_3',
                                       'rotate__above_cutoff': 'room_state_value_4'}
MOCK_CUTOFF_FOR_ROTATE = 135


@pytest.fixture
@mock.patch('apps.common.types.Room')
def mock_room(mocked_room):
    return mocked_room


@pytest.fixture
def active_scene_coreh(mock_room: Room):
    return UpdateRoomStateOnCubeAction(
        cube_action_room_state_mapping=MOCK_CUBE_ACTION_ROOM_STATE_MAPPING,
        rotate_cutoff=MOCK_CUTOFF_FOR_ROTATE,
        successor=EndOfCoRInjector(mock_room))


class TestValidAction:
    class TestActionWithoutValue:
        def test_valid_cube_action___switch_to_corresponding_room_state(self, active_scene_coreh, mock_room: Room):
            active_scene_coreh.handle_new_cube_action('flip')
            mock_room.switch_to_room_state.assert_called_with('room_state_value_1')

        def test_valid_cube_action_with_value___raise_exception(self, active_scene_coreh, mock_room: Room):
            with pytest.raises(ValueError) as expected_error:
                active_scene_coreh.handle_new_cube_action('flip', action_value=44)
            expected_error.match("Cube action: 'flip' can not have an action value")

    class TestRotate:
        def test_rotate_below_cutoff___switch_to_corresponding_room_state(self, active_scene_coreh, mock_room: Room):
            active_scene_coreh.handle_new_cube_action('rotate', action_value=40)
            mock_room.switch_to_room_state.assert_called_with('room_state_value_3')

        def test_rotate_above_cutoff___switch_to_corresponding_room_state(self, active_scene_coreh, mock_room: Room):
            active_scene_coreh.handle_new_cube_action('rotate', action_value=180)
            mock_room.switch_to_room_state.assert_called_with('room_state_value_4')

        def test_rotate_below_cutoff_inverse_direction___switch_to_corresponding_room_state(self, active_scene_coreh, mock_room: Room):
            active_scene_coreh.handle_new_cube_action('rotate', action_value=-40)
            mock_room.switch_to_room_state.assert_called_with('room_state_value_3')

        def test_rotate_above_cutoff_inverse_direction___switch_to_corresponding_room_state(self, active_scene_coreh, mock_room: Room):
            active_scene_coreh.handle_new_cube_action('rotate', action_value=-180)
            mock_room.switch_to_room_state.assert_called_with('room_state_value_4')

        def test_rotate_without_value___raise_exception(self, active_scene_coreh, mock_room: Room):
            with pytest.raises(ValueError) as expected_error:
                active_scene_coreh.handle_new_cube_action('rotate')
            expected_error.match("Cube action: 'rotate' should have an action value")

        def test_missing_from_mapping__do_not_raise__just_ignore(self, mock_room: Room):
            mapping_without_rotate__below_cutoff = {
                'flip':                 'room_state_value_1',
                'tap_twice':            'room_state_value_2',
                'rotate__above_cutoff': 'room_state_value_4'}
            active_scene_coreh = UpdateRoomStateOnCubeAction(
                    cube_action_room_state_mapping=mapping_without_rotate__below_cutoff,
                    rotate_cutoff=MOCK_CUTOFF_FOR_ROTATE,
                    successor=EndOfCoRInjector(mock_room)
            )

            # Test does not raise, just ignore
            active_scene_coreh.handle_new_cube_action(
                    'rotate',
                    action_value=10
            )


class TestInvalidAction:
    def test_without_value___ignore(self, active_scene_coreh, mock_room: Room):
        active_scene_coreh.handle_new_cube_action('not_a_cube_action')
        mock_room.switch_to_room_state.assert_not_called()

    def test_with_value___ignore(self, active_scene_coreh, mock_room: Room):
        active_scene_coreh.handle_new_cube_action('not_a_cube_action', action_value=333)
        mock_room.switch_to_room_state.assert_not_called()
