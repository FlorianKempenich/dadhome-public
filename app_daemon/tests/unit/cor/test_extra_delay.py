from unittest import mock

from appdaemon.plugins.hass.hassapi import Hass
from pytest import fixture, mark

from apps.common.constants import PERIODS
from apps.common.types import MotionSensor, Light, Room
from apps.cor import EndOfCoRInjector, ExtraDelayCoREH, AutomaticLightsCoREH, \
    ExtraDelayDuringPeriodCoREH

MOCK_MOTION_SENSORS = [MotionSensor('sensor1'), MotionSensor('sensor2')]
MOCK_LIGHTS = [Light('light1'), Light('light2')]
MOCK_DELAY = 4


@fixture
@mock.patch('apps.common.types.Room')
def mock_room(mocked_room, given_that, time_travel):
    hass = Hass(None, None, None, None, None, None, None)  # Automatically mocked by framework
    mocked_room.get_state.side_effect = hass.get_state
    mocked_room.run_in.side_effect = hass.run_in
    mocked_room.time.side_effect = hass.time
    return mocked_room


@fixture
def extra_delay_coreh(mock_room: Room):
    return ExtraDelayCoREH(
            extra_delay_in_min=MOCK_DELAY,
            successor=
            AutomaticLightsCoREH(
                    lights=MOCK_LIGHTS,
                    motion_sensors=MOCK_MOTION_SENSORS,
                    successor=EndOfCoRInjector(mock_room)))

@fixture
def extra_delay_during_day_coreh(mock_room: Room):
    return ExtraDelayDuringPeriodCoREH(
            extra_delay_in_min=MOCK_DELAY,
            period=PERIODS['day'],
            successor=AutomaticLightsCoREH(
                    lights=MOCK_LIGHTS,
                    motion_sensors=MOCK_MOTION_SENSORS,
                    successor=EndOfCoRInjector(mock_room)))


@fixture
def init_all_motion_sensor_to_off(given_that):
    for motion_sensor in MOCK_MOTION_SENSORS:
        given_that.state_of(motion_sensor.entity_id).is_set_to('off')


@mark.usefixtures('init_all_motion_sensor_to_off', 'day')
class TestExtraDelayCoREH:
    def test_immediately_after_no_more_motion__prevent_lights_from_turning_off(
            self,
            extra_delay_coreh,
            mock_room):
        extra_delay_coreh.handle_no_more_motion()
        mock_room.turn_off_lights.assert_not_called()

    def test_after_delay__turns_off_lights(self, extra_delay_coreh, mock_room,
                                           time_travel):
        extra_delay_coreh.handle_no_more_motion()

        time_travel.fast_forward(MOCK_DELAY).minutes()
        mock_room.turn_off_lights.assert_called_with(MOCK_LIGHTS)

    def test_after_delay__one_motion_sensor_still_on__do_not_turn_off_lights(
            self, given_that, extra_delay_coreh, mock_room, time_travel):
        extra_delay_coreh.handle_no_more_motion()
        given_that.state_of(MOCK_MOTION_SENSORS[0].entity_id).is_set_to('on')

        time_travel.fast_forward(MOCK_DELAY).minutes()
        mock_room.turn_off_lights.assert_not_called()

    def test_motion_before_delay__do_not_turn_off_lights_after_delay(self,
                                                                     extra_delay_coreh,
                                                                     mock_room,
                                                                     time_travel):
        extra_delay_coreh.handle_no_more_motion()

        time_travel.fast_forward(MOCK_DELAY - 1).minutes()
        extra_delay_coreh.handle_new_motion()

        time_travel.fast_forward(1).minutes()

        mock_room.turn_off_lights.assert_not_called()

    def test_multiple_on_off_motion___turn_off_after_delay(self,
                                                           extra_delay_coreh,
                                                           mock_room,
                                                           time_travel):
        extra_delay_coreh.handle_no_more_motion()

        time_travel.fast_forward(MOCK_DELAY - 2).minutes()
        extra_delay_coreh.handle_new_motion()
        time_travel.fast_forward(1).minutes()
        extra_delay_coreh.handle_no_more_motion()

        time_travel.fast_forward(MOCK_DELAY - 1).minutes()
        mock_room.turn_off_lights.assert_not_called()
        time_travel.fast_forward(1).minutes()
        mock_room.turn_off_lights.assert_called_with(MOCK_LIGHTS)


@mark.usefixtures('init_all_motion_sensor_to_off')
class TestExtraDelayDuringPeriodCoREH:
    @mark.usefixtures('day')
    def test_inside_period_add_extra_delay(self,
                                           extra_delay_during_day_coreh,
                                           mock_room,
                                           time_travel):
        extra_delay_during_day_coreh.handle_no_more_motion()

        time_travel.fast_forward(MOCK_DELAY - 1).minutes()
        mock_room.turn_off_lights.assert_not_called()
        time_travel.fast_forward(1).minutes()
        mock_room.turn_off_lights.assert_called_with(MOCK_LIGHTS)

    @mark.usefixtures('evening')
    def test_outside_period_do_not_add_delay(self,
                                             extra_delay_during_day_coreh,
                                             mock_room,
                                             time_travel):
        extra_delay_during_day_coreh.handle_no_more_motion()
        mock_room.turn_off_lights.assert_called_with(MOCK_LIGHTS)
