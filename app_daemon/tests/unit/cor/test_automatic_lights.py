from unittest import mock

from appdaemon.plugins.hass.hassapi import Hass
from pytest import fixture, mark

from apps.common.constants import TIMES
from apps.common.types import MotionSensor, Light, Room
from apps.cor import AutomaticLightsCoREH, EndOfCoRInjector

MOCK_MOTION_SENSORS = [MotionSensor('sensor1'), MotionSensor('sensor2')]
MOCK_LIGHTS = [Light('light1'), Light('light2')]
MOCK_DELAY = 4


@fixture
@mock.patch('apps.common.types.Room')
def mock_room(mocked_room, given_that):
    hass = Hass(None, None, None, None, None, None, None)  # Automatically mocked by framework
    mocked_room.get_state.side_effect = hass.get_state
    return mocked_room


@fixture
def auto_lights_coreh(mock_room: Room):
    return AutomaticLightsCoREH(
        lights=MOCK_LIGHTS,
        motion_sensors=MOCK_MOTION_SENSORS,
        successor=EndOfCoRInjector(mock_room))


@mark.parametrize("current_time, expected_light_mode", [
    (TIMES['day'], 'day'),
    (TIMES['dawn'], 'dawn'),
    (TIMES['evening'], 'evening'),
    (TIMES['night'], 'night')
])
def test_turn_on_when_motion(current_time,
                             expected_light_mode,
                             auto_lights_coreh,
                             mock_room):
    mock_room.time.return_value = current_time
    auto_lights_coreh.handle_new_motion()
    mock_room.turn_on_lights_with_light_mode.assert_called_with(MOCK_LIGHTS, expected_light_mode)


class TestTurnOffWhenNoMotion:
    def test_multiple_sensors_all_off___turn_off_light(self,
                                                       given_that,
                                                       auto_lights_coreh: AutomaticLightsCoREH,
                                                       mock_room: Room):
        for motion_sensor in MOCK_MOTION_SENSORS:
            given_that.state_of(motion_sensor.entity_id).is_set_to('off')

        auto_lights_coreh.handle_no_more_motion()

        mock_room.turn_off_lights.assert_called_with(MOCK_LIGHTS)

    def test_multiple_sensors_one_still_on___do_not_turn_off_light(self,
                                                                   given_that,
                                                                   mock_room: Room,
                                                                   auto_lights_coreh: AutomaticLightsCoREH):
        # Given: All sensors are OFF except the first one
        for motion_sensor in MOCK_MOTION_SENSORS:
            given_that.state_of(motion_sensor.entity_id).is_set_to('off')
        given_that.state_of(MOCK_MOTION_SENSORS[0].entity_id).is_set_to('on')

        auto_lights_coreh.handle_no_more_motion()

        mock_room.turn_off_lights.assert_not_called()
