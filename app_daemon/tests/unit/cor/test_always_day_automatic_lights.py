from unittest import mock

import pytest

from apps.common.constants import TIMES
from apps.cor import EndOfCoRInjector, AlwaysDayAutomaticLightsCoREH
from apps.common.types import MotionSensor, Light, Room

MOCK_MOTION_SENSORS = [MotionSensor('sensor1'), MotionSensor('sensor2')]
MOCK_LIGHTS = [Light('light1'), Light('light2')]


@pytest.fixture
@mock.patch('apps.common.types.Room')
def mock_room(mocked_room):
    return mocked_room


@pytest.fixture
def auto_lights_coreh(mock_room: Room):
    return AlwaysDayAutomaticLightsCoREH(
        lights=MOCK_LIGHTS,
        motion_sensors=MOCK_MOTION_SENSORS,
        successor=EndOfCoRInjector(mock_room))


@pytest.mark.parametrize("current_time, expected_light_mode", [
    (TIMES['day'], 'day'),
    (TIMES['dawn'], 'day'),
    (TIMES['evening'], 'day'),
    (TIMES['night'], 'day')
])
def test_turn_on_when_motion(current_time,
                             expected_light_mode,
                             auto_lights_coreh,
                             mock_room):
    mock_room.time.return_value = current_time
    auto_lights_coreh.handle_new_motion()
    mock_room.turn_on_lights_with_light_mode.assert_called_with(MOCK_LIGHTS, expected_light_mode)
