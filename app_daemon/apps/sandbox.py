import random

import appdaemon.plugins.hass.hassapi as hass

try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.common.constants import IDS
except ModuleNotFoundError:  # pragma: no cover
    from common.constants import IDS


class Sandbox(hass.Hass):  # pragma: no cover
    def initialize(self):
        # TODO: Fix --> Mock 'self.args' in Appdaemon Test Framework
        #  print(f"I am {self.args['name']}")
        #  print(f"I live in {self.args['city']}")
        #  print("My favorite colors are:")
        #  for color in self.args['favorite_colors']:
        #      print(f' - {color}')
        print('I am initialized :D :D :D')
        wo_attr_kw = self.get_state('light.salon_1')
        attr_kw_all = self.get_state('light.salon_1', attribute='all')
        from pprint import pprint
        print('wo_attr_kw')
        pprint(wo_attr_kw)
        print('')
        print('attr_kw_all')
        pprint(attr_kw_all)
        print('')


class Outside:  # pragma: no cover
    my_var: str

    class Inside:
        def hello(self):
            return self.Out.my_var

    def __init__(self, my_var: str) -> None:
        super().__init__()
        self.my_var = my_var

    def outside_hello(self):
        return self.my_var


class PropertyBasedSandbox:

    def minus(self, first, second):
        return first - second

    def sum_rand_numbers(self):
        first = random.randint(0, 100)
        second = random.randint(0, 100)
        return first + second
