import appdaemon.plugins.hass.hassapi as hass

try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.common.constants import IDS
except ModuleNotFoundError:  # pragma: no cover
    from common.constants import IDS


class Cabane(hass.Hass):  # pragma: no cover
    def initialize(self):
        print('I am Cabane :D')


class Terrasse(hass.Hass):
    def initialize(self):
        self.listen_state(self._sun_goes_up, IDS['other.sun'],
                          old='below_horizon', new='above_horizon')
        self.listen_state(self._sun_goes_down, IDS['other.sun'],
                          old='above_horizon', new='below_horizon')
        self.listen_state(self._coin_repas_turned_on,
                          IDS['outside.terrasse.light.coin_repas'], old='off', new='on')
        self.listen_state(self._coin_repas_turned_off,
                          IDS['outside.terrasse.light.coin_repas'], old='on', new='off')
        self.listen_state(self._home_theater_started,
                          IDS['rezchaussee.salonsallemanger.home_theater'], old='off', new='on')
        self.listen_state(self._home_theater_stopped,
                          IDS['rezchaussee.salonsallemanger.home_theater'], old='on', new='off')

        if self._sun_is_down():
            self.run_in(lambda _kwargs: self.turn_on(IDS['outside.terrasse.light.arbre']), 30)

    def _sun_goes_up(self, _entity, _attribute, _old, _new, _kwargs):
        self.turn_off(IDS['outside.terrasse.light.arbre'])

    def _sun_goes_down(self, _entity, _attribute, _old, _new, _kwargs):
        if not self._home_theater_on():
            self.turn_on(IDS['outside.terrasse.light.arbre'])

    def _coin_repas_turned_on(self, _entity, _attribute, _old, _new, _kwargs):
        self.turn_on(IDS['outside.terrasse.light.spots'])

    def _coin_repas_turned_off(self, _entity, _attribute, _old, _new, _kwargs):
        self.turn_off(IDS['outside.terrasse.light.spots'])

    def _home_theater_started(self, _entity, _attribute, _old, _new, _kwargs):
        self.turn_off(IDS['outside.terrasse.light.arbre'])

    def _home_theater_stopped(self, _entity, _attribute, _old, _new, _kwargs):
        if self._sun_is_down():
            self.turn_on(IDS['outside.terrasse.light.arbre'])

    def _sun_is_down(self):
        return self.get_state(IDS['other.sun']) == 'below_horizon'

    def _home_theater_on(self):
        return self.get_state(IDS['rezchaussee.salonsallemanger.home_theater']) == 'on'


class CabaneVelo(hass.Hass):  # pragma: no cover
    def initialize(self):
        print('I am CabaneVelo :D')
