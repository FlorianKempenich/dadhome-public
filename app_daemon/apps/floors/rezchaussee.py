import appdaemon.plugins.hass.hassapi as hass

try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.cor import ManualLightsOnTimeoutCoREH, AutomaticLightsCoREH, EndOfCoRInjector, \
        UpdateRoomStateOnCubeAction, ActivateSceneOnRoomStateChange, \
        CoREventHandlerWithDefault
    from apps.room_templates import AutomaticLightsRoom, AutomaticLightsWithManualLightsOnTimeoutRoom, HassRoom
    from apps.common.constants import EVENTS, DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, IDS
    from apps.common.types import Light, EntityId, EventHandler, Entity, MotionSensor, XiaomiSwitch, RoomState, \
        RoomStateValue, Scene
except ModuleNotFoundError:  # pragma: no cover
    from cor import ManualLightsOnTimeoutCoREH, AutomaticLightsCoREH, EndOfCoRInjector, \
        UpdateRoomStateOnCubeAction, ActivateSceneOnRoomStateChange, \
        CoREventHandlerWithDefault
    from room_templates import AutomaticLightsRoom, AutomaticLightsWithManualLightsOnTimeoutRoom, HassRoom
    from common.constants import EVENTS, DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, IDS
    from common.types import Light, EntityId, EventHandler, Entity, MotionSensor, XiaomiSwitch, RoomState, \
        RoomStateValue, Scene


class Cuisine(AutomaticLightsWithManualLightsOnTimeoutRoom):
    def initialize(self):
        super().initialize_room(
            motion_sensors=[MotionSensor(IDS['rezchaussee.cuisine.motion.1']),
                            MotionSensor(IDS['rezchaussee.cuisine.motion.2'])],
            automatic_lights=[Light(IDS['rezchaussee.cuisine.light.spots'],
                                    has_color_temperature=False,
                                    has_brightness=False)],
            manual_lights_on_timeout=[Light(IDS['rezchaussee.cuisine.light.plafond'],
                                            has_color_temperature=False,
                                            has_brightness=False)],
            manual_lights_triggers=[XiaomiSwitch(
                IDS['rezchaussee.cuisine.switch.extra'])],
            manual_lights_timeout=DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S)


class Entree(hass.Hass):
    def initialize(self):
        self.listen_state(self._closet_opened,
                          IDS['rezchaussee.entree.placard.haut'], new='on', old='off')
        self.listen_state(self._closet_opened,
                          IDS['rezchaussee.entree.placard.bas'], new='on', old='off')
        self.listen_state(self._closet_closed,
                          IDS['rezchaussee.entree.placard.haut'], new='off', old='on')
        self.listen_state(self._closet_closed,
                          IDS['rezchaussee.entree.placard.bas'], new='off', old='on')

    def _closet_opened(self, _entity, _attribute, _old, _new, _kwargs):
        self.turn_on(IDS['rezchaussee.entree.light.plafond'])

    def _closet_closed(self, _entity, _attribute, _old, _new, _kwargs):
        self.turn_off(IDS['rezchaussee.entree.light.plafond'])


class SalleManger(hass.Hass):  # pragma: no cover
    def initialize(self):
        pass


class SalonSalleManger(HassRoom):
    class CaptureMovementEventsIfNotInOffState(CoREventHandlerWithDefault):
        def _process_new_motion(self) -> bool:
            return self._capture_event_if_not_in_off_state()

        def _process_no_more_motion(self) -> bool:
            return self._capture_event_if_not_in_off_state()

        def _capture_event_if_not_in_off_state(self) -> bool:
            if not self._in_off_state():
                return True
            return False

        def _in_off_state(self):
            return self._room.get_state(IDS['rezchaussee.salonsallemanger.room_state']) == 'off'

    class ToggleLectureOnFlip(CoREventHandlerWithDefault):

        def _process_new_cube_action(self, cube_action, action_value) -> bool:
            if cube_action == 'flip180':
                self._room.toggle_lights([Light(IDS['rezchaussee.salonsallemanger.light.lecture'])])
                return True
            return False

    def initialize(self):
        self.register_magic_cubes([Entity(IDS['rezchaussee.salonsallemanger.magic_cube.1']),
                                   Entity(IDS['rezchaussee.salonsallemanger.magic_cube.2']),
                                   Entity(IDS['rezchaussee.salonsallemanger.magic_cube.3']),
                                   Entity(IDS['rezchaussee.salonsallemanger.magic_cube.4'])])
        self.register_room_state_listener(RoomState(IDS['rezchaussee.salonsallemanger.room_state']))
        self.register_motion_sensors([MotionSensor(IDS['rezchaussee.salonsallemanger.motion'])])

    def event_handler(self) -> EventHandler:
        cube_action_to_room_state_mapping = {
            'shake_air': 'off',
            'rotate__above_cutoff': 'evening',
            'flip90': 'day',
        }

        room_state_to_scene_mapping = {
            'off': Scene(IDS['rezchaussee.salonsallemanger.scene.off']),
            'day': Scene(IDS['rezchaussee.salonsallemanger.scene.day']),
            'evening': Scene(IDS['rezchaussee.salonsallemanger.scene.evening'])
        }

        return ActivateSceneOnRoomStateChange(
            room_state_scene_mapping=room_state_to_scene_mapping,
            successor=UpdateRoomStateOnCubeAction(
                cube_action_room_state_mapping=cube_action_to_room_state_mapping,
                rotate_cutoff=30,
                successor=self.ToggleLectureOnFlip(
                    successor=self.CaptureMovementEventsIfNotInOffState(
                        successor=AutomaticLightsCoREH(
                            lights=[Light(IDS['rezchaussee.salonsallemanger.light.salon_plafond.1']),
                                    Light(IDS['rezchaussee.salonsallemanger.light.ambiance_1.1'])],
                            motion_sensors=[MotionSensor(IDS['rezchaussee.salonsallemanger.motion'])],
                            successor=EndOfCoRInjector(self))))))

    def switch_to_room_state(self, room_state: RoomStateValue):
        self.call_service('input_select/select_option',
                          entity_id=IDS['rezchaussee.salonsallemanger.room_state'],
                          option=room_state)


class Toilettes(AutomaticLightsRoom):
    def initialize(self):
        super().initialize_room(motion_sensors=[MotionSensor(IDS['rezchaussee.toilettes.motion'])],
                                lights=[Light(IDS['rezchaussee.toilettes.light.plafond'])])
