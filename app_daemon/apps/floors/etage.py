from datetime import time


try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.cor import SleepModeCoREH, EndOfCoRInjector, AutomaticLightsCoREH, \
    ManualLightsOnTimeoutCoREH, \
    CoREventHandlerWithDefault, ExtraDelayDuringPeriodCoREH
    from apps.room_templates import HassRoom, AutomaticLightsRoom, AutomaticLightsWithManualLightsOnTimeoutRoom, \
        BedroomRoom
    from apps.common.constants import TIMES, LIGHT, PERIODS, DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, IDS
    from apps.common.utilities import TimePeriod24H
    from apps.common.types import Light, EventHandler, XiaomiSwitch, MotionSensor, \
    RoomState, Room
except ModuleNotFoundError:  # pragma: no cover
    from cor import SleepModeCoREH, EndOfCoRInjector, AutomaticLightsCoREH, \
        ManualLightsOnTimeoutCoREH, \
        CoREventHandlerWithDefault, ExtraDelayDuringPeriodCoREH
    from room_templates import HassRoom, AutomaticLightsRoom, AutomaticLightsWithManualLightsOnTimeoutRoom, \
        BedroomRoom
    from common.constants import TIMES, LIGHT, PERIODS, DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, IDS
    from common.utilities import TimePeriod24H
    from common.types import Light, EventHandler, XiaomiSwitch, MotionSensor, \
        RoomState, Room


class Couloir(AutomaticLightsRoom):
    # TODO: Refactor like SalleBain
    def initialize(self):
        super().initialize_room(motion_sensors=[MotionSensor(IDS['etage.couloir.motion'])],
                                lights=[Light(IDS['etage.couloir.light.plafond'])])

    def _no_more_motion(self, _entity, _attribute, _old, _new, _kwargs):
        super()._no_more_motion(_entity, _attribute, _old, _new, _kwargs)
        self.turn_off(IDS['etage.couloir.gateway'])

    def _new_motion(self, _event, _data, _kwargs):
        if self._in_sleep_mode():
            if self._before_day():
                self.turn_on(IDS['etage.couloir.gateway'],
                             color_name=LIGHT['sleep_before_day']['color_name'],
                             brightness_pct=LIGHT['sleep_before_day']['brightness_pct'])
            else:
                self.turn_on(IDS['etage.couloir.light.plafond'],
                             color_temp=LIGHT['sleep_after_day']['color_temp'],
                             brightness_pct=LIGHT['sleep_after_day']['brightness_pct'])
        else:
            super()._new_motion(_event, _data, _kwargs)

    def _in_sleep_mode(self):
        return self.get_state(IDS['etage.papa.room_state']) == 'Sommeil'

    def _before_day(self):
        return PERIODS['sleep_until_day'].contains(self.time())


class CaptureMotionOffIfEitherBathroomOrCouloirStillHasMotion(CoREventHandlerWithDefault):

    def _process_no_more_motion(self) -> bool:
        if self._still_motion_in(IDS['etage.couloir.motion']) \
                or self._still_motion_in(IDS['etage.sallebain.motion']):
            return True
        return False

    def _still_motion_in(self, motion_sensor_entity_id):
        return self._room.get_state(motion_sensor_entity_id) == 'on'


class Flo(BedroomRoom):
    def initialize(self):
        super().initialize_room(room_state=RoomState(IDS['etage.flo.room_state']),
                                automatic_lights=[Light(IDS['etage.flo.light.ambiance'],
                                                        has_color=False,
                                                        has_color_temperature=False,
                                                        has_brightness=True)],
                                night_lights=[Light(IDS['etage.couloir.gateway'], has_color=True),
                                              Light(IDS['etage.sallebain.light.plafond'], has_color=True)],
                                click_entities=[XiaomiSwitch(IDS['etage.flo.switch'])],
                                motion_sensors=[MotionSensor(IDS['etage.flo.motion'])],
                                reset_to_normal_state=time(hour=10))

        self._handlers['Sommeil'] = \
            CaptureMotionOffIfEitherBathroomOrCouloirStillHasMotion(successor=self._handlers['Sommeil'])


class Geo(BedroomRoom):
    def initialize(self):
        super().initialize_room(room_state=RoomState(IDS['etage.geo.room_state']),
                                automatic_lights=[Light(IDS['etage.geo.light.plafond'],
                                                        has_color=False,
                                                        has_color_temperature=False,
                                                        has_brightness=False)],
                                night_lights=[Light(IDS['etage.couloir.gateway'], has_color=True),
                                              Light(IDS['etage.sallebain.light.plafond'], has_color=True)],
                                click_entities=[XiaomiSwitch(IDS['etage.geo.switch'])],
                                motion_sensors=[MotionSensor(IDS['etage.geo.motion'])],
                                reset_to_normal_state=time(hour=10))

        self._handlers['Sommeil'] = \
            CaptureMotionOffIfEitherBathroomOrCouloirStillHasMotion(successor=self._handlers['Sommeil'])


class Papa(BedroomRoom):
    class DadNormalStateCoREH(CoREventHandlerWithDefault):
        def __init__(self,
                     automatic_lights: [Light],
                     motion_sensors: [MotionSensor],
                     room: Room) -> None:
            super().__init__(
                    successor=ExtraDelayDuringPeriodCoREH(
                            extra_delay_in_min=120,
                            period=PERIODS['evening'],
                            successor=AutomaticLightsCoREH(
                                    lights=automatic_lights,
                                    motion_sensors=motion_sensors,
                                    successor=EndOfCoRInjector(
                                            room))))
            self._automatic_lights = automatic_lights

        def _process_new_click(self) -> bool:
            self._room.turn_off_lights(self._automatic_lights)
            self._room.switch_to_room_state('Sommeil')
            return True

    def initialize(self):
        automatic_lights = [Light(IDS['etage.papa.light.plafond'],
                                  has_color=True,
                                  has_color_temperature=True,
                                  has_brightness=True)]
        motion_sensors = [MotionSensor(IDS['etage.papa.motion'])]
        super().initialize_room(room_state=RoomState(IDS['etage.papa.room_state']),
                                automatic_lights=automatic_lights,
                                night_lights=[Light(IDS['etage.couloir.gateway'], has_color=True),
                                              Light(IDS['etage.sallebain.light.plafond'], has_color=True)],
                                click_entities=[XiaomiSwitch(IDS['etage.papa.switch.1']),
                                                XiaomiSwitch(IDS['etage.papa.switch.2']),
                                                XiaomiSwitch(IDS['etage.papa.switch.3']),
                                                XiaomiSwitch(IDS['etage.papa.switch.4'])],
                                motion_sensors=motion_sensors,
                                reset_to_normal_state=time(hour=10))

        self._handlers['Sommeil'] = \
            CaptureMotionOffIfEitherBathroomOrCouloirStillHasMotion(successor=self._handlers['Sommeil'])
        self._handlers['Normal'] = Papa.DadNormalStateCoREH(
                automatic_lights=automatic_lights,
                motion_sensors=motion_sensors,
                room=self
        )


class SalleBain(HassRoom):
    _event_handler: EventHandler

    automatic_lights = [Light(IDS['etage.sallebain.light.plafond'], has_color=True)]
    manual_lights_on_timeout = [Light(IDS['etage.sallebain.light.neon'],
                                      has_brightness=False,
                                      has_color_temperature=False)]
    motion_sensors = [MotionSensor(IDS['etage.sallebain.motion'])]

    def initialize(self):
        self._event_handler = \
            ManualLightsOnTimeoutCoREH(manual_lights_on_timeout=self.manual_lights_on_timeout,
                                       timeout_in_s=DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, successor=SleepModeCoREH(
                    sleep_mode_entity_id=IDS['etage.papa.room_state'],
                    lights=self.automatic_lights,
                    successor=AutomaticLightsCoREH(
                        lights=self.automatic_lights,
                        motion_sensors=self.motion_sensors,
                        successor=EndOfCoRInjector(
                            room=self))))

        self.register_motion_sensors(self.motion_sensors)

    def event_handler(self) -> EventHandler:
        return self._event_handler
