import appdaemon.plugins.hass.hassapi as hass

try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.common.constants import EVENTS, IDS
except ModuleNotFoundError:  # pragma: no cover
    from common.constants import EVENTS, IDS


class Rallonge(hass.Hass):
    def initialize(self):
        self.listen_event(self._new_click_button, EVENTS['xiaomi_click'],
                          entity_id=IDS['other.button.rallonge'], click_type='single')

    def _new_click_button(self, _event, _data, _kwargs):
        self.call_service('switch/toggle', entity_id=IDS['other.plug.rallonge'])
