import appdaemon.plugins.hass.hassapi as hass

try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.room_templates import AutomaticLightsRoom, AutomaticLightsWithManualLightsOnTimeoutRoom
    from apps.common.constants import EVENTS, DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, IDS
    from apps.common.types import Light, MotionSensor, XiaomiButton
except ModuleNotFoundError:  # pragma: no cover
    from room_templates import AutomaticLightsRoom, AutomaticLightsWithManualLightsOnTimeoutRoom
    from common.constants import EVENTS, DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, IDS
    from common.types import Light, MotionSensor, XiaomiButton


class Buanderie(AutomaticLightsRoom):
    def initialize(self):
        super().initialize_room(motion_sensors=[MotionSensor(IDS['soussol.buanderie.motion'])],
                                lights=[Light(IDS['soussol.buanderie.light.plafond'])],
                                always_day_mode=True)


class Bureau(AutomaticLightsWithManualLightsOnTimeoutRoom):
    def initialize(self):
        super().initialize_room(motion_sensors=[MotionSensor(IDS['soussol.bureau.motion.1']),
                                                MotionSensor(IDS['soussol.bureau.motion.2'])],
                                automatic_lights=[Light(IDS['soussol.bureau.light.plafond'])],
                                manual_lights_on_timeout=[Light(IDS['soussol.bureau.light.neons'],
                                                                has_color_temperature=False,
                                                                has_brightness=False)],
                                manual_lights_triggers=[XiaomiButton(IDS['soussol.bureau.button'])],
                                manual_lights_timeout=DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S)


class Garage(hass.Hass):
    def initialize(self):  # pragma: no cover
        print('I am Garage :D')


class Couloir(AutomaticLightsRoom):
    def initialize(self):
        super().initialize_room(motion_sensors=[MotionSensor(IDS['soussol.couloir.motion'])],
                                lights=[Light(IDS['soussol.couloir.light.plafond'])])
