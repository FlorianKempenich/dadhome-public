import appdaemon.plugins.hass.hassapi as hass

try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.cor import ManualLightsOnTimeoutCoREH, AutomaticLightsCoREH, \
        EndOfCoRInjector, \
        UpdateRoomStateOnCubeAction, ActivateSceneOnRoomStateChange, \
        CoREventHandlerWithDefault
    from apps.room_templates import AutomaticLightsRoom, \
        AutomaticLightsWithManualLightsOnTimeoutRoom, HassRoom
    from apps.common.constants import EVENTS, \
    DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, IDS, CLICK_TYPES
    from apps.common.types import Light, EntityId, EventHandler, Entity, \
        MotionSensor, XiaomiSwitch, RoomState, \
        RoomStateValue, Scene
except ModuleNotFoundError:  # pragma: no cover
    from cor import ManualLightsOnTimeoutCoREH, AutomaticLightsCoREH, \
        EndOfCoRInjector, \
        UpdateRoomStateOnCubeAction, ActivateSceneOnRoomStateChange, \
        CoREventHandlerWithDefault
    from room_templates import AutomaticLightsRoom, \
        AutomaticLightsWithManualLightsOnTimeoutRoom, HassRoom
    from common.constants import EVENTS, \
    DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, IDS, CLICK_TYPES
    from common.types import Light, EntityId, EventHandler, Entity, \
        MotionSensor, XiaomiSwitch, RoomState, \
        RoomStateValue, Scene


class NoMoreBatteryInCubeWorkaround(hass.Hass):
    def initialize(self):
        self.listen_event(self._new_single_click,
                          EVENTS['xiaomi_click'],
                          entity_id=IDS['workaround.button'],
                          click_type=CLICK_TYPES['single'])
        self.listen_event(self._new_double_click,
                          EVENTS['xiaomi_click'],
                          entity_id=IDS['workaround.button'],
                          click_type=CLICK_TYPES['double'])
        self.listen_event(self._new_hold_click,
                          EVENTS['xiaomi_click'],
                          entity_id=IDS['workaround.button'],
                          click_type=CLICK_TYPES['hold'])

    def _new_single_click(self, _event, data, _kwargs):
        self.call_service('input_select/select_option',
                          entity_id=IDS['rezchaussee.salonsallemanger.room_state'],
                          option='evening')

    def _new_double_click(self, _event, data, _kwargs):
        self.call_service('input_select/select_option',
                          entity_id=IDS['rezchaussee.salonsallemanger.room_state'],
                          option='day')

    def _new_hold_click(self, _event, data, _kwargs):
        self.call_service('input_select/select_option',
                          entity_id=IDS['rezchaussee.salonsallemanger.room_state'],
                          option='off')

