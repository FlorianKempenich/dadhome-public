from abc import ABC, abstractmethod
from datetime import time
from uuid import uuid4

from appdaemon.plugins.hass.hassapi import Hass


try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.common.constants import IDS, all_windows_and_doors_ids, all_motion_sensors_ids, EVENTS
    from apps.common.types import Light
    from apps.common.utilities import TimePeriod24H, NotificationSender, WindowsOpened, NewMotion, \
        RandomTimePeriodGenerator, turn_on_lights_with_light_mode, turn_off_lights
except ModuleNotFoundError:  # pragma: no cover
    from common.constants import IDS, all_windows_and_doors_ids, all_motion_sensors_ids, EVENTS
    from common.types import Light
    from common.utilities import TimePeriod24H, NotificationSender, WindowsOpened, NewMotion, \
        RandomTimePeriodGenerator, turn_on_lights_with_light_mode, turn_off_lights


class PresenceSimulator(ABC):
    def __init__(self, hass: Hass, lights: [Light], start, end):
        self._hass = hass
        self.lights = lights
        self.active_period = TimePeriod24H(start, end)

    @abstractmethod
    def activate(self):
        pass

    @abstractmethod
    def deactivate(self):
        pass

    def _currently_in_active_period(self):
        now = self._hass.time()
        return self._in_active_period(now)

    def _in_active_period(self, time_to_check):
        return self.active_period.contains(time_to_check)


class AlwaysOnPresenceSimulator(PresenceSimulator):

    def __init__(self, hass: Hass, lights: [Light], start, end):
        super().__init__(hass, lights, start, end)
        self._hass.run_daily(self._enter_active_period, self.active_period.start)
        self._hass.run_daily(self._exit_active_period, self.active_period.end)
        self._hass.listen_state(self._sunset, IDS['other.sun'], old='above_horizon', new='below_horizon')
        self.activated = False

    def activate(self):
        self.activated = True
        if self._sun_is_down() and self._currently_in_active_period():
            self._turn_on_lights()

    def deactivate(self):
        self.activated = False
        self._turn_off_lights()

    def _sunset(self, _entity, _attribute, _old, _new, _kwargs):
        if self.activated and self._currently_in_active_period():
            self._turn_on_lights()

    def _enter_active_period(self, _kwargs):
        if self.activated and self._sun_is_down():
            self._turn_on_lights()

    def _exit_active_period(self, _kwargs):
        if self.activated:
            self._turn_off_lights()

    def _sun_is_down(self):
        return self._hass.get_state(IDS['other.sun']) == 'below_horizon'

    def _turn_on_lights(self):
        turn_on_lights_with_light_mode(self._hass, self.lights, 'evening')

    def _turn_off_lights(self):
        turn_off_lights(self._hass, self.lights)


class RandomPresenceSimulator(PresenceSimulator):

    def __init__(self, hass: Hass, lights: [Light], start, end):
        super().__init__(hass, lights, start, end)
        self.scheduled_on_off_periods = []
        self.cancelled_on_off_periods = []
        self._hass.run_daily(self._entering_active_period, start=start)
        self.activated = False

    def activate(self):
        self.activated = True
        if self._currently_in_active_period():
            self.schedule_random_on_off()

    def deactivate(self):
        self.activated = False
        self.cancel_scheduled_on_off()

    def _entering_active_period(self, _kwargs):
        if self.activated:
            self.schedule_random_on_off()

    def schedule_random_on_off(self):
        if not self._currently_in_active_period():
            raise ValueError("Can't start scheduling random on/off when outside of active period")

        now = self._hass.time()
        random_period_gen = RandomTimePeriodGenerator(start=now)

        def all_random_period_in_active_period():
            first_random_period = random_period_gen.next_period()
            if not self._in_active_period(first_random_period.start):
                raise ValueError("There should be at least ONE random On/Off period contained in active period!")

            next_random_period = first_random_period
            while self._in_active_period(next_random_period.end):
                current_random_period = next_random_period
                yield current_random_period
                next_random_period = random_period_gen.next_period()
                if next_random_period.start < current_random_period.end:
                    raise ValueError("Next period can not start before previous one ended!")

        for on_off_period in all_random_period_in_active_period():
            scheduled_start_from_now_in_min = self._time_difference_in_minutes(now, on_off_period.start)
            scheduled_end_from_now_in_min = self._time_difference_in_minutes(now, on_off_period.end)
            period_uuid = uuid4()
            self._hass.run_in(self._turn_on_lights, scheduled_start_from_now_in_min * 60, period_uuid=period_uuid)
            self._hass.run_in(self._turn_off_lights, scheduled_end_from_now_in_min * 60, period_uuid=period_uuid)
            self.scheduled_on_off_periods.append(period_uuid)

    def cancel_scheduled_on_off(self):
        self.cancelled_on_off_periods += self.scheduled_on_off_periods
        self.scheduled_on_off_periods = []

    def _turn_on_lights(self, kwargs):
        if kwargs['period_uuid'] in self.cancelled_on_off_periods:
            return

        turn_on_lights_with_light_mode(self._hass, self.lights, 'evening')

    def _turn_off_lights(self, kwargs):
        period_uuid = kwargs['period_uuid']
        if period_uuid in self.cancelled_on_off_periods:
            self.cancelled_on_off_periods.remove(period_uuid)
            return

        turn_off_lights(self._hass, self.lights)
        self.scheduled_on_off_periods.remove(period_uuid)

    @staticmethod
    def _time_difference_in_minutes(time1: time, time2: time):
        return TimePeriod24H(time1, time2).duration_in_min()


class VacationMode(Hass):
    _presence_simulators: [PresenceSimulator]
    _notification_sender: NotificationSender

    def initialize(self):
        self._notification_sender = NotificationSender(self)
        self.listen_state(self._vacation_mode_activated, IDS['other.vacation_mode'], old='off', new='on')
        self.listen_state(self._vacation_mode_disabled, IDS['other.vacation_mode'], old='on', new='off')
        for motion_sensor in all_motion_sensors_ids():
            self.listen_event(self._new_motion, EVENTS['motion'], entity_id=motion_sensor)
        for window_door_id in all_windows_and_doors_ids():
            self.listen_state(self._window_door_opened, window_door_id, old='off', new='on')

        self._presence_simulators = [
            AlwaysOnPresenceSimulator(self,
                                      lights=[Light(IDS['rezchaussee.salonsallemanger.light.salon_plafond.1']),
                                              Light(IDS['rezchaussee.salonsallemanger.light.salon_plafond.2']),
                                              Light(IDS['rezchaussee.salonsallemanger.light.salon_plafond.3']),
                                              Light(IDS['rezchaussee.salonsallemanger.light.salon_plafond.4']),
                                              Light(IDS['rezchaussee.salonsallemanger.light.salle_manger_plafond'])],
                                      start=time(hour=18),
                                      end=time(hour=22, minute=30)),
            AlwaysOnPresenceSimulator(self,
                                      lights=[Light(IDS['rezchaussee.cuisine.light.plafond']),
                                              Light(IDS['rezchaussee.cuisine.light.spots'])],
                                      start=time(hour=18),
                                      end=time(hour=22, minute=30)),
            AlwaysOnPresenceSimulator(self,
                                      lights=[Light(IDS['etage.papa.light.plafond'])],
                                      start=time(hour=22),
                                      end=time(hour=0)),
            RandomPresenceSimulator(self,
                                    lights=[Light(IDS['etage.sallebain.light.plafond'])],
                                    start=time(hour=18),
                                    end=time(hour=23))]

        if self.get_state(IDS['other.vacation_mode']) == 'on':
            self._vacation_mode_activated(None, None, None, None, None)

    def _vacation_mode_activated(self, _entity, _attribute, _old, _new, _kwargs):
        self.log('Vacation Mode: Activated')
        for ps in self._presence_simulators:
            ps.activate()

        currently_opened_windows = [id_ for id_ in all_windows_and_doors_ids() if self.get_state(id_) == 'on']
        if currently_opened_windows:
            self._notification_sender.send(WindowsOpened(currently_opened_windows))

    def _vacation_mode_disabled(self, _entity, _attribute, _old, _new, _kwargs):
        self.log('Vacation Mode: Deactivated')
        for ps in self._presence_simulators:
            ps.deactivate()

    def _new_motion(self, _event, data, _kwargs):
        if self._vacation_mode_is_activated():
            triggered_sensor = data['entity_id']
            self._notification_sender.send(NewMotion([triggered_sensor]))

    def _window_door_opened(self, entity, _attribute, _old, _new, _kwargs):
        if self._vacation_mode_is_activated():
            self._notification_sender.send(WindowsOpened([entity]))

    def _vacation_mode_is_activated(self):
        return self.get_state(IDS['other.vacation_mode']) == 'on'
