try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.common.constants import IDS
    from apps.common.types import Light, MotionSensor
    from apps.room_templates import AutomaticLightsRoom
except ModuleNotFoundError:  # pragma: no cover
    from common.constants import IDS
    from common.types import Light, MotionSensor
    from room_templates import AutomaticLightsRoom


class Atelier(AutomaticLightsRoom):
    def initialize(self):
        super().initialize_room(motion_sensors=[MotionSensor(IDS['cave.atelier.motion'])],
                                lights=[Light(IDS['cave.atelier.light.neons'],
                                              has_color_temperature=False,
                                              has_brightness=False)],
                                extra_delay_in_min=5)


class CaveMilieu(AutomaticLightsRoom):
    def initialize(self):
        super().initialize_room(motion_sensors=[MotionSensor(IDS['cave.cavemilieu.motion'])],
                                lights=[Light(IDS['cave.cavemilieu.light.plafond'],
                                              has_color_temperature=False,
                                              has_brightness=False)])


class CaveVin(AutomaticLightsRoom):
    def initialize(self):
        super().initialize_room(motion_sensors=[MotionSensor(IDS['cave.cavevin.motion'])],
                                lights=[Light(IDS['cave.cavevin.light.plafond'],
                                              has_color_temperature=False,
                                              has_brightness=False)])
