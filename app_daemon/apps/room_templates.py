import datetime
from abc import ABC
from datetime import time
from typing import Dict

from appdaemon.plugins.hass import hassapi as hass

try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.common.constants import EVENTS, LIGHT, PERIODS
    from apps.cor import AlwaysDayAutomaticLightsCoREH, EndOfCoRInjector, \
    AutomaticLightsCoREH, \
    ManualLightsOnTimeoutCoREH, CoREventHandler, CoREventHandlerWithDefault, \
    ExtraDelayCoREH, ExtraDelayDuringPeriodCoREH
    from apps.common.types import EntityId, Light, EventHandler, Room, RoomStateValue, ClickEntity, XiaomiSwitch, \
        XiaomiButton, MotionSensor, RoomState, Entity, Scene
    from apps.common.utilities import turn_on_lights_with_light_mode, turn_off_lights
except ModuleNotFoundError:  # pragma: no cover
    from common.constants import EVENTS, LIGHT, PERIODS
    from cor import AlwaysDayAutomaticLightsCoREH, EndOfCoRInjector, \
        AutomaticLightsCoREH, \
        ManualLightsOnTimeoutCoREH, CoREventHandler, CoREventHandlerWithDefault, \
        ExtraDelayCoREH, ExtraDelayDuringPeriodCoREH
    from common.types import EntityId, Light, EventHandler, Room, RoomStateValue, ClickEntity, XiaomiSwitch, \
        XiaomiButton, MotionSensor, RoomState, Entity, Scene
    from common.utilities import turn_on_lights_with_light_mode, turn_off_lights

"""
Module Structure:

* All Room templates inherit from `HassRoom`.
* No Room can inherit from another Room (except `HassRoom` ofc)

"""


class HassRoom(hass.Hass, Room, ABC):
    def register_motion_sensors(self, motion_sensors: [MotionSensor]):
        for motion_sensor in motion_sensors:
            self.listen_event(self._new_motion, EVENTS['motion'], entity_id=motion_sensor.entity_id)
            self.listen_state(self._no_more_motion, motion_sensor.entity_id, new='off', old='on')

    def register_click_entities(self, click_entities: [ClickEntity]):
        for click_entity in click_entities:
            self.listen_event(self._new_click,
                              EVENTS['xiaomi_click'],
                              entity_id=click_entity.entity_id,
                              **click_entity.registration_kwargs)

    def register_time_clock(self):
        for hour in range(0, 23):
            trigger_time = time(hour=hour)
            self.run_daily(self._new_time, trigger_time, trigger_time=trigger_time)

    def register_magic_cubes(self, magic_cubes: [Entity]):
        for magic_cube in magic_cubes:
            self.listen_event(self._new_cube_action,
                              EVENTS['cube_action'],
                              entity_id=magic_cube.entity_id)

    def register_room_state_listener(self, room_state: RoomState):
        self.listen_state(self._new_room_state, room_state.entity_id)

    def _new_motion(self, _event, data, _kwargs):
        if data:
            self.log(f"New motion: {data['entity_id']}")
        self.event_handler().handle_new_motion()

    def _no_more_motion(self, entity, _attribute, _old, _new, _kwargs):
        self.log(f'No more motion: {entity}')
        self.event_handler().handle_no_more_motion()

    def _new_click(self, _event, data, _kwargs):
        if data:
            self.log(f"New click: {data['entity_id']}")
        self.event_handler().handle_new_click()

    def _new_time(self, kwargs):
        self.log(f"New time: {kwargs['trigger_time']}")
        self.event_handler().handle_new_time(kwargs['trigger_time'])

    def _new_cube_action(self, _event, data, _kwargs):
        if 'entity_id' in data:
            self.log(f"New cube action - Cube: {data['entity_id']} | Action: {data['action_type']}")
        self.event_handler().handle_new_cube_action(cube_action=data['action_type'],
                                                    action_value=data.get('action_value', None))

    def _new_room_state(self, entity, _attribute, _old, new, _kwargs):
        self.log(f"New room state: Room: {entity} | New state: {new}")
        self.event_handler().handle_new_room_state(new_room_state_value=new)

    def turn_off_lights(self, lights):
        turn_off_lights(self, lights)

    def turn_on_lights_with_light_mode(self, lights, light_mode):
        turn_on_lights_with_light_mode(self, lights, light_mode)

    def toggle_lights(self, lights: [Light]):
        # TODO: Rename to toggle and use the built in `toggle` function in hass
        for light in lights:
            self.call_service('switch/toggle', entity_id=light.entity_id)

    def switch_to_room_state(self, room_state: RoomStateValue):
        # Default implementation for rooms without a RoomState
        raise ValueError("Default method: Should not be called!")  # pragma: no cover

    def activate_scene(self, scene: Scene):
        self.call_service('scene/turn_on', entity_id=scene.entity_id)


class AutomaticLightsRoom(HassRoom):
    _event_handler: EventHandler

    def initialize_room(self, **kwargs):
        if kwargs.get('always_day_mode', False):
            automatic_lights_handler = \
                AlwaysDayAutomaticLightsCoREH(
                    lights=kwargs['lights'],
                    motion_sensors=kwargs['motion_sensors'],
                    successor=EndOfCoRInjector(
                        room=self))
        else:
            automatic_lights_handler = \
                AutomaticLightsCoREH(
                    lights=kwargs['lights'],
                    motion_sensors=kwargs['motion_sensors'],
                    successor=EndOfCoRInjector(
                        room=self))

        if 'extra_delay_in_min' in kwargs:
            automatic_lights_handler = ExtraDelayCoREH(
                extra_delay_in_min=kwargs['extra_delay_in_min'],
                successor=automatic_lights_handler)

        self._event_handler = automatic_lights_handler
        self.register_motion_sensors(kwargs['motion_sensors'])

    def event_handler(self):
        return self._event_handler


class AutomaticLightsWithManualLightsOnTimeoutRoom(HassRoom):
    _event_handler: EventHandler

    def initialize_room(self,
                        motion_sensors: [MotionSensor],
                        automatic_lights: [Light],
                        manual_lights_on_timeout: [Light],
                        manual_lights_triggers: [ClickEntity],
                        manual_lights_timeout: int):
        self._event_handler = ManualLightsOnTimeoutCoREH(manual_lights_on_timeout=manual_lights_on_timeout,
                                                         timeout_in_s=manual_lights_timeout,
                                                         successor=AutomaticLightsCoREH(
                                                             lights=automatic_lights,
                                                             motion_sensors=motion_sensors,
                                                             successor=EndOfCoRInjector(
                                                                 room=self)))
        self.register_motion_sensors(motion_sensors)
        self.register_click_entities(manual_lights_triggers)

    def event_handler(self) -> EventHandler:
        return self._event_handler


class BedroomRoom(HassRoom):
    class NormalStateCoREH(CoREventHandlerWithDefault):
        def __init__(self,
                     automatic_lights: [Light],
                     motion_sensors: [MotionSensor],
                     room: Room) -> None:

            super().__init__(
                    successor=AutomaticLightsCoREH(
                                    lights=automatic_lights,
                                    motion_sensors=motion_sensors,
                                    successor=EndOfCoRInjector(
                                            room))
            )
            self._automatic_lights = automatic_lights

        def _process_new_click(self) -> bool:
            self._room.turn_off_lights(self._automatic_lights)
            self._room.switch_to_room_state('Sommeil')
            return True

    class SleepStateCoREH(CoREventHandlerWithDefault):
        _night_lights: [Light]
        _day_lights: [Light]
        _reset_to_normal_state_time: datetime.time

        def __init__(self,
                     day_lights: [Light],
                     night_lights: [Light],
                     reset_to_normal_state_time: datetime.time,
                     room: Room) -> None:
            super().__init__(successor=EndOfCoRInjector(room))
            self._night_lights = night_lights
            self._day_lights = day_lights
            self._reset_to_normal_state_time = reset_to_normal_state_time

        def _process_new_motion(self) -> bool:
            if self._current_time_is_in_period('sleep_until_day'):
                light_mode = 'sleep_before_day'
            else:
                light_mode = 'sleep_after_day'

            self._room.turn_on_lights_with_light_mode(self._night_lights, light_mode)
            return True

        def _process_no_more_motion(self) -> bool:
            self._room.turn_off_lights(self._night_lights)
            return True

        def _process_new_click(self) -> bool:
            self._room.turn_on_lights_with_light_mode(self._day_lights, self._normal_light_mode())
            lights_only_night_not_auto = [light for light in self._night_lights if light not in self._day_lights]
            self._room.turn_off_lights(lights_only_night_not_auto)
            self._room.switch_to_room_state('Normal')
            return True

        def _process_new_time(self, trigger_time: datetime.time) -> bool:
            if trigger_time == self._reset_to_normal_state_time:
                self._room.switch_to_room_state('Normal')
            return True

    _handlers: Dict[RoomStateValue, CoREventHandler]
    _room_state: RoomState

    def initialize_room(self,
                        room_state: RoomState,
                        automatic_lights: [Light],
                        night_lights: [Light],
                        motion_sensors: [MotionSensor],
                        click_entities: [ClickEntity],
                        reset_to_normal_state: datetime.time):
        self._room_state = room_state
        self._handlers = {
            'Normal': self.NormalStateCoREH(automatic_lights=automatic_lights,
                                            motion_sensors=motion_sensors,
                                            room=self),
            'Sommeil': self.SleepStateCoREH(night_lights=night_lights,
                                          day_lights=automatic_lights,
                                          reset_to_normal_state_time=reset_to_normal_state,
                                          room=self)
        }

        self.register_motion_sensors(motion_sensors)
        self.register_click_entities(click_entities)
        self.register_time_clock()

    def event_handler(self):
        current_mode = self.get_state(self._room_state.entity_id)
        return self._handlers[current_mode]

    def switch_to_room_state(self, room_state: RoomStateValue):
        self.call_service('input_select/select_option',
                          entity_id=self._room_state.entity_id,
                          option=room_state)
