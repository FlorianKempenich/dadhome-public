from datetime import time

H = 60


def add_minutes(time_: time, minutes_to_add: int) -> time:
    """
    Add minutes to an existing time.
    Has a 1 minutes resolution. Support for hour and day overlap.
    :param time_: time to add minutes to
    :param minutes_to_add: number of minutes to add
    :return: new time with added minutes
    """
    hour = time_.hour
    minute = time_.minute + minutes_to_add

    while minute >= 60:
        minute -= 60
        hour += 1

    while minute < 0:
        hour -= 1
        minute += 60

    hour %= 24

    return time(hour=hour, minute=minute)


def equal_to_the_minute(first: time, second: time):
    return first.hour == second.hour and first.minute == second.minute


class TimePeriod24H:
    """
    Represents a time period.
    Start is INCLUSIVE
    End is EXCLUSIVE
    """
    start: time
    end: time

    def __init__(self, start, end):
        if equal_to_the_minute(start, end):
            raise Exception('Start and End can not be equal')
        self.start = start
        self.end = end

    def _is_overnight(self):
        return self.end < self.start

    def contains(self, time):
        if time == self.start:
            return True
        if time == self.end:
            return False

        if self._is_overnight():
            return time < self.end or time > self.start

        return self.start < time < self.end

    def duration_in_min(self) -> int:
        duration = (self.end.hour - self.start.hour) * H + (self.end.minute - self.start.minute)
        if self._is_overnight():
            duration += 24 * H
        return duration

    def __eq__(self, o: object) -> bool:
        return isinstance(o, TimePeriod24H) and self.start == o.start and self.end == o.end

    def __hash__(self) -> int:
        return hash((self.start, self.end))

    def __repr__(self) -> str:
        return f'TimePeriod24H(Start: {self.start} | End: {self.end} | Overnight?: {self._is_overnight()})'
