import random
from datetime import time

from appdaemon.plugins.hass.hassapi import Hass

try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.common.constants import LIGHT
    from apps.common.time_period_24h import TimePeriod24H, add_minutes
except ModuleNotFoundError:  # pragma: no cover
    from common.constants import LIGHT
    from common.time_period_24h import TimePeriod24H, add_minutes


def turn_on_lights_with_light_mode(hass: Hass, lights, light_mode):
    for light in lights:
        def should_set_brightness():
            return light.has_brightness

        def should_set_color_temperature():
            return light.has_color_temperature and not should_set_color_by_name()

        def should_set_color_by_name():
            return light.has_color and 'color_name' in LIGHT[light_mode]

        opts = dict()
        if should_set_brightness():
            opts['brightness_pct'] = LIGHT[light_mode]['brightness_pct']
        if should_set_color_temperature():
            opts['color_temp'] = LIGHT[light_mode]['color_temp']
        if should_set_color_by_name():
            opts['color_name'] = LIGHT[light_mode]['color_name']
        hass.turn_on(light.entity_id, **opts)

    hass.log(f"Turned on lights with mode: '{light_mode}' | Lights: {lights}")


def turn_off_lights(hass: Hass, lights):
    for light in lights:
        hass.turn_off(light.entity_id)
    hass.log(f"Turned off lights: Lights: {lights}")


class RandomTimePeriodGenerator(object):

    def __init__(self, start: time, min_interval=10, max_interval=45, min_duration=5, max_duration=75):
        self.last_period_end = start
        self.start = start
        self.min_interval = min_interval
        self.max_interval = max_interval
        self.min_duration = min_duration
        self.max_duration = max_duration

    def next_period(self) -> TimePeriod24H:
        interval_time = random.randint(self.min_interval, self.max_interval)
        duration = random.randint(self.min_duration, self.max_duration)

        start = add_minutes(self.last_period_end, interval_time)
        end = add_minutes(start, duration)

        self.last_period_end = end
        return TimePeriod24H(start, end)

    def __repr__(self):
        return str(self.__dict__)


class Notification:
    def __init__(self, target, title, message):
        self.target = target
        self.title = title
        self.message = message

    def __eq__(self, o: object) -> bool:
        return isinstance(o, self.__class__) and \
               self.target == o.target and \
               self.title == o.title and \
               self.message == o.message

    def __hash__(self) -> int:
        return hash((self.target, self.title, self.message))

    def __repr__(self) -> str:
        return f"Notification(target='{self.target}' | title='{self.title}' | message='{self.message}')"


class PapaNotification(Notification):
    def __init__(self, title, message):
        super().__init__(
            target='device/Telephone',
            title=title,
            message=message)


class WindowsOpened(PapaNotification):
    def __init__(self, windows_ids: [str]) -> None:
        super().__init__(title="Fenêtres Ouvertes!",
                         message=' - ' + '\n  - '.join(windows_ids))


class NewMotion(PapaNotification):
    def __init__(self, motion_sensor_ids: [str]) -> None:
        super().__init__(title="Mouvement détécté!",
                         message=' - ' + '\n  - '.join(motion_sensor_ids))


class WeatherAlert(WindowsOpened):
    def __init__(self, windows_ids: [str]) -> None:
        super().__init__(windows_ids)
        self.title = "Il va Pleuvoir!"


class NotificationSender:
    def __init__(self, hass: Hass) -> None:
        self._hass = hass

    def send(self, notification: Notification):
        self._hass.call_service('notify/pushbullet',
                                target=notification.target,
                                title=notification.title,
                                message=notification.message)
