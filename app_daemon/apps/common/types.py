import datetime
from abc import ABC, abstractmethod

try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.common.constants import EVENTS
except ModuleNotFoundError:  # pragma: no cover
    from common.constants import EVENTS

EntityId = str
LightMode = str  # day / evening / etc
RoomStateValue = str  # normal / sleep / etc Basically the state machine state.
CubeAction = str


class Entity:
    entity_id: EntityId

    def __init__(self, entity_id) -> None:
        self.entity_id = entity_id

    def __eq__(self, o: object) -> bool:
        return isinstance(o, Entity) and self.entity_id == o.entity_id

    def __hash__(self) -> int:
        return id(self.entity_id)

    def __repr__(self):
        return f"{self.__class__.__name__}('{self.entity_id}')"


class MotionSensor(Entity):
    pass


class RoomState(Entity):
    """
    Do not put the actual state as attribute.
    The state isn't stored internally, only in the remote HomeAssistant
    """
    pass


class Scene(Entity):
    pass


class Light(Entity):
    has_brightness: bool
    has_color_temperature: bool
    has_color: bool

    def __init__(self,
                 entity_id: EntityId,
                 has_brightness: bool = True,
                 has_color_temperature: bool = True,
                 has_color: bool = False) -> None:
        super().__init__(entity_id)
        self.has_brightness = has_brightness
        self.has_color_temperature = has_color_temperature
        self.has_color = has_color

    def __repr__(self):
        return f"{self.__class__.__name__}('{self.entity_id}' | " \
            f"has_brightness = {self.has_brightness} | " \
            f"has_color_temperature = {self.has_color_temperature} | " \
            f"has_color = {self.has_color})"


class ClickEntity(Entity):
    registration_event: str
    registration_kwargs: str

    def __init__(self, entity_id, registration_event, registration_kwargs) -> None:
        super().__init__(entity_id)
        self.registration_event = registration_event
        self.registration_kwargs = registration_kwargs


class XiaomiButton(ClickEntity):
    def __init__(self, entity_id) -> None:
        super().__init__(entity_id,
                         registration_event=EVENTS['xiaomi_click'],
                         registration_kwargs={'click_type': 'single'})


class XiaomiSwitch(ClickEntity):
    def __init__(self, entity_id) -> None:
        super().__init__(entity_id,
                         registration_event=EVENTS['xiaomi_click'],
                         registration_kwargs={})


class EventHandler(ABC):  # pragma: no cover
    """
    Describes the services offered by a EventHandler
    """

    @abstractmethod
    def handle_new_motion(self) -> None:
        pass

    @abstractmethod
    def handle_no_more_motion(self) -> None:
        pass

    @abstractmethod
    def handle_new_click(self) -> None:
        pass

    @abstractmethod
    def handle_new_time(self, trigger_time: datetime.time) -> None:
        pass

    @abstractmethod
    def handle_new_room_state(self, new_room_state_value: RoomStateValue) -> None:
        pass

    @abstractmethod
    def handle_new_cube_action(self, cube_action, action_value=None) -> None:
        pass


class Room(ABC):  # pragma: no cover
    """
    Describes the services offered by a Room
    """

    @abstractmethod
    def event_handler(self) -> EventHandler:
        pass

    @abstractmethod
    def turn_on_lights_with_light_mode(self, lights: [Light], light_mode: LightMode):
        pass

    @abstractmethod
    def turn_off_lights(self, lights: [Light]):
        pass

    @abstractmethod
    def toggle_lights(self, lights: [Light]):
        pass

    @abstractmethod
    def time(self):
        pass

    @abstractmethod
    def get_state(self, entity_id: EntityId):
        pass

    @abstractmethod
    def run_in(self, callback, seconds: int, **kwargs):
        pass

    @abstractmethod
    def switch_to_room_state(self, room_state: RoomStateValue):
        pass

    @abstractmethod
    def activate_scene(self, scene: Scene):
        pass
