from datetime import time


try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.common.time_period_24h import TimePeriod24H
except ModuleNotFoundError:  # pragma: no cover
    from common.time_period_24h import TimePeriod24H

NORMAL = 370
WARM = 450

EVENTS = {
    'xiaomi_click': 'xiaomi_aqara.click',
    'motion': 'xiaomi_aqara.motion',
    'cube_action': 'xiaomi_aqara.cube_action'
}

CUBE_ACTIONS = {
    # Simple mapping to decouple action from actual event name (in case it evolves)
    'flip90': 'flip90',
    'flip180': 'flip180',
    'shake_air': 'shake_air',
    'tap_twice': 'tap_twice',
    'rotate': 'rotate'
}

CLICK_TYPES = {
    # Simple mapping to decouple action from actual event name (in case it evolves)
    'single': 'single',
    'double': 'double',
    'hold': 'hold'
}

LIGHT = {
    'evening': {
        'brightness_pct': 50,
        'color_temp': WARM
    },
    'night': {
        'brightness_pct': 10,
        'color_temp': WARM
    },
    'dawn': {
        'brightness_pct': 70,
        'color_temp': NORMAL
    },
    'day': {
        'brightness_pct': 100,
        'color_temp': NORMAL
    },
    'sleep_before_day': {
        'brightness_pct': 10,
        'color_temp': WARM,
        'color_name': 'red'
    },
    'sleep_after_day': {
        'brightness_pct': 50,
        'color_temp': WARM
    }
}

TIMES = {
    # Need to be BEFORE midnight (or update logic of automatic lights)
    'evening': time(hour=21),
    # Need to be AFTER midnight (or update logic of automatic lights)
    'night': time(hour=0, minute=1),
    'dawn': time(hour=5, minute=30),
    'day': time(hour=7, minute=0),
    'sleep_period_start': time(hour=19),
    'sleep_period_end': time(hour=7),
    'reset_to_normal_state': time(hour=10)
}

PERIODS = {
    'night': TimePeriod24H(start=TIMES['night'], end=TIMES['dawn']),
    'dawn': TimePeriod24H(start=TIMES['dawn'], end=TIMES['day']),
    'day': TimePeriod24H(start=TIMES['day'], end=TIMES['evening']),
    'evening': TimePeriod24H(start=TIMES['evening'], end=TIMES['night']),
    'sleep_until_day': TimePeriod24H(start=TIMES['sleep_period_start'], end=TIMES['day'])
}

DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S = 300  # 5 min

IDS = {
    'etage.couloir.gateway': 'light.xiaomi_gateway_etage',  # Xiaomi
    'etage.couloir.illumination': 'sensor.xiaomi_gateway_etage_illumination',  # Xiaomi
    'etage.couloir.light.plafond': 'light.couloir_etage',  # Hue
    'etage.couloir.motion': 'binary_sensor.couloir_etage_mouvement',  # Xiaomi
    'etage.flo.light.ambiance': 'light.flo_ambiance',  # Hue
    'etage.flo.light.plafond': 'light.flo_plafond',  # Hue
    'etage.flo.motion': 'binary_sensor.flo_mouvement',  # Xiaomi
    'etage.flo.room_state': 'input_select.room_state_flo',
    'etage.flo.switch': 'binary_sensor.flo_interrupteur',  # Xiaomi
    'etage.flo.window.1': 'binary_sensor.flo_fenetre_1',
    'etage.flo.window.2': 'binary_sensor.flo_fenetre_2',
    'etage.geo.light.plafond': 'switch.geo_plafond',  # Xiaomi - InWall
    'etage.geo.motion': 'binary_sensor.geo_mouvement',  # Xiaomi
    'etage.geo.room_state': 'input_select.room_state_geo',
    'etage.geo.switch': 'binary_sensor.geo_interrupteur',  # Xiaomi - Paddle
    'etage.geo.window': 'binary_sensor.geo_fenetre',
    'etage.papa.light.plafond': 'light.papa_plafond',  # Hue
    'etage.papa.motion': 'binary_sensor.papa_mouvement',  # Xiaomi
    'etage.papa.room_state': 'input_select.room_state_papa',
    'etage.papa.switch.1': 'binary_sensor.papa_interrupteur_1_left',  # Xiaomi - Paddle
    'etage.papa.switch.2': 'binary_sensor.papa_interrupteur_1_right',  # Xiaomi - Paddle
    'etage.papa.switch.3': 'binary_sensor.papa_interrupteur_1_both',  # Xiaomi - Paddle
    'etage.papa.switch.4': 'binary_sensor.papa_interrupteur_2',  # Xiaomi - Paddle
    'etage.papa.window.1': 'binary_sensor.papa_fenetre_1',
    'etage.papa.window.2': 'binary_sensor.papa_fenetre_2',
    'etage.papa.window.3': 'binary_sensor.papa_fenetre_3',
    'etage.sallebain.light.neon': 'switch.salle_de_bain_neon',  # Xiaomi - InWall
    'etage.sallebain.light.plafond': 'light.salle_de_bain',  # Hue
    'etage.sallebain.motion': 'binary_sensor.salle_de_bain_mouvement',  # Xiaomi
    'etage.sallebain.window': 'binary_sensor.salle_de_bain_fenetre',

    'rezchaussee.cuisine.door': 'binary_sensor.cuisine_porte',
    'rezchaussee.cuisine.light.plafond': 'switch.cuisine_plafond',  # Xiaomi - InWall
    'rezchaussee.cuisine.light.spots': 'switch.cuisine_spots',  # Sonoff
    'rezchaussee.cuisine.motion.1': 'binary_sensor.cuisine_mouvement_1',
    'rezchaussee.cuisine.motion.2': 'binary_sensor.cuisine_mouvement_2',
    'rezchaussee.cuisine.switch.extra': 'binary_sensor.cuisine_extra_interrupteur',  # Xiaomi
    'rezchaussee.cuisine.window.1': 'binary_sensor.cuisine_fenetre',
    'rezchaussee.cuisine.window.2': 'binary_sensor.cuisine_velux',
    'rezchaussee.entree.door': 'binary_sensor.entree_porte',
    'rezchaussee.entree.gateway': 'light.xiaomi_gateway_entree',  # Xiaomi
    'rezchaussee.entree.illumination': 'sensor.xiaomi_gateway_entree_illumination',  # Xiaomi
    'rezchaussee.entree.light.plafond': 'switch.entree_plafond',  # Xiaomi - InWall
    'rezchaussee.entree.motion': 'binary_sensor.entree_mouvement',  # Xiaomi
    'rezchaussee.entree.placard.bas': 'binary_sensor.entree_placard_bas',  # Xiaomi
    'rezchaussee.entree.placard.haut': 'binary_sensor.entree_placard_haut',  # Xiaomi
    'rezchaussee.salonsallemanger.door': 'binary_sensor.salle_a_manger_porte',
    'rezchaussee.salonsallemanger.home_theater': 'remote.skynethub',  # Logitech Harmony Hub
    'rezchaussee.salonsallemanger.light.ambiance_1.1': 'light.salle_a_manger_ambiance_1a',  # Hue
    'rezchaussee.salonsallemanger.light.ambiance_1.2': 'light.salle_a_manger_ambiance_1b',  # Hue
    'rezchaussee.salonsallemanger.light.ambiance_2': 'switch.salle_a_manger_ambiance_2',  # Sonoff
    'rezchaussee.salonsallemanger.light.ambiance_3': 'light.salon_ambiance',  # Hue
    'rezchaussee.salonsallemanger.light.lecture': 'switch.lecture',
    'rezchaussee.salonsallemanger.light.salle_manger_plafond': 'light.salle_a_manger',  # Hue
    'rezchaussee.salonsallemanger.light.salon_plafond.1': 'light.salon_1',  # Hue
    'rezchaussee.salonsallemanger.light.salon_plafond.2': 'light.salon_2',  # Hue
    'rezchaussee.salonsallemanger.light.salon_plafond.3': 'light.salon_3',  # Hue
    'rezchaussee.salonsallemanger.light.salon_plafond.4': 'light.salon_4',  # Hue
    'rezchaussee.salonsallemanger.magic_cube.1': 'binary_sensor.salon_cube_magique_1',
    'rezchaussee.salonsallemanger.magic_cube.2': 'binary_sensor.salon_cube_magique_2',
    'rezchaussee.salonsallemanger.magic_cube.3': 'binary_sensor.salon_cube_magique_3',
    'rezchaussee.salonsallemanger.magic_cube.4': 'binary_sensor.salon_cube_magique_4',
    'rezchaussee.salonsallemanger.motion': 'binary_sensor.salle_a_manger_mouvement',  # Xiaomi
    'rezchaussee.salonsallemanger.room_state': 'input_select.room_state_salonsallemanger',
    'rezchaussee.salonsallemanger.scene.day': 'scene.salon_salle_manger_jour',
    'rezchaussee.salonsallemanger.scene.evening': 'scene.salon_salle_manger_soir',
    'rezchaussee.salonsallemanger.scene.off': 'scene.salon_salle_manger_off',
    'rezchaussee.salonsallemanger.window.1': 'binary_sensor.salon_fenetre',
    'rezchaussee.salonsallemanger.window.2': 'binary_sensor.salon_velux',
    'rezchaussee.toilettes.light.plafond': 'light.toilettes',  # Hue
    'rezchaussee.toilettes.motion': 'binary_sensor.toilettes_mouvement',  # Xiaomi

    'cave.atelier.light.neons': 'switch.atelier_neons',  # Sonoff
    'cave.atelier.motion': 'binary_sensor.atelier_mouvement',  # Xiaomi
    'cave.cavemilieu.light.plafond': 'switch.cave_milieu_neon',  # Sonoff
    'cave.cavemilieu.motion': 'binary_sensor.cave_milieu_mouvement',  # Xiaomi
    'cave.cavevin.light.plafond': 'switch.cave_a_vin_neon',  # Sonoff
    'cave.cavevin.motion': 'binary_sensor.cave_a_vin_mouvement',  # Xiaomi

    'soussol.buanderie.light.plafond': 'light.buanderie',  # Hue
    'soussol.buanderie.motion': 'binary_sensor.buanderie_mouvement',  # Xiaomi
    'soussol.buanderie.window': 'binary_sensor.buanderie_fenetre',  # Xiaomi
    'soussol.bureau.button': 'binary_sensor.bureau_boutton',  # Xiaomi
    'soussol.bureau.interrupteur': 'binaryblabal',  # Xiaomi - Paddle #TODO: To Pair!
    'soussol.bureau.light.neons': 'switch.bureau_neons',  # Sonoff
    'soussol.bureau.light.plafond': 'light.bureau_plafond',  # Hue
    'soussol.bureau.motion.1': 'binary_sensor.bureau_mouvement_1',  # Xiaomi
    'soussol.bureau.motion.2': 'binary_sensor.bureau_mouvement_2',  # Xiaomi
    'soussol.bureau.window.1': 'binary_sensor.bureau_fenetre_1',  # Xiaomi
    'soussol.bureau.window.2': 'binary_sensor.bureau_fenetre_2',  # Xiaomi
    'soussol.couloir.light.plafond': 'light.couloir_bureau',  # Hue
    'soussol.couloir.motion': 'binary_sensor.couloir_bureau_mouvement',  # Xiaomi
    'soussol.garage.light.neon': 'switch.garage_neon',  # Xiaomi - InWall
    'soussol.garage.light.plafond': 'light.garage_plafond',  # Hue(TODO Not sure) #TODO: To Pair!
    'soussol.garage.motion': 'binary_sensor.garage_mouvement',  # Xiaomi #TODO: To Pair!
    'soussol.garage.window': 'binary_sensor.garage_porte',

    'outside.cabanevelo.light.neon': 'switch.cabane_velo_neon',  # Sonoff #TODO: To Pair!
    'outside.cabanevelo.mouvement': 'binary_sensor.cabane_velo_mouvement',  # Xiaomi #TODO: To Pair!
    'outside.terrasse.light.arbre': 'switch.terrasse_arbre',  # Sonoff
    'outside.terrasse.light.coin_repas': 'switch.terrasse_coin_repas',  # Xiaomi - InWall - Located in Kitchen
    'outside.terrasse.light.spots': 'switch.terrasse_spots',  # Sonoff

    'other.button.rallonge': 'binary_sensor.rallonge_boutton',
    'other.plug.rallonge': 'switch.rallonge',
    'other.sun': 'sun.sun',
    'other.vacation_mode': 'input_boolean.mode_vacances',

    'workaround.button': 'binary_sensor.switch_158d0001bd0d5c'
}


def all_windows_and_doors_ids():
    def is_window_or_door(entity_id):
        if 'fenetre' in entity_id:
            return True
        if 'porte' in entity_id:
            return True
        if 'velux' in entity_id:
            return True
        return False

    return [id_ for id_ in IDS.values() if is_window_or_door(id_)]


def all_motion_sensors_ids():
    return [id_ for id_ in IDS.values() if 'mouvement' in id_]
