import datetime
import math
from abc import abstractmethod
from typing import Optional, Mapping
from uuid import uuid4

try:
    # Module namespaces when Automation Modules are loaded in AppDaemon
    # is different from the 'real' python one.
    # Appdaemon doesn't seem to take into account packages
    from apps.common.constants import LIGHT, TIMES, PERIODS, \
        DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, EVENTS, IDS
    from apps.common.utilities import TimePeriod24H
    from apps.common.types import Light, EntityId, EventHandler, Room, \
        RoomStateValue, LightMode, Entity, ClickEntity, \
        MotionSensor, RoomState, Scene, CubeAction
except ModuleNotFoundError:  # pragma: no cover
    from common.constants import LIGHT, TIMES, PERIODS, \
        DEFAULT_TIMEOUT_FOR_MANUAL_LIGHTS_IN_S, EVENTS, IDS
    from common.utilities import TimePeriod24H
    from common.types import Light, EntityId, EventHandler, Room, \
        RoomStateValue, LightMode, Entity, ClickEntity, \
        MotionSensor, RoomState, Scene, CubeAction


class CoREventHandler(EventHandler):
    """
    Abstract Chain-of-responsibility Event Handler
    For each different type of events, it tries to handle the event and then decides to delegate
    or not to successor.

    Each method returns True if the processing of the event is done and the successor should **not**
    be called.
    If False, the successor **will** be called, even if the event was already partially processed.
    """

    _room: Room

    def __init__(self, successor: Optional['CoREventHandler']) -> None:
        if successor is None:  # pragma: no cover
            raise ValueError(
                    f"Be sure to always end the chain with a '{str(EndOfCoRInjector)}'")

        self._successor = successor
        self._room = successor._room

    def handle_new_motion(self) -> None:
        handled = self._process_new_motion()
        if not handled:
            self._successor.handle_new_motion()

    def handle_no_more_motion(self) -> None:
        handled = self._process_no_more_motion()
        if not handled:
            self._successor.handle_no_more_motion()

    def handle_new_click(self) -> None:
        handled = self._process_new_click()
        if not handled:
            self._successor.handle_new_click()

    def handle_new_time(self, trigger_time: datetime.time) -> None:
        handled = self._process_new_time(trigger_time)
        if not handled:
            self._successor.handle_new_time(trigger_time)

    def handle_new_room_state(self,
                              new_room_state_value: RoomStateValue) -> None:
        handled = self._process_new_room_state(new_room_state_value)
        if not handled:
            self._successor.handle_new_room_state(new_room_state_value)

    def handle_new_cube_action(self, cube_action, action_value=None) -> None:
        handled = self._process_new_cube_action(cube_action, action_value)
        if not handled:
            self._successor.handle_new_cube_action(cube_action, action_value)

    def _current_time_is_in_period(self, period):
        current_time = self._room.time()
        return PERIODS[period].contains(current_time)

    def _normal_light_mode(self) -> LightMode:
        if self._current_time_is_in_period('evening'):
            return 'evening'
        elif self._current_time_is_in_period('night'):
            return 'night'
        elif self._current_time_is_in_period('dawn'):
            return 'dawn'
        elif self._current_time_is_in_period('day'):
            return 'day'
        else:
            raise ValueError('Current time is not correctly handled')

    @abstractmethod
    def _process_new_motion(self) -> bool:  # pragma: no cover
        pass

    @abstractmethod
    def _process_no_more_motion(self) -> bool:  # pragma: no cover
        pass

    @abstractmethod
    def _process_new_click(self) -> bool:  # pragma: no cover
        pass

    @abstractmethod
    def _process_new_time(self,
                          trigger_time: datetime.time) -> bool:  # pragma: no cover
        pass

    @abstractmethod
    def _process_new_room_state(self,
                                new_room_state_value: RoomStateValue) -> bool:  # pragma: no cover
        pass

    @abstractmethod
    def _process_new_cube_action(self, cube_action,
                                 action_value) -> bool:  # pragma: no cover
        pass


class CoREventHandlerWithDefault(CoREventHandler):
    _default: bool

    def __init__(self, successor: Optional['CoREventHandler'],
                 default=False) -> None:
        super().__init__(successor)
        self._default = default

    def _process_new_motion(self) -> bool:
        return self._default

    def _process_no_more_motion(self) -> bool:
        return self._default

    def _process_new_click(self) -> bool:
        return self._default

    def _process_new_time(self, trigger_time: datetime.time) -> bool:
        return self._default

    def _process_new_room_state(self,
                                new_room_state_value: RoomStateValue) -> bool:
        return self._default

    def _process_new_cube_action(self, cube_action, action_value) -> bool:
        return self._default


class EndOfCoRInjector(CoREventHandlerWithDefault):
    """
    Defines the end of the Chain-of-Responsibilities and make available
    dependencies to all links in the chain
    """

    # noinspection PyMissingConstructor
    def __init__(self, room: Room) -> None:
        def ensure_not_none(dependency):
            if dependency is None:  # pragma: no cover
                raise ValueError("Can not be None!")
            return dependency

        self._default = True
        self._room = ensure_not_none(room)


class AutomaticLightsCoREH(CoREventHandlerWithDefault):
    def __init__(self,
                 lights: [Light],
                 motion_sensors: [MotionSensor],
                 successor: 'CoREventHandler') -> None:
        super().__init__(successor)
        self._lights = lights
        self._motion_sensors = motion_sensors

    def _process_new_motion(self) -> bool:
        light_mode = self._normal_light_mode()
        self._room.turn_on_lights_with_light_mode(self._lights, light_mode)
        return False

    def _process_no_more_motion(self) -> bool:
        if self.all_motion_off():
            self._room.turn_off_lights(self._lights)
        return False

    def all_motion_off(self):
        for motion_sensor in self._motion_sensors:
            if self._room.get_state(motion_sensor.entity_id) == 'on':
                return False
        return True

    @property
    def lights(self):
        return self._lights


class AlwaysDayAutomaticLightsCoREH(AutomaticLightsCoREH):
    def _process_new_motion(self) -> bool:
        self._room.turn_on_lights_with_light_mode(self._lights, 'day')
        return False


class ExtraDelayCoREH(CoREventHandlerWithDefault):
    def __init__(self,
                 extra_delay_in_min,
                 successor: AutomaticLightsCoREH) -> None:
        super().__init__(successor)
        self._delay = extra_delay_in_min
        self._auto_lights_successor = successor
        self._scheduled_callbacks_uuids = []

    def _process_no_more_motion(self) -> bool:
        self._schedule_shutdown_callback()
        return True

    def _schedule_shutdown_callback(self):
        def turn_off_lights_callback(kwargs):
            if kwargs['uuid'] not in self._scheduled_callbacks_uuids:
                return

            self._scheduled_callbacks_uuids.remove(kwargs['uuid'])
            if self._auto_lights_successor.all_motion_off():
                self._room.turn_off_lights(self._auto_lights_successor.lights)

        uuid = uuid4()
        self._scheduled_callbacks_uuids.append(uuid)
        self._room.run_in(turn_off_lights_callback, self._delay * 60, uuid=uuid)

    def _cancel_scheduled_callbacks(self):
        self._scheduled_callbacks_uuids.clear()

    def _process_new_motion(self) -> bool:
        self._cancel_scheduled_callbacks()
        return False


class ExtraDelayDuringPeriodCoREH(ExtraDelayCoREH):
    def __init__(self,
                 extra_delay_in_min,
                 period: TimePeriod24H,
                 successor: AutomaticLightsCoREH) -> None:
        super().__init__(extra_delay_in_min, successor)
        self.period = period

    def _process_no_more_motion(self) -> bool:
        if self.period.contains(self._room.time()):
            return super()._process_no_more_motion()
        else:
            return False


class SleepModeCoREH(CoREventHandlerWithDefault):

    def __init__(self, sleep_mode_entity_id: str, lights: [Light],
                 successor: 'CoREventHandler') -> None:
        super().__init__(successor)
        self.sleep_mode_entity_id = sleep_mode_entity_id
        self.lights = lights

    def _process_new_motion(self) -> bool:
        if self._in_sleep_mode():
            if self._before_day():
                light_mode = 'sleep_before_day'
            else:
                light_mode = 'sleep_after_day'
            self._room.turn_on_lights_with_light_mode(self.lights, light_mode)
            return True
        else:
            return False

    def _in_sleep_mode(self):
        return self._room.get_state(IDS['etage.papa.room_state']) == 'Sommeil'

    def _before_day(self):
        return PERIODS['sleep_until_day'].contains(self._room.time())


class ManualLightsOnTimeoutCoREH(CoREventHandlerWithDefault):
    _shutdown_cancelled: bool

    def __init__(self,
                 manual_lights_on_timeout: [Light],
                 timeout_in_s: int,
                 successor: Optional['CoREventHandler']) -> None:
        super().__init__(successor)
        self._manual_lights_on_timeout = manual_lights_on_timeout
        self._timeout_in_s = timeout_in_s
        self._scheduled_callbacks_uuids = []

    def _process_new_click(self) -> bool:
        self._room.toggle_lights(self._manual_lights_on_timeout)
        return False

    def _process_new_motion(self) -> bool:
        self._cancel_scheduled_secondary_light_shutdown()
        return False

    def _process_no_more_motion(self) -> bool:
        self._schedule_secondary_light_shutdown()
        return False

    def _schedule_secondary_light_shutdown(self):
        def turn_off_manual_lights_on_timeout(kwargs):
            if kwargs['uuid'] in self._scheduled_callbacks_uuids:
                self._scheduled_callbacks_uuids.remove(kwargs['uuid'])
                self._room.turn_off_lights(self._manual_lights_on_timeout)

        uuid = uuid4()
        self._scheduled_callbacks_uuids.append(uuid)
        self._room.run_in(turn_off_manual_lights_on_timeout, self._timeout_in_s,
                          uuid=uuid)

    def _cancel_scheduled_secondary_light_shutdown(self):
        self._scheduled_callbacks_uuids.clear()


class ActivateSceneOnRoomStateChange(CoREventHandlerWithDefault):
    _room_state_scene_mapping: Mapping[RoomStateValue, Scene]

    def __init__(self,
                 room_state_scene_mapping: Mapping[RoomStateValue, Scene],
                 successor: Optional['CoREventHandler']) -> None:
        super().__init__(successor)
        self._room_state_scene_mapping = room_state_scene_mapping

    def _process_new_room_state(self,
                                new_room_state_value: RoomStateValue) -> bool:
        if new_room_state_value in self._room_state_scene_mapping:
            scene = self._room_state_scene_mapping[new_room_state_value]
            self._room.activate_scene(scene)
        return False


class UpdateRoomStateOnCubeAction(CoREventHandlerWithDefault):
    _cube_action_room_state_mapping: Mapping[CubeAction, RoomStateValue]
    _rotate_cutoff: int

    def __init__(self,
                 cube_action_room_state_mapping: Mapping[
                     CubeAction, RoomStateValue],
                 rotate_cutoff: int,
                 successor: Optional['CoREventHandler']) -> None:
        super().__init__(successor)
        self._cube_action_room_state_mapping = cube_action_room_state_mapping
        self._rotate_cutoff = rotate_cutoff

    def _process_new_cube_action(self, cube_action, action_value) -> bool:
        if cube_action == 'rotate':
            return self._process_rotate_action(rotate_value=action_value)
        else:
            return self._process_regular_action(cube_action, action_value)

    def _process_rotate_action(self, rotate_value) -> bool:
        if not rotate_value:
            raise ValueError(
                    f"Cube action: 'rotate' should have an action value")

        if math.fabs(rotate_value) >= self._rotate_cutoff:
            if 'rotate__above_cutoff' not in self._cube_action_room_state_mapping:
                return False

            room_state = self._cube_action_room_state_mapping[
                'rotate__above_cutoff']
        else:
            if 'rotate__below_cutoff' not in self._cube_action_room_state_mapping:
                return False

            room_state = self._cube_action_room_state_mapping[
                'rotate__below_cutoff']
        self._room.switch_to_room_state(room_state)

        return False

    def _process_regular_action(self, cube_action, action_value) -> bool:
        if cube_action in self._cube_action_room_state_mapping:
            if action_value is not None:
                raise ValueError(
                        f"Cube action: '{cube_action}' can not have an action value")
            room_state = self._cube_action_room_state_mapping[cube_action]
            self._room.switch_to_room_state(room_state)

        return False
