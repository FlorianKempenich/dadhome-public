#!/bin/bash

function kill_and_restart {
    docker-compose kill $1 \
        && docker-compose rm -f $1 \
        && docker-compose up -d $1
}

kill_and_restart 'transfer_sh'
kill_and_restart 'nginx'

docker-compose logs -f nginx transfer_sh
