import os
import yaml
from sseclient import SSEClient
import homeassistant.remote as remote
import requests


def _relative_to_this_file(path):
    return os.path.join(os.path.dirname(__file__), path)


SECRETS_FILE = _relative_to_this_file('../../homeassistant/config/secrets.yaml')
CONFIG_FILE = _relative_to_this_file('../config/config.yaml')

HASS_ENDPOINT = 'https://dad-home.access.ly'
HASS_PORT = '443'
CHROMECAST_PORT = '8008'

class Api:
    def __init__(self):
        self.secrets = self._load_file(SECRETS_FILE)
        self.config = self._load_file(CONFIG_FILE)
        self.hass_api = remote.API(
            host=HASS_ENDPOINT,
            port=HASS_PORT,
            api_password=self.secrets['http_password'])

    @staticmethod
    def _load_file(file_to_load):
        with open(file_to_load) as f:
            return yaml.load(f)

    def add_new_xiaomi_device(self, *, gateway):

        def get_gateway_mac_address(gateway):
            gateway_mac_key_mapping = {
                'entree': 'xiaomi_gateway_entree_mac',
                'etage': 'xiaomi_gateway_etage_mac'}
            if gateway not in gateway_mac_key_mapping:
                valid_gateway_names = gateway_mac_key_mapping.keys()
                raise Exception(
                    f'Invalid gateway name | Valid names: {valid_gateway_names}')
            key = gateway_mac_key_mapping[gateway]
            return self.secrets[key]

        remote.call_service(self.hass_api, 'xiaomi_aqara', 'add_device', {
            'gw_mac': get_gateway_mac_address(gateway)
        })

    def send_notif(self, message):
        remote.call_service(self.hass_api, 'notify', 'pushbullet', {
            'title': 'From HassToolbox',
            'message': message,
            'target': 'device/OnePlus 5T'
        })

    def reload_automations(self):
        remote.call_service(self.hass_api, 'automation', 'reload')

    def get_event_stream(self):
        """
        Return a generator representing the stream of events sent by home assistant
        """
        url = HASS_ENDPOINT + ':' + HASS_PORT + '/api/stream'
        headers = {
            "Content-Type": "application/json",
            "x-ha-access": self.secrets['http_password']
        }
        events = SSEClient(url, headers=headers)
        for event in events:
            yield event

    def reboot_all_chromecasts(self):
        chromecasts = self.config['StaticIps']['Chromecast']
        for chromecast in chromecasts:
            ip = chromecasts[chromecast]['IP']
            res = requests.post(f'http://{ip}:{CHROMECAST_PORT}/setup/reboot', json={'params': "now"})
            if res.status_code == 200:
                print(f'Chromecast rebooted: {chromecast}')
            else:
                print(f'Problem trying to reboot: {chromecast}')
