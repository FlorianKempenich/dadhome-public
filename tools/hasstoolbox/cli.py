import time
import click
from api import Api
from pygments import highlight, lexers, formatters
import json


def _show_progress_bar_timer(seconds=30, label=''):
    colored_bar_template = ''.join([
        '%(label)s ',
        click.style('◀', fg='green'),
        '%(bar)s',
        click.style('▶', fg='green'),
        '%(info)s',
    ])
    with click.progressbar(
            range(1, seconds),
            label=click.style(label, fg='green'),
            color='green',
            fill_char=click.style(u'●', fg='green'),
            empty_char=click.style(u'◦', fg='green'),
            bar_template=colored_bar_template,
            show_eta=False,
            show_percent=False) as progressbar:

        for i in progressbar:
            time.sleep(1)


@click.group()
def cli():
    pass


@click.command()
@click.argument('message')
def send_notif(message):
    api = Api()
    api.send_notif(message)
    click.echo(f"Message Sent!")


@click.group()
def add_new_xiaomi_device():
    pass


@click.command()
def entree():
    _add_new_xiaomi_device('entree')


@click.command()
def etage():
    _add_new_xiaomi_device('etage')


def _add_new_xiaomi_device(gateway):
    api = Api()
    api.add_new_xiaomi_device(gateway=gateway)
    click.secho('Entering Pairing Mode', bg='green', fg='black')
    click.secho('')
    click.secho('You now have 30 seconds to add a device', fg='green')
    _show_progress_bar_timer(seconds=31)
    click.secho('')
    click.secho('Exiting Pairing Mode', bg='green', fg='black')


@click.command()
def remove_xiaomi_device():
    click.secho("DOING NOTHING: To remove a device, there is no need to call this service",
                bg='green', fg='black', dim=True)
    click.secho("                                                                        ",
                bg='green', fg='black')
    click.secho("To remove a device, simply:                                             ",
                bg='green', fg='black')
    click.secho("* Go on the Mi App                                                      ",
                bg='green', fg='black')
    click.secho("* Select the Gateway with which the device is paired                    ",
                bg='green', fg='black')
    click.secho("* Long-press on the device                                              ",
                bg='green', fg='black')
    click.secho("* Select 'delete'                                                       ",
                bg='green', fg='black')
    click.secho("* And you're done :)                                                    ",
                bg='green', fg='black')


@click.command()
def reload_automations():
    api = Api()
    api.reload_automations()
    click.secho('Done!', fg='green')


@click.command()
def stream_events():
    api = Api()
    event_stream = api.get_event_stream()
    click.secho('Streaming events | Press CTRL+C to stop', fg='green')
    click.secho('')

    def event_to_json_string(event):
        def is_json_object(string):
            return string.startswith('{')

        def wrap_string_in_quotes(string):
            return '"' + string + '"'

        str_event = str(event)
        if is_json_object(str_event):
            return str_event
        return wrap_string_in_quotes(str_event)

    for event in event_stream:

        formatted_event = highlight(
            json.dumps(
                json.loads(
                    event_to_json_string(event)),
                sort_keys=True,
                indent=2),
            lexers.JsonLexer(),
            formatters.TerminalFormatter())

        click.secho(formatted_event)
        click.echo('')

@click.command()
def reboot_all_chromecasts():
    api = Api()
    api.reboot_all_chromecasts()

cli.add_command(send_notif)
cli.add_command(remove_xiaomi_device)
cli.add_command(reload_automations)
cli.add_command(stream_events)
add_new_xiaomi_device.add_command(etage)
add_new_xiaomi_device.add_command(entree)
cli.add_command(add_new_xiaomi_device)
cli.add_command(reboot_all_chromecasts)
