#!/bin/bash

IP="192.168.1.135"

curl http://$IP:8008/setup/reboot \
    -H "Content-Type: application/json" \
    -d '{"params":"now"}' \
    -X POST
