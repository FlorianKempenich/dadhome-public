#!/bin/bash

EMAIL='Flori@nKempenich.com'

function generate_args {
    cert_name=$1
    domains="${@:2}"

    printf -- '--standalone '
    printf -- '--cert-name %s ' $cert_name
    for domain in $domains; do
        printf -- '-d %s ' $domain
    done
    printf -- '--non-interactive '
    printf -- '--agree-tos '
    printf -- "-m $EMAIL "
    # printf -- '--dry-run '
}

function generate_cert {
    cert_name=$1
    domains="${@:2}"

    sudo $(asdf which certbot) certonly $(generate_args $cert_name $domains)
}

sudo rm -rf /etc/letsencrypt
generate_cert 'hass' 'dad-home.access.ly' 'dadhome.floriankempenich.com'
generate_cert 'transfersh' 'transfersh.floriankempenich.com'
generate_cert 'dadtadlminion' 'dadtadlminion.floriankempenich.com'
