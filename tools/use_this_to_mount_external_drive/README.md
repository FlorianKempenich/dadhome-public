# To mount external drive
- Format it in NTFS
- Check the contents of the included `fstab` file in this directory
- Understand and reproduce in `/etc/fstab`
    - Need `sudo`

## Explanation
### `fstab` options
- `nofail`: Don't make the system enter emergency mode if this drive wasn't found
- `...device-timeout`: Wait only Xs before giving up on trying to start this device
