
# Before using the tool:

1. **Build** with `tools/huemanagementcli/build.sh`

2. **Ensure Registered:** 
  * `homeassistant/config/.huemanagementcli-token` should contain a valid token
  * _If not:_ Call `./huemanagementcli register USERNAME`
    * `USERNAME` can be anything

2. **Ensure Static IP correct:** 
  * `tools/config/config.yaml` should contain the correct `IP` for `HueBridge` in `StaticIps` section
