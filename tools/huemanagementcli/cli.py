import os
import json
import click
import yaml
import docker
import requests
from pygments import highlight, lexers, formatters


def _relative_to_this_file(path):
    return os.path.join(os.path.dirname(__file__), path)


DOCKER_IMAGE_NAME = 'floriankempenich/hue-management-cli'
TOKEN_FILE = _relative_to_this_file(
    '../../homeassistant/config/.huemanagementcli-token')
CONFIG_FILE = _relative_to_this_file('../config/config.yaml')


def _hue_gateway_ip():
    with open(CONFIG_FILE) as f:
        config = yaml.load(f)
        return config['StaticIps']['Standalone']['HueBridge']['IP']


def _hue_gateway_user_token():
    with open(TOKEN_FILE) as f:
        return f.readline().rstrip('\n')


AVAILABLE_ZIGBEE_CHANNELS = [11, 15, 20, 25]


class RawApi:
    @staticmethod
    def _raw_url(path):
        return 'http://' + \
            _hue_gateway_ip() + \
            '/api/' \
            + _hue_gateway_user_token() \
            + '/' \
            + path

    @staticmethod
    def _print_response(res):
        print(highlight(
            json.dumps(
                json.loads(
                    res.content.decode('utf-8')),
                sort_keys=True,
                indent=2),
            lexers.JsonLexer(),
            formatters.TerminalFormatter()))

    def get_and_print(self, path):
        res = requests.get(self._raw_url(path))
        self._print_response(res)

    def put_and_print(self, path, data={}):
        res = requests.put(self._raw_url(path), data=json.dumps(data))
        self._print_response(res)

    def post_and_print(self, path, data={}):
        res = requests.post(self._raw_url(path), data=json.dumps(data))
        self._print_response(res)


class DirectCommands:
    """
    Class to handle commands not supported by 'hueadm'.
    It calls the REST API directly.
    """

    def __init__(self, raw_api):
        self.raw_api = raw_api

    def update_zigbee_channel(self, channel):
        """
        Update the zigbee channel to try and avoid clashes with WiFi channels.
        See: https://support.metageek.com/hc/en-us/articles/203845040-ZigBee-and-WiFi-Coexistence
        """
        if channel not in AVAILABLE_ZIGBEE_CHANNELS:
            raise Exception(
                f'{channel} is not a valid zigbee channel | Supported channels: {str(AVAILABLE_ZIGBEE_CHANNELS)}')
        self.raw_api.put_and_print('config', {'zigbeechannel': channel})

    def activate_auto_update(self):
        """
        See: https://developers.meethue.com/documentation/software-update
        """
        self.raw_api.put_and_print(
            'config', {'swupdate2': {'autoinstall': {'on': True}}})

    def trigger_manual_install(self):
        self.raw_api.put_and_print(
            'config', {'swupdate2': {'install': True}})


class HueadmDockerWrapper:
    """
    Docker wrapper for the 'hueadm' nodejs cli tool
    """

    def __init__(self, hue_mgmt_command_as_tuple):
        def build_command(hue_mgmt_command_as_tuple, *, default):
            hue_mgmt_command = ' '.join(hue_mgmt_command_as_tuple)
            if hue_mgmt_command == '':
                hue_mgmt_command = default
            return '-H ' + _hue_gateway_ip() + ' ' \
                + ' -U ' + _hue_gateway_user_token() + ' '  \
                + hue_mgmt_command

        self.hue_mgmt_command = build_command(
            hue_mgmt_command_as_tuple, default='help')

    def run(self):
        def hack_replace_command_name_in_help(res):
            # Only the first 2 occurences (from 'Usage' section)
            return res.replace('hueadm', './huemanagementcli hueadmwrapper', 2)

        res = docker.from_env().containers.run(
            DOCKER_IMAGE_NAME,
            command=self.hue_mgmt_command,
            remove=True)
        return hack_replace_command_name_in_help(
            res.decode('utf-8'))


@click.group()
def cli():
    pass


@click.group(short_help="Direct commands not supported by the 'hueadm' nodejs tool")
def direct_commands():
    pass


@click.command()
@click.argument('channel_number', nargs=1, type=int)
def update_zigbee_channel(channel_number):
    direct_cmds = DirectCommands(RawApi())
    direct_cmds.update_zigbee_channel(channel_number)


@click.command()
def activate_auto_update():
    direct_cmds = DirectCommands(RawApi())
    direct_cmds.activate_auto_update()

@click.command()
def trigger_manual_install():
    direct_cmds = DirectCommands(RawApi())
    direct_cmds.trigger_manual_install()


@click.command(short_help="Docker wrapper for the 'hueadm' nodejs tool")
@click.argument('hue_mgmt_command', nargs=-1, required=False)
def hueadm(hue_mgmt_command):
    hueadm_wrapper = HueadmDockerWrapper(hue_mgmt_command)
    res = hueadm_wrapper.run()
    click.echo(res)


direct_commands.add_command(update_zigbee_channel)
direct_commands.add_command(activate_auto_update)
direct_commands.add_command(trigger_manual_install)
cli.add_command(direct_commands)
cli.add_command(hueadm)
