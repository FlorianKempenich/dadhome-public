#!/bin/bash

IMAGE_NAME='floriankempenich/hue-management-cli'
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker build -t $IMAGE_NAME $SCRIPT_DIR
