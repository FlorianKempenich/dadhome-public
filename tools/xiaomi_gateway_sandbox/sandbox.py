import json

from xiaomi_gateway import XiaomiGatewayDiscovery, XiaomiGateway

FILE = 'debug.json'


def debug_func(*args):
    print('in debug func')
    print(args)


def file_name(gateway: XiaomiGateway):
    return gateway.ip_adress.split('.')[-1] + '.json'


def write_devices_to_file(gateway: XiaomiGateway):
    with open(file_name(gateway), 'w') as file:
        dict_as_str = json.dumps(dict(gateway.devices))
        file.write(dict_as_str)


def read_devices_from_file(filename) -> dict:
    with open(filename) as file:
        return json.loads(file.read())


def discover_gateways() -> [XiaomiGateway]:
    xiaomi = XiaomiGatewayDiscovery(debug_func, [], 'any')
    xiaomi.discover_gateways()

    g1: XiaomiGateway = xiaomi.gateways['192.168.1.24']
    g2: XiaomiGateway = xiaomi.gateways['192.168.1.26']
    return [g1, g2]


def print_number_of_devices(devices):
    print('binary_sensor: ' + str(len(devices['binary_sensor'])))
    print('sensor: ' + str(len(devices['sensor'])))
    print('switch: ' + str(len(devices['switch'])))
    print('light: ' + str(len(devices['light'])))

    total = len(devices['binary_sensor']) + len(devices['sensor']) + len(devices['switch']) + len(devices['light'])
    print('total: ' + str(total))


if __name__ == '__main__':
    devices = read_devices_from_file('24.json')
    print('24')
    print_number_of_devices(devices)
    print('')

    devices = read_devices_from_file('26.json')
    print('26')
    print_number_of_devices(devices)
    print('')

    # res = '192.168.1.26'.split('.')[-1]
    # print(res)
    #
    # gateways = discover_gateways()

    # for g in gateways:
    #     write_devices_to_file(g)
    #     print(g)
    #     print(g.sid)
    #     print(g.ip_adress)
    #     # pprint(devices(g))

    # write_to_file({})

    print('done')
