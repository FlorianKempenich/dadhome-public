# Sonoff switch Tutorial

---------
## Steps

1. [Requirements](#1-requirements)
1. [Configure devices](#2-configure-devices)

--------- 

## 1. Requirements
### Install drivers
> **/!\ Important Note /!\ **  
> If drivers not installed it might **look** like it works ... but it really doesn't.  
> **Always ensure the drivers are installed**

* Check which chip being used on the FTDI Usb adapter
  * Using `CP2102` with the current adapter.
* Find and install drivers
  * [Drivers for the `CP2102`](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)

### Prepare Hardware
* Setup connection following this guide: [Hardware Preparation - Serial connection](https://github.com/arendst/Sonoff-Tasmota/wiki/Hardware-Preparation)
  *    **WARNING:**   
       In the guide the `RX/TX` are crossed.
       This is how it's supposed to be, however, I found on **my modules** that the 
       `RX/TX` should **NOT** be crossed.  
       So ... try both :) 
  *    **WARNING 2:**   
       Do **not** connect `VCC` to `5V` on the USB adapter, but look for the `3V3` port.

### Install `platformio`
* `pipenv install platformio --two`
  * **Use `--two`** <-- `platformio` doesn't support python 3



## 2. Configure devices

**Run the steps 1 by 1:**

1. `A_build.sh`
1. `B_find_device.sh`
1. `C_upload.sh`- 1x for **each** device
1. `C_upload.sh`- 1x for **each** device
1. ...
1. `D_pair_with_wifi.sh`
1. `E_configure_ALL_devices_remotely.py`
1. `F_clean.sh`

> ### Steps performed
> #### `platformio.ini`
> * Uncomment: `env_default = sonoff`
> 
> #### `user_config.h`
> * Use the `TasmotaMqtt` library in `MQTT_LIBRARY_TYPE`
>   * we don't need `TLS` on local network
> * Enable HomeAssistant discovery: `HOME_ASSISTANT_DISCOVERY_ENABLE` set to `1`
> * Don't set the wifi password
> * Don't set anything else
