#!/bin/bash

if [ "$1" == "" ]; then
    echo "MISSING DEVICE"
    echo "Usage: ./upload.sh DEVICE"
    exit 1
fi

cd ./Sonoff-Tasmota
platformio run -t upload --upload-port $1
