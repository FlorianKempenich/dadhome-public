#!/bin/bash

echo ''
echo 'Setup on device:'
echo ' - Click 4 Times on the button to enter Hotspot mode'
echo ' - The device LED should start blinking (TO CONFIRM)'
echo ' - Connect to the hotspot wifi on the switch'
echo ' - Configure the wifi SSID & password'
echo ''
echo 'DO THIS FOR ALL DEVICES BEFORE NEXT STEP'
echo ''
echo 'Setup on project:'
echo ' - Wait all devices have IP'
echo "   -> Use 'Fing' on Android to quickly find the IPs"
echo ' - Update the `Sonoff -> devices` section in `../config/config.yaml`'
echo ''
