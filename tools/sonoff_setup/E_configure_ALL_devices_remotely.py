#!/usr/bin/env python

import os
import requests
import yaml


def _relative_to_this_file(path):
    return os.path.join(os.path.dirname(__file__), path)


SECRETS_FILE = _relative_to_this_file('../config/secrets.yaml')
CONFIG_FILE = _relative_to_this_file('../config/config.yaml')
TIMEOUT_S=2


def load_sonoff_section(file_path):
    with open(file_path) as f:
        return yaml.load(f)['Sonoff']


class NotReachable(Exception):
    pass

class SonoffApi:
    def __init__(self, ip, web_password=None):
        self.url = 'http://{}/cm'.format(ip)
        self.web_password = web_password

    def send_cmd_and_print_res(self, cmd):
        params = {'cmnd': cmd}

        if self.web_password:
            params['user'] = 'admin'
            params['password'] = self.web_password

        try:
            response = requests.get(self.url, params=params, timeout=TIMEOUT_S)
            print response.text
        except requests.exceptions.Timeout:
            raise NotReachable


def configure_sonoff(ip, name, topic=None, old_password=None):
    def configure_web_password(web_password):
        api_before_setting_password = SonoffApi(ip, old_password)
        api_before_setting_password.send_cmd_and_print_res(
            'WebPassword {}'.format(web_password))

    def configure_mqtt_and_name():
        def to_snake_case(word):
            return '_'.join(name.lower().split(' '))

        friendly_name = name
        if topic:
            mqtt_topic = topic
        else:
            mqtt_topic = to_snake_case(name)

        api = SonoffApi(ip, web_password=secrets['web_password'])
        print ''
        print 'Before Update'
        api.send_cmd_and_print_res('Status')
        print ''

        api.send_cmd_and_print_res('FriendlyName {}'.format(friendly_name))

        api.send_cmd_and_print_res('Topic {}'.format(mqtt_topic))
        api.send_cmd_and_print_res('MqttHost {}'.format(config['mqtt_host']))
        api.send_cmd_and_print_res('MqttUser {}'.format(secrets['mqtt_user']))
        api.send_cmd_and_print_res(
            'MqttPassword {}'.format(secrets['mqtt_pass']))

        api.send_cmd_and_print_res('IPAddress1 {}'.format(ip))
        api.send_cmd_and_print_res('IPAddress2 {}'.format(config['IpConfig']['gateway']))
        api.send_cmd_and_print_res('IPAddress3 {}'.format(config['IpConfig']['mask']))
        api.send_cmd_and_print_res('IPAddress4 {}'.format(config['IpConfig']['dns']))

        print ''
        print 'After Update'
        api.send_cmd_and_print_res('Status')
        print ''

    try:
        configure_web_password(secrets['web_password'])
        configure_mqtt_and_name()
        return True
    except NotReachable:
        return False


def show_status(ip):
    api = SonoffApi(ip, web_password=secrets['web_password'])
    api.send_cmd_and_print_res('Status')


if __name__ == '__main__':
    secrets = load_sonoff_section(SECRETS_FILE)
    config = load_sonoff_section(CONFIG_FILE)
    old_password = None

    configured = []
    failed = []
    for device in config['devices']:
        success = configure_sonoff(device['IP'], device['name'], topic=device.get('topic'), old_password=old_password)
        if success:
            configured.append(device)
        else:
            failed.append(device)


    print ''
    print("Configured devices:")
    for device in configured:
        print(" - Name: {name} | IP: {ip}".format(name=device['name'], ip=device['IP']))
    print("")
    if failed:
        print("Failed devices:")
        for device in failed:
            print(" - Name: {name} | IP: {ip}".format(name=device['name'], ip=device['IP']))

    print ''
