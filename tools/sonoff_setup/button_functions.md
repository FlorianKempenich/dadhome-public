# Button function
## Short presses
1. toggle switch status.
2. toggle switch status. For Sonoff Dual, toggle switch 2 status
3. To enter SmartConfig mode, the switch must be configured using the ESP8266 SmartConfig app. Press again to exit and restart.
4. Enter hotspot mode. The switch creates a hotspot and can be set by accessing 192.168.4.1 after the connection. Press again to exit and restart.
5. Enter WPS (Quick Connect) mode. At this point, press the router's WPS button to connect the switch to WiFi.
6. Restart the switch.
7. Start OTA update. The indicator light will continue to flash while updating.

## Hold
For 4 seconds: Restore all settings to their default values ​​(user_config.h).
