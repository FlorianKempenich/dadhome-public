#!/bin/bash


if ! hash platformio 2>/dev/null
then
    echo "'platformio' must be installed!"
    echo ""
    echo "Create a Pipenv & install 'platformio'"
    echo ""
    exit 1
fi

SONOFF_TASMOTA_VERSION="v6.1.1"

# Update `platformio`
platformio update
# platformio upgrade # Do not update keep version set in Pipfile

# Clone correct version of 'Sonoff-Tasmota'
git clone git@github.com:arendst/Sonoff-Tasmota.git
cd Sonoff-Tasmota
git checkout $SONOFF_TASMOTA_VERSION

# Setup the project
cp ../templates/platformio.ini ./platformio.ini
cp ../templates/user_config.h ./sonoff

# Compile the project
platformio run
