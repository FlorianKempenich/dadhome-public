from flask import Flask, render_template, request
from api import Api

app = Flask(__name__)
api = Api()


def new_motion(sensor_id):
    api.send_event('xiaomi_aqara.motion', entity_id=sensor_id)
    api.set_state(entity_id=sensor_id, new_state='on')

def no_more_motion(sensor_id):
    api.set_state(entity_id=sensor_id, new_state='off')

def click(interrupteur_id):
    api.send_event('click', entity_id=interrupteur_id)


@app.route('/', methods=['POST', 'GET'])
def sandbox():
    if request.method == 'POST':

        if 'click_interrupteur_1_papa' in request.form:
            click('binary_sensor.papa_interrupteur_1')
        elif 'click_interrupteur_2_papa' in request.form:
            click('binary_sensor.papa_interrupteur_2')

        elif 'new_motion_papa' in request.form:
            new_motion('binary_sensor.papa_mouvement')
        elif 'no_more_motion_papa' in request.form:
            no_more_motion('binary_sensor.papa_mouvement')

        elif 'new_motion_couloir' in request.form:
            new_motion('binary_sensor.couloir_etage_mouvement')
        elif 'no_more_motion_couloir' in request.form:
            no_more_motion('binary_sensor.couloir_etage_mouvement')

        elif 'new_motion_salle_bain' in request.form:
            new_motion('binary_sensor.salle_de_bain_mouvement')
        elif 'no_more_motion_salle_bain' in request.form:
            no_more_motion('binary_sensor.salle_de_bain_mouvement')

    return render_template('sandbox.html')
