import os
import yaml
import homeassistant.remote as remote
import requests


def _relative_to_this_file(path):
    return os.path.join(os.path.dirname(__file__), path)


SECRETS_FILE = _relative_to_this_file(
    '../../homeassistant/config/secrets.yaml')
CONFIG_FILE = _relative_to_this_file('../config/config.yaml')

HASS_ENDPOINT = 'https://dad-home.access.ly'
HASS_PORT = '443'
CHROMECAST_PORT = '8008'


def main():
    def _load_file(file_to_load):
        with open(file_to_load) as f:
            return yaml.load(f)

    api_password = _load_file(SECRETS_FILE)['http_password']
    api = remote.API(
        host=HASS_ENDPOINT,
        port=HASS_PORT,
        api_password=api_password)

    print(f' ApiStatus: {remote.validate_api(api)}')


    states = remote.get_states(api)
    
    entities = [state.entity_id for state in states]
    for entity in entities:
        print(entity)
    #  print(remote.get_states(api))



if __name__ == "__main__":
    main()



