import os
import yaml
import homeassistant.remote as remote
import requests


def _relative_to_this_file(path):
    return os.path.join(os.path.dirname(__file__), path)


SECRETS_FILE = _relative_to_this_file(
    '../../homeassistant/config/secrets.yaml')
CONFIG_FILE = _relative_to_this_file('../config/config.yaml')

HASS_ENDPOINT = 'https://dad-home.access.ly'
HASS_PORT = '443'
CHROMECAST_PORT = '8008'


class Api():
    def __init__(self):
        api_password = self._load_file(SECRETS_FILE)['http_password']
        self.hass_api = remote.API(
            host=HASS_ENDPOINT,
            port=HASS_PORT,
            api_password=api_password)

    @staticmethod
    def _load_file(file_to_load):
        with open(file_to_load) as f:
            return yaml.load(f)

    def send_event(self, event_type, **data):
        remote.fire_event(self.hass_api, event_type, data)

    def set_state(self, entity_id, new_state):
        remote.set_state(self.hass_api, entity_id=entity_id, new_state=new_state)

