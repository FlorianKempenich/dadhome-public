# Smart Home Setup Journal

## July 12th - Thursday - 4h15
* Setup NUC Server
* Install ubuntu & all software

## July 13th - Friday - 5h35
* Initial Project Setup
    * HomeAssistant
    * NginX
    * Mosquitto
    * _Copied from `porto_hass`_

## July 14th - Saturday - 7h35
* Xiaomi Setup
* Remote access via Python script
    * Basic version for now

## July 15th - Sunday - 6h45
* **HassToolbox**
    * Send notif
    * **Add Xiaomi device** - With progressbar!
    * Remove Xiaomi device
    * **Stream Events** - With formatting!
* Xiaomi Second Gateway

## July 16th - Monday - 4h10
* **Appdaemon Test Framework**
    * Release Framework
        * Setup continuous integration
        * Refactor packages
        * Add Examples: 
            * Simple: `unittest` & `pytest`
            * Full: Project with `Kitchen` & `Bathroom`

## July 17th - Tuesday - 9h55
* **HueManagementCli**
    * Direct Commands
        * Activate auto-update
        * Trigger manual install - Hue Software Update
        * Update zigbee channel
    * `HueAdm` integration
        * Add new lights: `search-lights`
        * Check newly added lights: `new-lights`
        * Info on lights: `lights`
        * Groups, and other
* **HassToolbox**
    * Multiple gateways --- Kitchen/Upstairs

## July 18th - Wednesday - 1h43
* Huge cleanup of `entity_registry.yaml`
* **HassToolbox**
    * Reload automations

## July 19th - Thursday - 4h20
* Setup Plex
* **Appdaemon Test Framework**
    * Add comments to examples

## July 20th - Friday - 0h15
* Draft script to reboot Google Homes

## July 21th - Saturday - 2h05
* **Appdaemon Test Framework**
    * Write 5-min Quick-Start Guide

## July 23th - Monday - 3h30
* **Appdaemon Test Framework**
    * Finish redacting `README.md`
        * General Test Flow and Available Helpers
        * Under The Hood
        * Advanced Usage

## July 26th - Thursday - 0h35
* **Appdaemon**
    * Initial setup

## July 27th - Friday - _TBC_
* Write Journal
* _TBC_


