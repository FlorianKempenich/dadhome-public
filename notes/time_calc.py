import re
import math
from datetime import timedelta

HOURLY_RATE = 30
TAX_RATE = 55

regex = re.compile(
    r'((?P<hours>\d+?)hr)?((?P<minutes>\d+?)m)?((?P<seconds>\d+?)s)?')


def parse_time(time_str):
    parts = regex.match(time_str)
    if not parts:
        return
    parts = parts.groupdict()
    time_params = {}
    for (name, param) in parts.items():
        if param:
            time_params[name] = int(param)
    res = timedelta(**time_params)
    print(res)
    return res


def total_hours(time_delta):
    return math.ceil(
        time_delta.total_seconds() / 3600)


def money_made(hours_worked):
    def gross_to_net(gross):
        return gross * (1 - TAX_RATE/100)
    return gross_to_net(hours_worked * HOURLY_RATE)


duration_worked = parse_time('4hr15m')\
    + parse_time('5hr35m')\
    + parse_time('7hr35m')\
    + parse_time('6hr45m')\
    + parse_time('4hr10m')\
    + parse_time('9hr55m')\
    + parse_time('1hr43m')\
    + parse_time('4hr20m')\
    + parse_time('0hr15m')\
    + parse_time('2hr05m')\
    + parse_time('3hr30m')\
    + parse_time('0hr35m')\
    + parse_time('3hr40m')\
    + parse_time('1hr30m')\
    + parse_time('4hr30m')\
    + parse_time('9hr25m')\
    + parse_time('11hr05m')\
    + parse_time('8hr05m')\
    + parse_time('5hr50m')\
    + parse_time('5hr45m')\
    + parse_time('2hr00m')\
    + parse_time('5hr40m')\
    + parse_time('3hr00m')\
    + parse_time('9hr10m')\
    + parse_time('5hr10m')


print(duration_worked)

hours_worked = total_hours(duration_worked)
print(hours_worked)
print(money_made(hours_worked))
print(money_made(hours_worked)*12)
