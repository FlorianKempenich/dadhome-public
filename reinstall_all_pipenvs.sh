#!/bin/bash

pipfiles=(\
    $(pwd)/tools/hasstoolbox/Pipfile\
    $(pwd)/tools/huemanagementcli/Pipfile\
    $(pwd)/tools/sonoff_setup/Pipfile\
    $(pwd)/tools/remote_access_sandbox/Pipfile\
    $(pwd)/tools/xiaomi_gateway_sandbox/Pipfile\
    $(pwd)/app_daemon/Pipfile)

for pipfile in ${pipfiles[@]}; do
    export PIPENV_PIPFILE=$pipfile
    pipenv --rm
    pipenv install --dev
done
