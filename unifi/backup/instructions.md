# Restore Backup of Unifi Controller

1. Make sure the IP of the new controller (this server) is the same as before
  * IP: `192.168.1.18` at the time of writing
2. If existing, erase the `unifi/config` directory
3. Start the `docker-compose` project
4. Go to `https://192.168.1.18:8443/`
5. Select: `restore from a previous backup`
6. Restore with `backup.unf`
